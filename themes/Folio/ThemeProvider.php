<?php

namespace Folio\Themes\Folio;

use App\Themes\ThemeProvider as ThemeProviderInterface;

class ThemeProvider implements ThemeProviderInterface
{
	public function getBasePath()
	{
		return __DIR__;
	}
}