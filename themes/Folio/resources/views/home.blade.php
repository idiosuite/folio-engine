@extends('layout')
@section('content')
	@if ($post)
		@include('content')
	@endif
	@include('gallery')
@endsection