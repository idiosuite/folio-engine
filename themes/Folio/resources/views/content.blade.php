<div class="Post Post--is{{ ucfirst((string)$post->type) }} Post--{{ $post->slug }}">
	<h1>{{ $post->title }}</h1>
	{!! $post->html()->getBody() !!}
	@if ($post->credits)
		<div class="Credits">
			<h3>Credits</h3>
			{!! $post->html()->getCredits() !!}
		</div>
	@endif
	@if ($post->type != 'page')
		<h4 class="Post__date">{{ $post->getDate()->format('F Y') }}</h4>
	@endif
</div>