<ul class="Links">	
	@foreach ($links as $link)
		<li class="Links__item">
			<a href="{{ $link->url }}" class="Links__{{ $link->slug }}">
				{{ $link->text }}
			</a>
		</li>
	@endforeach		
</ul>