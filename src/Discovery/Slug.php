<?php

namespace App\Discovery;

interface Slug
{
	public function getFilePath();
	public function getPath();
	public function getBasePath();
	public function getFull();
	public function getPrefix();
	public function getOriginal();
	public function getBase();
}