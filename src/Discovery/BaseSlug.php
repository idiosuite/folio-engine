<?php

namespace App\Discovery;

class BaseSlug implements Slug
{
	private $originalSlug;

	private function __construct($originalSlug)
	{
		$this->originalSlug = $originalSlug;
	}

	public function __toString()
	{
		return $this->getOriginal();
	}

	public static function fromString($originalSlug)
	{
		return new static($originalSlug);
	}

	public function getPath()
	{
		return $this->getOriginal();
	}

	public function getFilePath()
	{
		return $this->getFilename();
	}

	public function getBasePath()
	{
		return null;
	}

	private function getFilename()
	{
		return $this->originalSlug . '.md';
	}

	public function getFull()
	{
		return $this->getPrefix() . $this->getBase();
	}

	public function getPrefix()
	{
		return null;
	}

	public function getOriginal()
	{
		return $this->originalSlug;
	}

	public function getBase()
	{
		return $this->getOriginal();
	}
}