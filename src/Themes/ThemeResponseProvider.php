<?php

namespace App\Themes;

use App\Context;
use App\Domain\Post;
use App\Http\Response;
use App\Portfolio;

class ThemeResponseProvider
{
	private $portfolio;
	private $context;
	private $theme;
	
	public function __construct(Portfolio $portfolio, Context $context = null)
	{
		$this->portfolio = $portfolio;
		$this->context = $context ?: app(Context::class);
		$this->theme = new Theme($this->portfolio, $this->context);
	}

	public function homepage() : Response
	{
		$this->context->isHome();

		return $this->theme->getHomeResponse();
	}

	public function project($slug) : Response
	{
		return $this->findPostOrReturnNotFound($this->portfolio->projects(), $slug);
	}

	public function page($slug) : Response
	{
		return $this->findPostOrReturnNotFound($this->portfolio->pages(), $slug);
	}

	private function findPostOrReturnNotFound($collection, $slug) : Response
	{
		try {
			$post = $collection->find($slug);
		} catch (\Exception $e) {
			return $this->notFound();
		}

		return $this->content($post);
	}

	public function content(Post $post) : Response
	{
		$this->context->isNotHome();

		if ( ! $post->shouldRender()) {
			return response('');
		}

		if ( ! $post->exists()) {
			return $this->notFound();
		}

		return $this->theme->getPostResponse($post);
	}

	public function notFound() : Response
	{
		return $this->theme->getNotFoundResponse();
	}
}