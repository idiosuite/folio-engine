<?php

namespace App\Themes\Publishers\Images\Output;

class OutputFactory
{
	private $output = [];
	private $files = [];
	private $currentFormat = null;

	public function __construct()
	{
		$this->files = new FileOutputFactory($this);
	}

	public static function file()
	{
		return (new FileOutputFactory);
	}

	public static function make()
	{
		return (new static)
	    	->export('webp')->compress(['jpg', 'webp'])
	    	->export('png')->compress(['jpg', 'webp'])->compressAlpha(['png', 'webp'])
	    	->export('jpg')->compress(['jpg', 'webp'])
	    	->export('gif')->raw()
	    	->export('svg')->raw()
	    	->export('default')->skip();
	}

	public function export($format)
	{
		$this->currentFormat = $format;
		$this->output[$this->currentFormat] = new OutputMap;
		
		return $this;
	}

	public function compress(array $formats)
	{
		$this->output[$this->currentFormat]->compress($formats);

		return $this;
	}

	public function compressAlpha(array $formats)
	{
		$this->output[$this->currentFormat]->compressAlpha($formats);

		return $this;
	}

	public function raw()
	{
		$this->output[$this->currentFormat]->raw();

		return $this;
	}

	public function skip()
	{
		$this->output[$this->currentFormat]->skip();

		return $this;
	}

	public function get($format) : OutputMap
	{
		if ($this->has($format)) {
			return $this->output[$format];
		}

		if ($this->has('default')) {
			return $this->output['default'];
		}

		throw new \Exception(
			"Please provide a [default] export or specify an export for [{$format}]"
		);
	}

	public function has($format)
	{
		return array_key_exists($format, $this->output);
	}

	public function files()
	{
		return $this->files;
	}

	public function toArray()
	{
		return array_map(function($outputMap) {
			return $outputMap->toArray();
		}, $this->output);
	}
}