<?php

namespace App\Themes\Publishers\Images\Output;

class OutputMap
{
	private $output = [];

	public function getMethod()
	{
		return $this->output['method'];
	}

	public function getExports(bool $hasAlpha)
	{
		return $hasAlpha && $this->has('exports:alpha')
			? $this->output['exports:alpha']
			: $this->output['exports'];
	}

	public function has($key)
	{
		return array_key_exists($key, $this->output);
	}

	public function override(array $override)
	{
		$this->output = array_merge($this->output, $override);
	}
	
	public function skip()
	{
		$this->output['method'] = 'skip';
	}

	public function raw()
	{
		$this->output['method'] = 'uncompressed';
	}

	public function compress(array $formats)
	{
		$this->output['method'] = 'compressed';
		$this->output['exports'] = $formats;
	}

	public function compressAlpha(array $formats)
	{
		$this->output['method'] = 'compressed';
		$this->output['exports:alpha'] = $formats;
	}

	public function toArray()
	{
		return $this->output;
	}
}