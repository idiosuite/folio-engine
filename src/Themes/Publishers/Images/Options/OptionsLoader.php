<?php

namespace App\Themes\Publishers\Images\Options;

class OptionsLoader
{
	private $config;

	public function __construct(array $config)
	{
		$this->config = $config;
	}

	public static function fromArray(array $config)
	{
		return new static($config);
	}

	public function load(Options $options)
	{
		foreach ($this->config as $file => $override) {
			if ( ! $this->providesOptions($override)) {
				return;
			}

			$options->override($file, $this->getOptionsKeyValuePairs($override));
		}
	}

	private function providesOptions(array $override)
	{
		return !! $this->getOptionKeys($override);
	}

	private function getOptionsKeyValuePairs(array $override)
	{
		return array_intersect_key(
			$override, 
			array_flip($this->getOptionKeys())
		);
	}

	private function getOptionKeys()
	{
		return ['quality'];
	}
}