<?php

namespace App\Themes\Publishers\Images\Options;

use App\Themes\Publishers\Images\Output\OutputFactory;

class Options
{
	private $baseOptions = [];
	private $output;
	private $override = [];

	public function __construct(array $baseOptions, OutputFactory $output = null)
	{
		$this->baseOptions = array_merge(
			$this->getDefaultOptions(), 
			$baseOptions
		);

		$this->output = $output ?: OutputFactory::make();

		(new OptionsFromJsonFile)->load($this);
	}

	public function output()
	{
		return $this->output;
	}

	public function get($key)
	{
		$override = $this->has($key)
			? $this->override[$key]
			: [];

		return array_merge($this->baseOptions, $override);
	}

	public function has($key)
	{
		return array_key_exists($key, $this->override);
	}

	public function override($key, array $overrideOptions)
	{
		$this->override[$key] = $overrideOptions;

		return $this;
	}

	private function getDefaultOptions()
	{
		return ['quality' => 70];
	}
}