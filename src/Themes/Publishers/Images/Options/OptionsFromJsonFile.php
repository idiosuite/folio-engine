<?php

namespace App\Themes\Publishers\Images\Options;

class OptionsFromJsonFile
{
	private $basePath;
	private $fileName;
	private $config = [];

	public function __construct(string $fileName = null)
	{
		$this->basePath = env('BASE_PATH');
		$this->fileName = $fileName ?: 'images.config.json';
	}

	public function load(Options $options)
	{
		if ( ! $this->exists()) {
			return;
		}

		$this->config = $this->getArray();

		OptionsLoader::fromArray($this->config)->load($options);
		OutputLoader::fromArray($this->config)->load($options);
	}

	private function getFilePath()
	{
		return $this->basePath . '/' . $this->fileName;
	}

	private function exists()
	{
		return file_exists($this->getFilePath());
	}

	private function getArray()
	{
		return json_decode($this->getJson(), $assoc = true);
	}

	private function getJson()
	{
		return file_get_contents($this->getFilePath());
	}
}