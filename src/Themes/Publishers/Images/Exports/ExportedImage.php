<?php

namespace App\Themes\Publishers\Images\Exports;

use App\Themes\Publishers\Manifests\ImageManifestData;
use App\Themes\Publishers\Images\Options\Options;

class ExportedImage
{
	private $image;
	private $errorHandler;
	
	private $overwrite = true;

	public function __construct(AbstractImage $image)
	{
		$this->image = $image;
		$this->errorHandler = new ErrorHandler;
	}

	public function overwrite(bool $overwrite = true)
	{
		$this->overwrite = $overwrite;

		return $this;
	}

	public function create(ExportData $exportData)
	{
		$destinationPath = $this->getDestinationPathWithQualifiedExtension($exportData);

		if ( ! $this->overwrite) {
			$destinationPath = (new ConflictResolver)->resolve($destinationPath);
		}
		
		$this->errorHandler->replace();
		$this->image->create($exportData, $destinationPath);
		$this->errorHandler->reset();

		return ImageManifestData::fromFilePath($destinationPath)->get([
			'method' => 'compressed',
			'errors' => $this->errorHandler->getErrors()
		]);
	}

	private function getDestinationPathWithQualifiedExtension(ExportData $exportData)
	{
		return str_replace(
			".{$exportData->sourceExtension}", 
			"-compressed.{$this->image->getExtension()}", 
			$exportData->destinationPath
		);
	}
}