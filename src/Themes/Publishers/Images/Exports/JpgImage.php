<?php

namespace App\Themes\Publishers\Images\Exports;

class JpgImage extends AbstractImage
{
	protected function getCreateMethod()
	{
		return 'imagejpeg';
	}

	public function getExtension()
	{
		return 'jpg';
	}
}