<?php

namespace App\Themes\Publishers\Images\Exports;

class ConflictResolver
{
	private $limit;

	public function __construct(int $limit = 100) {
		$this->limit = $limit;
	}

	public function resolve(string $destinationPath) : string
	{
		$parts = $this->separateRootFromExtension($destinationPath);
		
		$i = 1;

		while ( $this->exists($destinationPath) ) {
			$this->guardLimit($i);
			$destinationPath = $this->getNewDestinationPath($parts, $i);
			$i++;
		}

		return $destinationPath;
	}

	private function getNewDestinationPath(array $parts, int $i)
	{
		return $parts['root'] . "-{$i}." . $parts['extension'];
	}

	private function separateRootFromExtension(string $destinationPath) : array
	{
		$parts = explode('.', $destinationPath);

		$extension = array_pop($parts);
		$root = implode('.', $parts);

		return compact('root', 'extension');
	}

	private function exists(string $destinationPath)
	{
		return file_exists($destinationPath);
	}

	private function guardLimit($i)
	{
		if ($i < $this->limit) {
			return;
		}

		throw new \Exception('Too many iterations.');
	}
}