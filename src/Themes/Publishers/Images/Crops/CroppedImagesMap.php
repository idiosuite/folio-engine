<?php

namespace App\Themes\Publishers\Images\Crops;

class CroppedImagesMap
{
    private $basePath;
    private $defaultOptions;
    private $map = [];

    public function __construct($basePath = null)
    {
        $this->basePath = $basePath ?: env('IMAGES_SOURCE_PATH');
        $this->defaultOptions = new CroppedImageOptions;
    }

    public function load()
    {
        (new CroppedImagesMapLoader)->load($this);

        return $this;
    }

    public function addArrayMap(array $mapArray)
    {
        foreach ($mapArray as $relativePath => $options) {
			$this->addWithRelativePath($relativePath, $options);
		}

        return $this;
    }

    public function addWithRelativePath($relativePath, array $options)
    {
        return $this->add(
            $this->getAbsolutePath($relativePath), 
            $options
        );
    }

    public function skip($absolutePath)
    {
        return $this->add($absolutePath, ['skip' => true]);
    }

    public function add($absolutePath, array $options)
    {
        $this->map[$absolutePath] = new CroppedImageOptions($options);

        return $this;
    }

    public function getRequestedFormats($key, array $formats) : array
    {
        $requestedFormats = $this->getFormats($key);

		return array_filter(
			$formats,
			function ($format) use ($requestedFormats) {
				return in_array(
					$format['suffix'], 
					$requestedFormats
				);
			}
		);
    }

    public function getFormats($key)
    {
        return $this->get($key)->getFormatsWithLimit();
    }

    private function get($key)
    {
        if ($this->hasMatchingKey($key)) {
            $key = $this->getMatchingKey($key);
        }
        
        if ( ! $this->hasExactly($key)) {
            return $this->defaultOptions;
        }

        return $this->map[$key];
    }

    public function hasExactly($absolutePath)
    {
        return array_key_exists($absolutePath, $this->map);
    }

    private function hasMatchingKey($absolutePath)
    {
        return !! $this->getMatchingKey($absolutePath);
    }

    private function getMatchingKey($absolutePath)
    {
        return current(
            array_filter(
                array_keys($this->map),
                function ($key) use ($absolutePath) {
                    return stripos($absolutePath, $key) === 0;
                }
            )
        );
    }

    private function getAbsolutePath($relativePath)
    {
        return $this->basePath . '/' . trim($relativePath, '/');
    }
}

class CroppedImageOptions
{
    private $options = [];

    public function __construct(array $options = [])
    {
        if (array_key_exists('skip', $options) && !! $options['skip']) {
            $options['crop-limit'] = 'source';
            unset($options['skip']);
        }

        $this->options = $options;
    }

    public function getFormatsWithLimit()
    {
        $cropLimit = $this->has('crop-limit')
            ? $this->get('crop-limit')
            : null;

        switch ($cropLimit)
        {
            case 'source':
            case 'landscape':
                return [];
            case 'square':
                return ['square'];
            default:
                return $this->allFormats();
        }
    }

    private function allFormats()
    {
        return ['square', 'portrait'];
    }

    public function has($key)
    {
        return array_key_exists($key, $this->options);
    }

    public function get($key)
    {
        return $this->options[$key];
    }
}