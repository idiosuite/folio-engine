<?php

namespace App\Themes\Publishers\Images\Crops;

interface CropsImage
{
    public function writeCroppedImage($sourceImagePath, $destinationImagePath, array $cropRectangle);
}