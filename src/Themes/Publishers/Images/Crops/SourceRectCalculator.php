<?php

namespace App\Themes\Publishers\Images\Crops;

class SourceRectCalculator
{
    private $sourcePath;

    public function __construct($sourcePath)
    {
        $this->sourcePath = $sourcePath;
    }
    
    public function getSourceRect(array $destinationRect)
    {
        $sourceImageSize = getimagesize($this->sourcePath);

        return $this->getCroppedSourceRect(
            $this->getBaseSourceRectangle($sourceImageSize), 
            $destinationRect
        );
    }

    private function getBaseSourceRectangle($imageSize)
    {
        return [
            'width' => $imageSize[0],
            'height' => $imageSize[1],
            'x' => 0,
            'y' => 0
        ];
    }

    private function getCroppedSourceRect(array $sourceRect, array $destinationRect)
    {
        return $this->sourceImageFitsInDestinationRatio($sourceRect, $destinationRect)
            ? $this->getSourceRectForHorizontalCrop($sourceRect, $destinationRect)
            : $this->calculateRectForVerticalCrop($sourceRect, $destinationRect);
    }

    private function getSourceRectForHorizontalCrop(array $sourceRect, array $destinationRect) : array
    {
        $originalSourceWidth = $sourceRect['width'];
        
        $sourceRect['width'] = ceil(
            $sourceRect['height'] * $destinationRect['width'] / $destinationRect['height']
        );
        $sourceRect['x'] = ceil(
            ($originalSourceWidth - $sourceRect['width']) / 2
        );

        return $sourceRect;
    }

    private function calculateRectForVerticalCrop(array $sourceRect, array $destinationRect) : array
    {
        $originalSourceHeight = $sourceRect['height'];

        $sourceRect['height'] = ceil(
            $sourceRect['width'] * $destinationRect['height'] / $destinationRect['width']
        );
        $sourceRect['y'] = ceil(
            ($originalSourceHeight - $sourceRect['height']) / 2
        );

        return $sourceRect;
    }

    private function sourceImageFitsInDestinationRatio(array $sourceRect, array $destinationRect) : bool
    {
        $sourceRatio = $sourceRect['width'] / $sourceRect['height'];
        $destinationRatio = $destinationRect['width'] / $destinationRect['height'];

        return $sourceRatio >= $destinationRatio;
    }
}