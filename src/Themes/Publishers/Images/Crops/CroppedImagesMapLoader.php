<?php

namespace App\Themes\Publishers\Images\Crops;

use App\Repositories\FileSystem\FilesCrawler;
use App\Repositories\FileSystem\PathInspector;

class CroppedImagesMapLoader
{
    private $configFileName;
    private $imagesSourcePath;

    public function __construct()
    {
        $this->imagesSourcePath = env('IMAGES_SOURCE_PATH');
        $this->configFileName = 'images.json';
    }

    public function load(CroppedImagesMap $map)
    {
        foreach ($this->getAllConfigFiles() as $absolutePath) {
            $this->loadFile($map, $absolutePath);
        }
    }

    private function getAllConfigFiles() : array
    {
        $paths = $this->getAllFilesInImagesSourcePath();
        $paths = $this->getOnlyConfigFiles($paths);
        return array_values($paths);
    }

    private function getAllFilesInImagesSourcePath()
    {
        return FilesCrawler::fromPath(
            $this->imagesSourcePath
        )->getAllFiles();
    }

    private function getOnlyConfigFiles(array $paths)
    {
        return array_filter(
            $paths, 
            function ($absolutePath) {
                $pathInspector = PathInspector::fromPath($absolutePath);
                return $pathInspector->getFileName() === $this->configFileName;
            }
        );
    }

    private function loadFile(CroppedImagesMap $map, string $absolutePath)
    {
        foreach ($this->readJsonFile($absolutePath) as $relativePath => $options) {
            $map->add(
                $this->getAbsolutePath($absolutePath, $relativePath),
                $options
            );
        }
    }

    private function getAbsolutePath($absolutePath, $relativePath)
    {
        $basePath = PathInspector::fromPath($absolutePath)->getBasePath();
        $relativePath = trim($relativePath, '/');
        return rtrim(
            "{$basePath}/{$relativePath}", 
            '/'
        );
    }

    private function readJsonFile(string $absolutePath)
    {
        return json_decode(
            file_get_contents($absolutePath), 
            $assoc = true
        );
    }
}