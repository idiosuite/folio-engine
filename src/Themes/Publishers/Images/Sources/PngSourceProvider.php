<?php

namespace App\Themes\Publishers\Images\Sources;

class PngSourceProvider extends SourceProvider
{
	protected function createImage($sourcePath)
	{
		return imagecreatefrompng($sourcePath);
	}

	protected function writeImage(\GDImage $destinationImage, $destinationPath)
	{
		return imagepng($destinationImage, $destinationPath);
	}
}