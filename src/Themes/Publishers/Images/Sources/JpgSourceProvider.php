<?php

namespace App\Themes\Publishers\Images\Sources;

class JpgSourceProvider extends SourceProvider
{
	protected function createImage($sourcePath)
	{
		return imagecreatefromjpeg($sourcePath);
	}

	protected function writeImage(\GDImage $destinationImage, $destinationPath)
	{
		return imagejpeg($destinationImage, $destinationPath);
	}
}