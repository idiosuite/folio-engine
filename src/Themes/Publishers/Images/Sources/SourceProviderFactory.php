<?php

namespace App\Themes\Publishers\Images\Sources;

use App\Repositories\FileSystem\PathInspector;

class SourceProviderFactory
{
	public static function fromPath($filePath)
	{
		return static::fromExtension(
			PathInspector::fromPath($filePath)->getExtension()
		);
	}

    public static function fromExtension($extension)
    {
        $sourceProviderClassName = sprintf(
			'App\Themes\Publishers\Images\Sources\%sSourceProvider', 
			ucfirst($extension)
		);
		
		return new $sourceProviderClassName;
    }

    public static function isSupported($extension)
    {
        return in_array(
			$extension, 
			static::supportedExtensions()
		);
    }

    public static function supportedExtensions()
	{
		return ['jpg', 'png', 'webp', 'gif'];
	}
}