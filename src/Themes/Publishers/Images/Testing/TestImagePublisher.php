<?php

namespace App\Themes\Publishers\Images\Testing;

use App\Themes\Publishers\Images\ImagePublisher;
use App\Themes\Publishers\Manifests\Manifest;
use App\Themes\Publishers\Manifests\ImageManifestData;
use App\Themes\Publishers\Images\Options\Options;

class TestImagePublisher implements ImagePublisher
{
	private $sourcePath;
	
	public function __construct($sourcePath)
	{
		$this->sourcePath = $sourcePath;
	}

	public function overwrite(bool $overwrite) : static
	{
		return $this;
	}

	public function publish(Options $options, Manifest $manifest)
	{
		$manifest->addEntry($this->sourcePath, [
			'input' => ImageManifestData::fromFilePath($this->sourcePath)->get(),
			'output' => [
				ImageManifestData::fromFilePath($this->sourcePath)->get()
			]
		]);
	}
}