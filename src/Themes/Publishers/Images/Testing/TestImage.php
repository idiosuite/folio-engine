<?php

namespace App\Themes\Publishers\Images\Testing;

use App\Themes\Publishers\Images\Exports\AbstractImage;
use App\Themes\Publishers\Images\Exports\ExportData;

class TestImage extends AbstractImage
{
	public function __construct()
	{

	}
	
	public function create(ExportData $exportData, $destinationPath)
	{
		imagejpeg($exportData->source, $destinationPath);

		trigger_error('warning triggered by test image', E_USER_WARNING);
		trigger_error('notice triggered by test image', E_USER_NOTICE);
	}

	protected function getCreateMethod()
	{
		return 'imagejpeg';
	}

	public function getExtension()
	{
		return 'test';
	}
}