<?php

namespace App\Themes\Publishers\Images;

use App\Themes\Publishers\Manifests\Manifest;
use App\Themes\Publishers\Manifests\ImageManifestData;
use App\Themes\Publishers\Images\Options\Options;

class UncompressedImagePublisher implements ImagePublisher
{
	private $sourcePath;
	private $destinationPath;

	public function __construct($sourcePath, $destinationPath)
	{
		$this->sourcePath = $sourcePath;
		$this->destinationPath = $destinationPath;
	}

	public function overwrite(bool $overwrite) : static
	{
		return $this;
	}
	
	public function publish(Options $options, Manifest $manifest)
	{
		copy(
			$this->sourcePath, 
			$this->destinationPath
		);
		
		$manifest->addEntry($this->sourcePath, [
			'input' => ImageManifestData::fromFilePath($this->sourcePath)->get(), 
			'output' => [
				ImageManifestData::fromFilePath($this->destinationPath)->get([
					'method' => 'copy'
				])
			]
		]);
	}
}