<?php

namespace App\Themes\Publishers\Images;

use App\Themes\Publishers\Manifests\ManifestFactory;
use App\Generators\FolioManifest;

class ImagesBackup
{
	private $input;
	private $manifest;
	private $destinationPath;
	private $backupDestinationPath;
	private $imagesInFolioManifest;

	public function __construct($input)
	{
		$this->input = $input;

		$this->destinationPath = env('IMAGES_DESTINATION_PATH');
		$this->backupDestinationPath = str_replace('/images', '/images_tmp', $this->destinationPath);
	}

	public function save()
	{
		if ( 
			$this->publishesImages() || 
			! $this->hasPreviouslyPublishedImages()
		) {
			return;
		} 

		$this->loadImagesInFolioManifest();
		$this->loadImagesManifestToPreventDeletion();
		$this->renameImagesFolderToPreventDeletion();
	}

	public function restore()
	{
		if (
			$this->publishesImages() ||
			! $this->hasImagesBackup()
		) {
			return;
		}

		$this->mergeImagesInFolioManifest();
		$this->restoreImagesManifest();
		$this->restoreImagesFolder();
	}

	private function loadImagesInFolioManifest()
	{
		$this->imagesInFolioManifest = FolioManifest::fromJson()
			->filterByPath($this->destinationPath);
	}

	private function publishesImages()
	{
		return !! $this->input->getOption('images');
	}

	private function hasPreviouslyPublishedImages()
	{
		return file_exists($this->destinationPath);
	}

	private function hasImagesBackup()
	{
		return file_exists($this->backupDestinationPath);
	}

	private function loadImagesManifestToPreventDeletion()
	{
		$this->manifest = ManifestFactory::images()->loadIfNotLoaded();
	}

	private function mergeImagesInFolioManifest()
	{
		app(FolioManifest::class)->merge($this->imagesInFolioManifest);
	}

	private function renameImagesFolderToPreventDeletion()
	{
		rename(
			$this->destinationPath, 
			$this->backupDestinationPath
		);
	}

	private function restoreImagesFolder()
	{
		rename(
			$this->backupDestinationPath, 
			$this->destinationPath
		);
	}

	private function restoreImagesManifest()
	{
		$this->manifest->publish();
	}
}