<?php

namespace App\Themes\Publishers\Images\Used;

interface UsedImagesProvider {
    public function findUsedImages(UsedImagesFinder $usedImagesFinder);
}