<?php

namespace App\Themes\Publishers\Images;

use App\Themes\Publishers\Manifests\Manifest;
use App\Themes\Publishers\Manifests\ManifestFactory;
use App\Themes\Publishers\Images\Used\UsedImagesFinder;

class ImagesStorageCleaner
{
    private $relativePath;
    private $absoluteSourcePath;
    private $absoluteStoragePath;
    private $manifest;

    public function __construct(string $relativePath = '')
    {
        $this->relativePath = trim($relativePath, '/');
        $this->absoluteSourcePath = env('IMAGES_SOURCE_PATH') . '/' . $this->relativePath;
        $this->absoluteStoragePath = env('IMAGES_STORAGE_PATH') . '/' . $this->relativePath;
    }

    public static function fromRelativePath(string $relativePath = '')
    {
        return new static($relativePath);
    }

    public function deleteUnusedImages()
    {
        $this->deleteUnusedCroppedImages();
        $this->deleteUnusedCompressedImages();
    }

    private function deleteUnusedCroppedImages()
    {
        $this->deleteManifestImages(
            ManifestFactory::croppedImages()
        );
    }

    private function deleteUnusedCompressedImages()
    {
        $this->deleteManifestImages(
            ManifestFactory::compressedImages()
        );
    }

    private function deleteManifestImages($manifest)
    {
        $manifest->loadIfNotLoaded();

        foreach ($this->getManifestEntriesToBeDeleted($manifest) as $key => $entry) {
            $this->deleteManifestOutputFiles($manifest, $key);
            $this->removeManifestEntry($manifest, $key);
        }

        $manifest->publish();
    }

    private function getManifestEntriesToBeDeleted(Manifest $manifest)
    {
        $entries = $manifest->toArray();

        $entries = array_filter($entries, [$this, 'shouldBeDeleted']);
        $entries = array_filter($entries, [$this, 'isInSpecifiedPath']);

        return $entries;
    }

    private function deleteManifestOutputFiles(Manifest $manifest, $key)
    {
        foreach ($manifest->getEntry($key)->getOutputs() as $output) {
            unlink($output['path']);
        }
    }

    private function removeManifestEntry(Manifest $manifest, $key)
    {
        $manifest->removeEntry($key);
    }

    private function shouldBeDeleted(array $entry)
    {
        return ! app(UsedImagesFinder::class)->isUsed(
            $this->getInputPath($entry)
        );
    }

    private function isInSpecifiedPath(array $entry)
    {
        return $this->startsWithAbsolutePath($entry, $this->absoluteSourcePath) 
            || $this->startsWithAbsolutePath($entry, $this->absoluteStoragePath);
    }

    private function startsWithAbsolutePath($entry, $absolutePath)
    {
        return stripos($this->getInputPath($entry), $absolutePath) === 0;
    }

    private function getInputPath(array $entry)
    {
        return $entry['input']['path'];
    }
}