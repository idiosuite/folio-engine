<?php

namespace App\Themes\Publishers\Images\Processors;

use App\Themes\Publishers\Manifests\Manifest;
use App\Repositories\FileSystem\PathInspector;
use App\Themes\Publishers\Images\Options\Options;
use App\Themes\Publishers\Manifests\ManifestFactory;
use App\Themes\Publishers\Images\ImagePublisherFactory;
use App\Themes\Publishers\Images\CompressedImagePublisher;

class CompressImagesProcessor implements Processor, ProcessorProvider
{
    private $options;
    private $manifest;

    private $imagesIndex = [];
    
    public function __construct($options = [])
    {
        $this->options = $options instanceof Options
            ? $options
            : new Options($options);
    }

    public function process(ProcessorProvider $provider) : ProcessorProvider
    {
        $this->manifest = $this->getLoadedManifest();

        foreach ($provider->getImages() as $images) {
            foreach ($images as $image) {
                $this->compressImage($image);
            }
        }

        $this->manifest->publish();

        return $this;
    }

    public function getImages() : array
    {
        $map = [];

        foreach ($this->imagesIndex as $sourcePath) {
            $map[$sourcePath] = $this->getOutputPaths($sourcePath);
        }

        return $map;
    }

    private function getOutputPaths($sourcePath)
    {
        return array_map(
            function ($output) {
                return $output['path'];
            },
            $this->manifest->getEntry($sourcePath)->getOutputs()
        );
    }

    private function getLoadedManifest()
    {
        $manifest = ManifestFactory::compressedImages();

        $manifest->loadIfNotLoaded();

        return $manifest;
    }

    public function compressImage($sourcePath)
    {
        $this->imagesIndex[] = $sourcePath;

        if ($this->imagesWerePreviouslyCompressed($sourcePath)) {
            return;
        }

        $this->getPublisherInstance($sourcePath)
            ->overwrite(true)
            ->publish(
                $this->getOptions(), 
                $this->manifest
            );
    }

    private function imagesWerePreviouslyCompressed($sourcePath)
    {
        return $this->manifest->has($sourcePath) && 
            $this->manifest->getEntry($sourcePath)->isSameChecksum() &&
            $this->manifest->getEntry($sourcePath)->allOutputsExist();
    }

    private function getPublisherInstance($sourcePath)
    {
        return ImagePublisherFactory::fromOutput(
			$sourcePath,
			$this->getFileDestinationPath($sourcePath),
			$this->getOptions()->output()
		);
    }

    private function getOptions()
    {
        return $this->options;
    }

    private function getFileDestinationPath($fileSourcePath)
    {
        return str_replace(
            $this->getSourcePath(),
            $this->getDestinationPath(),
            $fileSourcePath
        );
    }

    private function getFileExtension($fileSourcePath)
    {
        return PathInspector::fromPath($fileSourcePath)->getExtension();
    }

    private function getSourcePath()
    {
        return env('IMAGES_SOURCE_PATH');
    }

    private function getDestinationPath()
    {
        return env('IMAGES_STORAGE_PATH');
    }
}