<?php

namespace App\Themes\Publishers\Images\Processors;

use App\Themes\Publishers\Images\Used\UsedImagesFinder;

class UsedImagesProvider implements ProcessorProvider
{
    private $sourcePath;
    
    public function __construct($sourcePath = null)
    {
        $this->sourcePath = $sourcePath ?: env('IMAGES_SOURCE_PATH');
    }

    public function getImages() : array
    {
        $files = app(UsedImagesFinder::class)->getProcessorMap(
            $this->sourcePath
        );

        $map = [];

        foreach ($files as $path) {
            $map[$path] = [
                $path
            ];
        }

        return $map;
    }
}