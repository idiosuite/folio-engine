<?php

namespace App\Themes\Publishers\Images\Processors;

use App\Repositories\FileSystem\FilesCrawler;

class AllImagesProvider implements ProcessorProvider
{
    private $sourcePath;
    
    public function __construct($sourcePath = null)
    {
        $this->sourcePath = $sourcePath ?: env('IMAGES_SOURCE_PATH');
    }

    public function getImages() : array
    {
        $files = FilesCrawler::fromPath($this->sourcePath);

        $map = [];

        $files->eachFile(
            function ($item) use (&$map) {
                $map[$item] = [
                    $item
                ];
            }
        );

        return $map;
    }
}