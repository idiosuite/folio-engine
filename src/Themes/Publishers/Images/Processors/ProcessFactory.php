<?php

namespace App\Themes\Publishers\Images\Processors;

class ProcessFactory
{
    public $provider;
    private $processors;

    private function __construct(ProcessorProvider $provider, array $processors = [])
    {
        $this->provider = $provider;
        $this->processors = $processors ?: [
            new CreateImageFoldersProcessor,
            new CreateImageFormatsProcessor,
            new CompressImagesProcessor
        ];
    }

    public static function test($sourcePath)
    {
        return new static(
            new AllImagesProvider($sourcePath),
            [
                new CreateImageFoldersProcessor,
                new CompressImagesProcessor
            ]
        );
    }

    public static function allImages($sourcePath)
    {
        return new static(
            new AllImagesProvider($sourcePath)
        );
    }

    public static function usedImages($sourcePath)
    {
        return new static(
            new UsedImagesProvider($sourcePath)
        );
    }

    public static function websiteFeatureImages($sourcePath = null)
    {
        return new static(
            new WebsiteFeatureImagesProvider($sourcePath),
            [
                new CreateImageFoldersProcessor,
                new CreateWebsiteFeatureImagesProcessor,
                new CompressImagesProcessor
            ]
        );
    }

    public function process()
    {
        $provider = $this->provider;

        foreach ($this->processors as $processor) {
            $provider = $processor->process( $provider );
        }

        return $provider;
    }
}