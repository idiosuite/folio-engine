<?php

namespace App\Themes\Publishers\Images\Processors;

class ArrayImagesProvider implements ProcessorProvider
{
    private $images;

    public function __construct(array $images)
    {
        $this->images = $images;
    }

    public function getImages() : array
    {
        $map = [];

        foreach ($this->images as $path) {
            $map[$path] = [ $path ];
        }

        return $map;
    }
}