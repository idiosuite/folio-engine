<?php

namespace App\Themes\Publishers\Images\Processors;

use App\Repositories\FileSystem\PathInspector;
use App\Themes\Publishers\Manifests\ManifestFactory;
use App\Themes\Publishers\Manifests\ImageManifestData;
use App\Themes\Publishers\Images\Crops\SourceRectCalculator;
use App\Themes\Publishers\Images\Sources\SourceProviderFactory;

class CreateWebsiteFeatureImagesProcessor implements Processor, ProcessorProvider
{
    private $provider;
    private $manifest;
    
    private $providedImages = [];

    public function process(ProcessorProvider $provider) : ProcessorProvider
    {
        $this->manifest = ManifestFactory::websiteFeatureImages()->loadIfNotLoaded();

        foreach ($provider->getImages() as $sourcePath => $images) {
            foreach ($images as $image) {
                $this->processImageIfNotAlreadyProcessed($image);
                $this->addImageToProvidedImages($image);
            }
        }

        $this->manifest->publish();

        return $this;
    }

    public function getImages() : array
    {
        return $this->providedImages;
    }

    private function processImageIfNotAlreadyProcessed($image)
    {
        if ($this->imageWasAlreadyProcessed($image)) {
            return;
        }

        $this->createCroppedImage($image);
        $this->addImageToManifest($image);
    }

    private function imageWasAlreadyProcessed(string $sourcePath) : bool
    {
        $entry = $this->manifest->getEntry($sourcePath);

        return $entry->isSameChecksum() && $entry->allOutputsExist();
    }

    private function createCroppedImage(string $sourcePath) : void
    {
        if ($this->isDSStoreFile($sourcePath)) {
            return;
        }
        
        SourceProviderFactory::fromPath($sourcePath)
            ->writeCopyResampledImage(
                $sourcePath, 
                $this->getDestinationPath($sourcePath), 
                $this->getSourceRect($sourcePath),
                $this->getDestinationRect()
            );
    }

    private function addImageToManifest($sourcePath)
    {
        $this->manifest->addEntry(
            $sourcePath,
            $this->getManifestData($sourcePath)
        );
    }

    private function getManifestData($sourcePath)
    {
        $destinationPath = $this->getDestinationPath($sourcePath);

        return [
            'input' => ImageManifestData::fromFilePath($sourcePath)->get(),
            'output' => [
                ImageManifestData::fromFilePath($destinationPath)->get()
            ]
        ];
    }

    private function addImageToProvidedImages($sourcePath)
    {
        $destinationPath = $this->getDestinationPath($sourcePath);

        $this->providedImages[$destinationPath] = [
            $destinationPath
        ];
    }

    private function getDestinationPath(string $sourcePath)
    {
        $destinationPath = str_replace(
            env('IMAGES_SOURCE_PATH'),
            env('IMAGES_STORAGE_PATH'),
            $sourcePath
        );

        $pathInspector = PathInspector::fromPath($destinationPath);

        return sprintf(
            '%s/%s%s.%s', 
            $pathInspector->getBasePath(),
            $pathInspector->getFileNameWithoutExtension(),
            $suffix = '-website-feature',
            $pathInspector->getExtension()
        );
    }

    private function getSourceRect(string $sourcePath) : array
    {
        return (new SourceRectCalculator($sourcePath))->getSourceRect(
            $this->getDestinationRect()
        );
    }

    private function getDestinationRect() : array
    {
        return [
            'x' => 0, 
            'y' => 0, 
            'width' => 1200, 
            'height' => 630
        ];
    }

    private function isDSStoreFile($filePath)
    {
        return PathInspector::fromPath($filePath)->isDSStoreFile();
    }
}