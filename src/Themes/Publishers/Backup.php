<?php

namespace App\Themes\Publishers;

use \App\Generators\FileSystem;
use App\Themes\Publishers\Manifests\Manifest;

class Backup
{
	private $manifest;
	private $source;
	private $destination;
	private $backupDestination;
	private $backupManifest;

	public function __construct(Manifest $manifest, $source, $destination)
	{
		if ( ! app()->env('local') && ! app()->env('testing')) {
			throw new \Exception('Cannot run in production.');
		}

		$this->source = $source;
		$this->destination = $destination;
		$this->backupDestination = $destination . '_backup';
		$this->manifest = $manifest;
	}

	public function create()
	{
		if ( ! file_exists($this->destination)) {
			return;
		}

		rename($this->destination, $this->backupDestination);

		$this->backupManifest = $this->manifest->createBackupManifest($this->backupDestination);
	}

	public function delete()
	{
		if (is_file($this->backupDestination)) {
			unlink($this->backupDestination);
			return;
		}

		if (is_link($this->backupDestination)) {
			unlink($this->backupDestination);
			return;
		}
		
		if (is_dir($this->backupDestination)) {
			(new BackupEraser(
				$this->source, 
				$this->destination, 
				$this->backupDestination,
				$this->backupManifest
			))->erase();

			$this->restoreForeignFiles(
				$this->backupDestination, 
				$this->destination
			);
			
			return;
		}
	}

	private function restoreForeignFiles($sourcePath, $destinationPath)
	{
		if ( ! file_exists($sourcePath)) {
			return;
		}

		foreach (app(FileSystem::class)->getFilesInFolder($sourcePath) as $file) {
			$pathToSourceFile = $sourcePath . '/' . $file;
			$pathToDestinationFile = $destinationPath . '/' . $file;
			if (is_dir($pathToSourceFile)) {
				return $this->restoreForeignFiles($pathToSourceFile, $pathToDestinationFile);
			}
			if (is_file($pathToSourceFile)) {
				copy($pathToSourceFile, $pathToDestinationFile);
			}
		}
	}

	public function restore()
	{
		if (file_exists($this->backupDestination)) {
			rename($this->backupDestination, $this->destination);
		}
	}
}