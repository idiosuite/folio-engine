<?php

namespace App\Themes\Publishers\Manifests;

interface ManifestEntry
{
	public function toArray() : array;
	public function getOutputs() : array;
	public function isSameChecksum() : bool;
	public function isUncompressed() : bool;
	public function isSkipped() : bool;
	public function inputFileExists() : bool;
	public function allOutputsExist() : bool;
}