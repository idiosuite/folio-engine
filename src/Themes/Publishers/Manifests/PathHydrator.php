<?php

namespace App\Themes\Publishers\Manifests;

class PathHydrator
{
	private $basePath;

	public function __construct()
	{
		$this->basePath = env('BASE_PATH');
	}

	public function getAbsoluteSourcePath($relativePath)
	{
		return $this->basePath . '/' . $relativePath;
	}

	public function getRelativeSourcePath($absolutePath)
	{
		return trim(
			str_replace(
				$this->basePath, 
				'', 
				$absolutePath
			),
			'/'
		);
	}
}