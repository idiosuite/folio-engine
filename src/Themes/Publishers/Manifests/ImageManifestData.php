<?php

namespace App\Themes\Publishers\Manifests;

use \App\Domain\Images\ImageSizeCalculator;

class ImageManifestData
{
	private $filePath;
	private $data = [];

	public function __construct($filePath, array $data = [])
	{
		$this->filePath = $filePath;
		$this->data = $data;
	}

	public static function fromFilePath($filePath)
	{
		return new static($filePath);
	}

	public static function fromArray($filePath, array $data)
	{
		return new static($filePath, $data);
	}

	public static function fromData(array $data)
	{
		return new static($data['path'], $data);
	}

	public function get(array $override = [])
	{
		$data = array_merge(
			$this->calculateData(),
			$this->data
		);

		return array_merge(
			$data, 
			$override
		);
	}

	private function calculateData() : array
	{
		$size = $this->getActualImageSize();

		return [
			'checksum' => $this->getActualChecksum(),
			'bytes' => $this->getActualFileSize(),
			'width' => $size['width'],
			'height' => $size['height'],
			'path' => $this->filePath,
			'format' => $this->getActualFormat()
		];
	}

	public function hydratePath($callback)
	{
		$data = $this->data;

		$data['path'] = call_user_func_array(
			$callback, 
			[ $data['path'] ]
		);

		return $data;
	}

	public function getChecksum() : string
	{
		if ( $this->data ) {
			return $this->data['checksum'];
		}

		return $this->getActualChecksum();
	}

	private function getActualChecksum() : string
	{
		if ( ! $this->fileExists()) {
			return '';
		}

		return md5(
			file_get_contents($this->filePath)
		);
	}

	private function getActualImageSize()
	{
		if (! $this->fileExists() ||  ! $this->isImageFormat()) {
			return ['width' => 0, 'height' => 0];
		}

		return (new ImageSizeCalculator($this->filePath))->get();
	}

	private function getActualFileSize()
	{
		if ( ! $this->fileExists()) {
			return 0;
		}

		return filesize($this->filePath);
	}

	private function isImageFormat()
	{
		return in_array(
			$this->getActualFormat(), 
			['jpg', 'png', 'webp', 'gif', 'svg']
		);
	}

	private function getActualFormat()
	{
		$parts = explode('.', $this->filePath);

		return end($parts);
	}

	private function fileExists()
	{
		return file_exists($this->filePath);
	}
}