<?php

namespace App\Themes\Publishers\Manifests;

class BackupManifestFactory
{
	private $manifest;
	private $backupDestination;

	public function __construct(Manifest $manifest, $backupDestination)
	{
		$this->manifest = $manifest;
		$this->backupDestination = $backupDestination;
	}

	public function get()
	{
		if ($this->previousVersionExists()) {
			return $this->getBackupManifestClone();
		}
	}

	private function getBackupManifestClone() : Manifest
	{
		copy(
			$this->manifest->getFilePath(), 
			$this->backupDestination . '/' . $this->getBackupFileName()
		);

		return new Manifest($this->backupDestination . '/' . $this->getBackupFileName());
	}

    private function previousVersionExists()
    {
        return $this->manifest->exists();
    }

	public function getBackupFileName()
	{
		return str_replace(
			'manifest', 
			'manifest_backup', 
			$this->manifest->getFileName()
		);
	}
}