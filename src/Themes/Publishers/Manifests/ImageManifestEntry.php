<?php

namespace App\Themes\Publishers\Manifests;

use App\Generators\FileSystem;

class ImageManifestEntry implements ManifestEntry
{
	private $data;

	public function __construct(array $data)
	{
		$this->data = $data;
	}

	public function toArray() : array
	{
		return $this->data;
	}

	private function getInput() : array
	{
		return $this->data['input'];
	}

	private function getInputAbsolutePath() : string
	{
		return $this->getInput()['path'];
	}

	public function getOutputs() : array
	{
		return $this->data['output'];
	}

	public function isSameChecksum() : bool
	{
		$actualChecksum = ImageManifestData::fromFilePath(
			$this->getInputAbsolutePath()
		)->getChecksum();
		
		$expectedChecksum = ImageManifestData::fromArray(
			$this->getInputAbsolutePath(),
			$this->getInput()
		)->getChecksum();
		
		return $actualChecksum == $expectedChecksum;
	}

	public function isUncompressed() : bool
	{
		return count($this->getOutputs()) === 1;
	}

	public function isSkipped() : bool
	{
		return count($this->getOutputs()) === 0;
	}

	public function inputFileExists() : bool
	{
		return file_exists(
			$this->getInputAbsolutePath()
		);
	}

	public function allOutputsExist() : bool
	{
		foreach ($this->getOutputs() as $output) {
			if ( ! file_exists($output['path'])) {
				return false;
			}
		}
		
		return true;
	}

	public static function hydrateData(array $data) : static
	{
		return new static($data);
	}

	public static function hydratePaths(array $data, callable $callback) : array
	{
		$data['input'] = static::hydrateDataPath($data['input'], $callback);

		$data['output'] = array_map(
			function ($output) use ($callback) {
				return static::hydrateDataPath($output, $callback);
			},
			$data['output']
		);

		$key = $data['input']['path'];

		return [ $key => $data ];
	}

	private static function hydrateDataPath($data, $callback)
	{
		return ImageManifestData::fromData($data)->hydratePath($callback);
	}
}