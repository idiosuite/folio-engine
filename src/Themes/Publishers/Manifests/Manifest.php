<?php

namespace App\Themes\Publishers\Manifests;

use App\Generators\FileSystem;
use App\Repositories\FileSystem\PathInspector;

class Manifest implements \Countable
{
	private $manifestDestinationPath;
	private $filePath;
	private $entries = [];

	public function __construct($filePath)
	{
		$this->manifestDestinationPath = env('MANIFESTS_DESTINATION_PATH');
		$this->filePath = $filePath;
	}

	public function createBackupManifest($backupDestination)
	{
		return (new BackupManifestFactory(
			$this, 
			$backupDestination
		))->get();
	}

	public function getFileName() : string
	{
		return PathInspector::fromPath(
			$this->getFilePath()
		)->getFileName();
	}

	public function getFilePath() : string
	{
		return $this->filePath;
	}

	public function exists() : bool
	{
		return file_exists($this->filePath);
	}

	public function loadIfNotLoaded() : static
	{
		if ( ! $this->entries && $this->exists()) {
			$this->load();
		}

		return $this;
	}

	public function refresh() : static
	{
		return $this->load();
	}

	public function load() : static
	{
		if ( ! $this->exists()) {
			return $this;
		}

		$this->entries = $this->fromJson();

		return $this;
	}

	public function publish() : static
	{
		$this->removeFilesThatDoNotExist();

		$this->createManifestDestinationFolderIfDoesNotExist();

		file_put_contents($this->filePath, $this->toJson());

		return $this;
	}

	public function count() : int
	{
		return count($this->entries);
	}

	public function addEntry($absoluteSourcePath, array $data = []) : static
	{
		$this->entries[$absoluteSourcePath] = $this->hydrateData($data);

		return $this;
	}

	public function removeEntry($absoluteSourcePath) : static
	{
		unset($this->entries[$absoluteSourcePath]);
		
		return $this;
	}

	public function getEntry($absoluteSourcePath) : ManifestEntry
	{
		if ( ! $this->has($absoluteSourcePath)) {
			return new NullManifestEntry;
		}

		return $this->entries[$absoluteSourcePath];
	}

	public function has($absoluteSourcePath) : bool
	{
		return array_key_exists(
			$absoluteSourcePath, 
			$this->entries
		);
	}

	private function fromJson() : array
	{
		$data = json_decode(
			file_get_contents($this->filePath),
			$assoc = true
		);

		$entries = [];

		foreach ($data as $entryData) {
			$entries = array_merge(
				$entries, 
				$this->fromRelativePath($entryData)
			);
		}

		return $entries;
	}

	public function toJson() : string
	{
		$entries = [];

		foreach ($this->entries as $entry) {
			$entries = array_merge(
				$entries, 
				$this->toRelativePath($entry)
			);
		}

		return json_encode(
			$entries
		);
	}

	public function fromRelativePath(array $data) : array
	{
		return array_map(
			function ($data) {
				return $this->hydrateData($data);
			},
			$this->hydratePaths(
				$data, 
				[new PathHydrator, 'getAbsoluteSourcePath']
			)
		);
	}

	public function toRelativePath(ImageManifestEntry $entry) : array
	{
		return $this->hydratePaths(
			$entry->toArray(), 
			[new PathHydrator, 'getRelativeSourcePath']
		);
	}

	private function hydrateData(array $data)
	{
		return ImageManifestEntry::hydrateData($data);
	}

	private function hydratePaths(array $data, $callback)
	{
		return ImageManifestEntry::hydratePaths($data, $callback);
	}

	public function toArray() : array
	{
		return array_map(
			function ($entry) {
				return $entry->toArray();
			},
			$this->entries
		);
	}

	private function removeFilesThatDoNotExist()
	{
		$this->entries = array_filter(
			$this->entries,
			function ($entry) {
				return $entry->inputFileExists();
			}
		);
	}

	private function createManifestDestinationFolderIfDoesNotExist()
	{
		if (file_exists($this->manifestDestinationPath)) {
			return;
		}

		mkdir($this->manifestDestinationPath);
	}
}