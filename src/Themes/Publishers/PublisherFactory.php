<?php

namespace App\Themes\Publishers;

class PublisherFactory
{
	public static function makeWithBackup($type, $onlyUsed = false)
	{
		return new BackupPublisher(
			static::make($type, $onlyUsed)
		);
	}
	
	public static function make($type, $onlyUsed = false)
	{
		switch ($type)
		{
			case 'symlink':
				return new SymlinkPublisher;
			case 'copy':
				return new CopyPublisher;
			case 'optimized':
				return new ProcessedPublisher($onlyUsed);
			default:
				throw new \Exception('Please provide a valid publisher.');
		}
	}
}