<?php

namespace App\Themes;

use App\Themes\Publishers\Backup;
use App\Themes\Publishers\BackupPublisher;
use App\Themes\Publishers\Publisher;
use App\Themes\Publishers\SymlinkPublisher;
use App\Themes\Publishers\Manifests\Manifest;
use App\Themes\Publishers\Manifests\ManifestFactory;
use App\Generators\FileSystem;

class ThemeAssets
{
	private $themeProvider;
	private $themeName;
	private $publisher;
	private $destinationFolder;

	public function __construct($themeName, Publisher $publisher = null)
	{
		$this->themeProvider = app()->getThemeProvider($themeName);
		$this->themeName = $themeName;
		$this->publisher = $publisher;
	}

	public function setDestinationFolder($destinationFolder)
	{
		$this->destinationFolder = $destinationFolder;

		return $this;
	}

	private function getDestinationFolder()
	{
		return $this->destinationFolder 
			? $this->getCustomDestinationFolder()
			: $this->getDefaultDestinationFolder();
	}

	public function publish(Publisher $publisher = null)
	{
		$this->guardPublisher($publisher);

		$this->createDefaultDestinationRootIfDoesNotExist();

		$manifest = ManifestFactory::assets();
		
		$result = $this->getBackupPublisher($publisher)
			->publish(
				$manifest,
				$this->getSourceFolder(), 
				$this->getDestinationFolder()
			);

		$manifest->publish();

		return $result;
	}

	private function guardPublisher(Publisher $publisher = null)
	{
		if ( ! $this->getQualifiedPublisher($publisher)) {
			throw new \Exception('Please provide a publisher');
		}
	}

	private function getBackupPublisher($publisher)
	{
		return new BackupPublisher(
			$this->getQualifiedPublisher($publisher)
		);
	}

	private function getQualifiedPublisher(Publisher $publisher = null)
	{
		return $publisher ?: $this->publisher;
	}

	public function getSourceFolder()
	{
		return "{$this->themeProvider->getBasePath()}/assets";
	}

	private function getCustomDestinationFolder()
	{
		return "{$this->destinationFolder}/{$this->themeName}";
	}

	private function getDefaultDestinationFolder()
	{
		return "{$this->getDefaultDestinationRoot()}/{$this->themeName}";
	}

	public function getUrl($relativePath)
	{
		$baseUrl = env('BASE_URL');
		return "{$baseUrl}/_assets/{$this->themeName}/{$relativePath}";
	}

	private function createDefaultDestinationRootIfDoesNotExist()
	{
		return app(FileSystem::class)->createFolderIfDoesNotExist(
			$this->getDefaultDestinationRoot()
		);
	}

	private function getDefaultDestinationRoot()
	{
		$basePath = env('BASE_PATH');
		return "{$basePath}/public/_assets";
	}
}