<?php

namespace App\Themes;

interface ThemeProvider
{
	public function getBasePath();
}