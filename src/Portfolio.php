<?php

namespace App;

use App\Http\WebsiteController;
use App\Domain\Post;
use App\Repositories\PostCollection;
use App\Repositories\PostsRepository;
use App\Repositories\Registry;
use App\Themes\ThemeAssets;
use Symfony\Component\HttpFoundation\Request;

class Portfolio
{
	private $basePath;
	private $projects;
	private $pages;
	private $links = [];
	private $settings;

	public function __construct($basePath) 
	{
		$this->basePath = $basePath;

		$this->projects = new PostCollection(
			app(Registry::class)->projects()
		);
		
		$this->pages = new PostCollection(
			app(Registry::class)->pages()
		);

		$this->links = app(Registry::class)->links()->all();
		
		$this->settings = new Domain\Settings;
		$this->settings->load(
			app(Registry::class)->settings()->all()
		);
	}

	public function autoDiscovery()
	{
		(new PortfolioAutoDiscovery($this))->discover();

		return $this;
	}

	public function projects()
	{
		return $this->projects;
	}

	public function pages()
	{
		return $this->pages;
	}

	public function links()
	{
		return $this->links;
	}

	public function settings()
	{
		return $this->settings;
	}

	public function getProject($slug) : Post
	{
		return $this->projects()->find($slug);
	}

	public function getPage($slug) : Post
	{
		return $this->pages()->find($slug);
	}

	public function getHomePost() : Post
	{
		if ($this->settings()->has('defaultSlug')) {
			return $this->pages()->find(
				$this->settings()->defaultSlug
			);
		}

		return new Post;
	}

	public function getGallery() : PostCollection
	{
		return $this->projects()->visibleOnly();
	}

	public function getMenu() : PostCollection
	{
		return $this->pages()
			->visibleOnly()
			->exceptSlug(
				$this->settings()->defaultSlug
			);
	}

	public function getLinks()
	{
		return $this->links();
	}

	public function getBasePath()
	{
		return $this->basePath;
	}

	public function generate()
	{
		return new PortfolioGenerator($this);
	}

	public function run(Request $request = null)
	{
		return (new WebsiteController)->run($request);
	}

	public function themeAssets()
	{
		if ( ! $this->settings()->has('theme')) {
			throw new \Exception('Please provide a theme.');
		}

		return new ThemeAssets($this->settings()->theme);
	}
}