<?php

namespace App\Generators;

trait HasFolioManifest
{
    private $folioManifest;
    
    public function setFolioManifest(FolioManifest $folioManifest)
    {
        $this->folioManifest = $folioManifest;

        return $this;
    }

    public function getFolioManifest()
    {
        return $this->folioManifest;
    }
}