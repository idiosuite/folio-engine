<?php

namespace App\Generators\Php;

use App\Portfolio;
use App\Generators\Generator;
use App\Generators\FileSystem;

class PhpGenerator implements Generator
{
	private $portfolio;
	private $destinationPath;

	public function __construct(Portfolio $portfolio, $destinationPath)
	{
		$this->portfolio = $portfolio;
		$this->destinationPath = $destinationPath;
	}
	
	public function generate()
	{
		$this->createIndexFile();
	}

	private function createIndexFile()
	{
		app(FileSystem::class)->createFile(
			$this->getIndexPhpPath(),
			'<?php
require_once "../bootstrap.php";
app(App\Portfolio::class)->run();'
		);
	}

	private function getIndexPhpPath()
	{
		return $this->destinationPath . '/index.php';
	}
}