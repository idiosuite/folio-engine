<?php

namespace App\Generators\Json;

use App\Generators\Generator;
use App\Generators\FileSystem;
use App\Portfolio;

class JsonGenerator implements Generator
{
	private $portfolio;
	private $destinationPath;
	
	public function __construct(Portfolio $portfolio, $destinationPath)
	{
		$this->portfolio = $portfolio;
		$this->destinationPath = "{$destinationPath}/json";
	}
	
	public function generate()
	{
		$this->createDestinationFolderIfDoesNotExist();
		
		foreach ($this->getFileDelegates() as $className) {
			(new $className(
				$this->portfolio, 
				$this->destinationPath
			))->generate();
		}
	}

	private function getFileDelegates()
	{
		return [
			ProjectsJsonFile::class,
			PagesJsonFile::class,
			SettingsJsonFile::class,
			LinksJsonFile::class,
			RuntimeRepositoryJsonFiles::class
		];
	}

	private function createDestinationFolderIfDoesNotExist()
	{
		app(FileSystem::class)->createFolderIfDoesNotExist(
			$this->destinationPath
		);
	}
}