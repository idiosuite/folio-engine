<?php

namespace App\Generators\Json;

use App\Generators\Generator;
use App\Portfolio;

class RuntimeRepositoryJsonFiles implements Generator
{
	private $destinationPath;

	public function __construct(Portfolio $portfolio, $destinationPath)
	{
		$this->destinationPath = $destinationPath;
	}

	public function generate()
	{
		foreach ($this->getRepositoryKeysRegisteredAtRuntime() as $repositoryKey) {
			$this->getGenerator($repositoryKey)->generate();
		}
	}

	private function getGenerator($repositoryKey)
	{
		return new RepositoryJsonFile(
			$this->destinationPath, 
			$repositoryKey
		);
	}

	private function getRepositoryKeysRegisteredAtRuntime()
	{
		return array_values(
			array_diff(
				registry()->getRepositoryKeys(), 
				$this->getFirstClassCitizens()
			)
		);
	}

	private function getFirstClassCitizens()
	{
		return ['settings', 'links', 'projects', 'pages'];
	}
}