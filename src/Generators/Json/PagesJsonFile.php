<?php

namespace App\Generators\Json;

use App\Generators\Generator;
use App\Portfolio;

class PagesJsonFile extends JsonFile
{
	protected function getFilename() : string
	{
		return 'pages.json';
	}

	protected function getArray() : array
	{
		return [
			'pages' => $this->portfolio->pages()->visibleOnly()->apiResource()->toArray()
		];
	}
}