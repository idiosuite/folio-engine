<?php

namespace App\Generators\Json;

use App\Generators\Generator;
use App\Portfolio;

class SettingsJsonFile extends JsonFile
{
	protected function getFilename() : string
	{
		return 'settings.json';
	}

	protected function getArray() : array
	{
		return [
			'settings' => $this->portfolio->settings()->apiResource()->toArray()
		];
	}
}