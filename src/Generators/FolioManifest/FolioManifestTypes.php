<?php

namespace App\Generators\FolioManifest;

class FolioManifestTypes
{
	const TYPE_FILE = 'file';
	const TYPE_SYMLINK = 'symlink';
	const TYPE_FOLDER = 'folder';

	public function isValid(string $type = null)
	{
		return in_array($type, $this->getAll());
	}

	public function getAll()
	{
		return [
			static::TYPE_FOLDER, 
			static::TYPE_FILE, 
			static::TYPE_SYMLINK
		];
	}

	public function guessFromPath($absolutePath)
	{
		if (is_link($absolutePath)) {
			return static::TYPE_SYMLINK;
		}
		
		if (is_file($absolutePath)) {
			return static::TYPE_FILE;
		}
		
		if (is_dir($absolutePath)) {
			return static::TYPE_FOLDER;
		}

		throw new \Exception('Could not determine type');
	}
}