<?php

namespace App\Generators;

use \App\Generators\FolioManifest;

class FileSystem
{
    public function createFolderIfDoesNotExist($destination)
    {
        if ($this->exists($destination)) {
            return;
        }

        $this->createFolder($destination);
    }

    public function createFolderOrFail($destination)
    {
        if ( ! $this->createFolder($destination)) {
			throw new \Exception('Could not create destination ' . $this->destination);
		}
    }

    public function exists($path)
    {
        return file_exists($path);
    }

    public function createFolder($destination)
    {
        $result = mkdir($destination);
		
        app(FolioManifest::class)->addFolder($destination);

        return $result;
    }

    public function createSymlink($source, $destination)
    {
        $result = @symlink($source, $destination);
		
        app(FolioManifest::class)->addSymlink($destination);
        
        return $result;
    }

    public function createFile($destination, $contents)
    {
        $result = file_put_contents($destination, $contents);

        app(FolioManifest::class)->addFile($destination);

        return $result;
    }

    public function copyFile($source, $destination)
    {
        copy($source, $destination);
		
        app(FolioManifest::class)->addFile($destination);
    }

    public function getFilesInFolder($path) 
    {
        return array_values(
			array_diff(
				scandir($path), 
				['.', '..']
			)
		);
    }
}