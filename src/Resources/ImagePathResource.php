<?php

namespace App\Resources;

use App\Domain\ImageFactory;

class ImagePathResource
{
	private $value;

	public function __construct($value)
	{
		$this->value = $value;
	}

	public static function hydrate($value)
	{
		return new static($value);
	}

	public function get()
	{
		return $this->getValueRecursive($this->value);
	}

	public function toArray()
	{
		return $this->getValueRecursive($this->value, $asImageArray = true);
	}

	private function getValueRecursive($value, $asImageArray = false)
	{
		if ($this->isImageArray($value)) {
			return $asImageArray 
				? $value 
				: $this->getImage($value['src']);
		}

		if ($this->isArray($value)) {
			foreach ($value as $k => $v) {
				$value[$k] = $this->getValueRecursive($v, $asImageArray);
			}
			return $value;
		}

		if ($this->isImageInstance($value) && $asImageArray) {
			return $value->toArray();
		}
		
		if ($this->isImagePath($value)) {
			return $asImageArray 
				? $this->getImage($value)->toArray() 
					?: ImageFactory::makeNull()->toArray()
				: $this->getImage($value);
		}

		return $value;
	}

	private function isArray($value)
	{
		return is_array($value);
	}

	private function isImageArray($value)
	{
		return $this->isArray($value) && 
			!! array_intersect(['src', 'width', 'height'], array_keys($value));
	}

	private function isImageInstance($value)
	{
		return $value instanceof \App\Domain\Images\Image;
	}

	private function getImage($value)
	{
		return ImageFactory::makeResponsive(
			$this->trimBasePath($value)
		);
	}

	private function trimBasePath($value)
	{
		return str_replace(
			env('IMAGES_SOURCE_PATH'), 
			'', 
			$value
		);
	}

	private function isImagePath($value)
	{
		return is_string($value) && in_array(
			$this->getLastFourCharacters($value), 
			$this->getSupportedImageExtensions()
		);
	}

	private function getLastFourCharacters($value)
	{
		return substr($value, -4);
	}

	private function getSupportedImageExtensions()
	{
		return ['.jpg', '.png', '.svg'];
	}
}