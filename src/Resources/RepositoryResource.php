<?php

namespace App\Resources;

use App\Repositories\Repository;

class RepositoryResource
{
	private $repository;

	public function __construct(Repository $repository)
	{
		$this->repository = $repository;
	}

	public function toArray()
	{
		return ImagePathResource::hydrate(
			$this->repository->all()
		)->toArray();
	}
}