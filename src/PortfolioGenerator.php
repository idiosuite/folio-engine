<?php

namespace App;

use App\Themes\Theme;
use Psr\Log\LoggerInterface;

class PortfolioGenerator
{
	private $portfolio;
	
	public function __construct(Portfolio $portfolio)
	{
		$this->portfolio = $portfolio;
	}

	public function html($destinationPath = null, LoggerInterface $log = null)
	{
		return (new Generators\Html\HtmlGenerator(
			$this->portfolio, 
			$destinationPath ?: $this->getPublicPath(),
			$log
		))->generate();
	}

	public function json($destinationPath = null, LoggerInterface $log = null)
	{
		return (new Generators\Json\JsonGenerator(
			$this->portfolio, 
			$destinationPath ?: $this->getPublicPath(),
			$log
		))->generate();
	}

	public function develop($destinationPath = null, LoggerInterface $log = null)
	{
		return (new Generators\Php\PhpGenerator(
			$this->portfolio,
			$destinationPath ?: $this->getPublicPath(),
			$log
		))->generate();
	}
	
	private function getPublicPath()
	{
		return $this->portfolio->getBasePath() . '/public';
	}
}