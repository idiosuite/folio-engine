<?php

namespace App\Http;

use \Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class Response extends SymfonyResponse
{
	public function __toString()
	{
		return $this->getContent();
	}
}