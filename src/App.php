<?php

namespace App;

use App\Context;
use App\Portfolio;
use Pimple\Container;
use App\Views\ClientsGrid;
use Jenssegers\Blade\Blade;
use App\Generators\FileSystem;
use App\Repositories\Registry;
use App\Repositories\Repository;
use App\Generators\FolioManifest;
use App\Views\OpenGraph\OpenGraph;
use App\Views\OpenGraph\BaseOpenGraph;
use App\Views\OpenGraph\PostOpenGraph;
use App\Interpolators\InterpolatorsBag;
use App\Views\OpenGraph\OpenGraphConfig;
use App\Themes\Publishers\Manifests\Manifest;
use App\Themes\Publishers\Manifests\ManifestFactory;
use App\Themes\Publishers\Images\Used\UsedImagesFinder;
use App\Themes\Publishers\Images\Crops\CroppedImagesMap;

class App
{
	private static $instance;
	private $container;
	private $registryFactory;
	private $viewFactory;

	private function __construct()
	{
		$this->viewFactory = new Views\ViewFactory;
		$this->registryFactory = new Repositories\RegistryFactory;

		$this->bootstrap();
	}

	public function bootstrap()
	{
		$this->container = new Container();

		$this->container[Portfolio::class] = function ($c) {
		    $portfolio = new Portfolio(env('BASE_PATH'));

		    return $portfolio->autoDiscovery();
		};

		$this->container[FolioManifest::class] = function ($c) {
			return new FolioManifest;
		};

		$this->container[FileSystem::class] = function ($c) {
			return new FileSystem;
		};

		$this->container[Registry::class] = function ($c) {
			return $this->registryFactory->make();
		};

		$this->container[Blade::class] = function ($c) {
			return $this->viewFactory->make(
				$c[Portfolio::class]->settings()->theme
			);
		};

		$this->container[Manifest::class] = function ($c) {
			return ManifestFactory::images();
		};

		$this->container[InterpolatorsBag::class] = function ($c) {
			return new InterpolatorsBag;
		};

		$this->container[UsedImagesFinder::class] = function ($c) {
			return new UsedImagesFinder;
		};

		$this->container[CroppedImagesMap::class] = function ($c) {
			return (new CroppedImagesMap)->load();
		};

		$this->container[OpenGraph::class] = function ($c) {
			return (new BaseOpenGraph)->bootstrapWithCallback([ 
				$c[Portfolio::class]->settings()
			]);
		};

		$this->container[OpenGraphConfig::class] = function ($c) {
			return new OpenGraphConfig;
		};

		$this->container[Context::class] = function ($c) {
			return new Context;
		};

		$this->container[ClientsGrid::class] = function ($c) {
			return ClientsGrid::fromRegistry();
		};
	}

	public static function bootstrapThemes(array $themeProviders)
	{
		static::instance()->viewFactory->setThemeProviders($themeProviders);
	}

	public static function bootstrapImagesWhitelist(array $whitelist)
	{
		static::instance()
			->get(UsedImagesFinder::class)
			->whitelist()
			->set($whitelist);
	}

	public static function bootstrapImagesFormats(array $mapArray)
	{
		static::instance()
			->get(CroppedImagesMap::class)
			->addArrayMap($mapArray);
	}

	public static function bootstrapOpenGraph(
		\Closure $baseCallback = null, 
		\Closure $postCallback = null
	) {
		if ($baseCallback) {
			app(OpenGraphConfig::class)->customize(
				BaseOpenGraph::class, 
				$baseCallback
			);
		}

		if ($postCallback) {
			app(OpenGraphConfig::class)->customize(
				PostOpenGraph::class, 
				$postCallback
			);
		}
	}

	public function getThemeProvider($themeId)
	{
		return $this->viewFactory->getThemeProvider($themeId);
	}

	public static function bootstrapRegistry(Registry $registry)
	{
		static::instance()->registryFactory->setRegistry($registry);
	}

	public static function instance()
	{
		if ( ! static::$instance) {
			static::$instance = new static;
		}

		return static::$instance;
	}

	public static function destroy()
	{
		static::$instance = null;
	}

	public function get($identifier)
	{
		return $this->container[$identifier];
	}

	public function env($is) 
	{
		return env('APP_ENV') === $is;
	}
}