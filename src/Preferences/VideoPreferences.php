<?php

namespace App\Preferences;

use App\Domain\Post;

class VideoPreferences
{
	private $preferences = [];
	private $provider;
	private $post;

	public function __construct(Post $post, $provider)
	{
		$this->post = $post;
		$this->preferences = $post->preferences ?: [];
		$this->provider = $provider;
	}

	public function get($id)
	{
		$preferences = $this->getDefaultPreferences();
		$preferences = $this->getCustomPreferencesWithId($preferences, $id);
		$preferences = $this->getCustomPreferencesWithSlug($preferences, $id);
		$preferences = $this->overrideSizeAccordingToFormat($preferences);

		return $preferences;
	}

	private function getDefaultPreferences()
	{
		return array_key_exists($this->provider, $this->preferences) 
			? $this->preferences[$this->provider]
			: [
				'autoplay' => false,
				'loop' => false,
				'size' => [960, 540],
				'format' => '16:9'
			];
	}

	private function getCustomPreferencesWithSlug($preferences, $id)
	{
		$video = $this->post->videos
			->filterById($id)	
			->filterByProvider($this->provider)
			->getFirst();
		
		return $this->getCustomPreferencesWithId($preferences, $video->slug);
	}

	private function getCustomPreferencesWithId($preferences, $id)
	{
		$key = $this->provider . '.' . $id;
		
		if ( ! array_key_exists($key, $this->preferences)) {
			return $preferences;
		}

		return array_merge(
			$preferences,
			$this->preferences[$key]
		);
	}

	private function overrideSizeAccordingToFormat($preferences)
	{
		$formatMap = [
			'4:3' => [720, 540],
			'16:9' => [960, 540],
		];
		
		if (array_key_exists($preferences['format'], $formatMap)) {
			$preferences['size'] = $formatMap[$preferences['format']];
		}

		return $preferences;
	}
}