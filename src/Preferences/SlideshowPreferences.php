<?php

namespace App\Preferences;

use App\Domain\Post;

class SlideshowPreferences
{
	private $preferences = [];
	private $id;

	public function __construct(Post $post)
	{
		$this->preferences = $post->preferences ?: [];
	}

	public function get($id = null) : array
	{
		$this->id = $id;

		if ( ! $this->exists()) {
			return [];
		}

		return $this->preferences[$this->key()];
	}

	private function exists()
	{
		return array_key_exists(
			$this->key(), 
			$this->preferences
		);
	}

	private function key()
	{
		return sprintf('slideshow.%s', $this->id);
	}
}