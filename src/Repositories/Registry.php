<?php

namespace App\Repositories;

class Registry
{
	private $registry = [];
	
	public function __call($methodName, $args)
	{
		if (array_key_exists($methodName, $this->registry)) {
			return $this->registry[$methodName];
		}

		throw new \Exception("Please register repository '{$methodName}'");
	}

	public function register($key, $repository)
	{
		$this->registry[$key] = $repository instanceof Repository
			? new Hydrations\HydratedImagesRepository($repository)
			: $repository;
	}

	public function getRepositoryKeys() {
		return array_keys($this->registry);
	}
}