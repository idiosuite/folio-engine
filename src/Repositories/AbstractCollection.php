<?php

namespace App\Repositories;

use \App\Domain\Preview;
use App\Domain\DomainObject;
use App\Resources\CollectionResource;
use \App\Resources\ProvidesApiResource;
use \Illuminate\Contracts\Support\Arrayable;

abstract class AbstractCollection implements 
    \Iterator, 
    \Countable, 
    Arrayable, 
    ProvidesApiResource
{
    private $position = 0;
    protected $items = [];

    public function __construct(array $items = null)
	{
		$items = $items ?: [];

		$this->fill($items);
	}

	// Provides API Resource

	public function apiResource()
	{
		return new CollectionResource($this);
	}

    // Iterator implementation

    public function current() : mixed
	{
		return $this->items[$this->position];
	}

	public function next() : void
	{
		$this->position++;
	}

	public function key() : mixed
	{
		return $this->position;
	}

	public function rewind() : void
	{
		$this->position = 0;
	}

	public function valid() : bool
	{
		return isset($this->items[$this->position]);
	}

    // Countable Implementation

    public function count() : int
    {
        return count($this->items);
    }

	// Arrayable Implementation

    public function toJson()
	{
		return json_encode(
			$this->toArray()
		);
	}

	public function toArray()
	{
		return $this->items;
	}

    //

    public function isEmpty()
	{
		return $this->count() == 0;
	}

    public function getFirst() : DomainObject
	{
		return $this->getRow(0);
	}

    public function getRow($index) : DomainObject
	{
		if ($this->isEmpty()) {
			return $this->getNewDomainObjectInstance();
		}

		return $this->items[$index];
	}

    public function addWithArray(array $item)
	{
		return $this->add(
			$this->getNewDomainObjectInstance($item)
		);
	}

    public function add(DomainObject $domainObject)
	{
		$this->items[] = $domainObject;

		return $this;
	}

    public function flatten($key)
	{
		return array_map(
			function (DomainObject $domainObject) use ($key) {
				return $domainObject->$key;
			}, 
			$this->items
		);
	}

    private function fill(array $items)
	{
		$this->items = array_values(
			array_map(
				[$this, 'normalizeDomainObject'],
				$items
			)
        );

		return $this;
	}

    public function sortBy($field, $orderStr = 'asc')
	{
		$order = strtolower($orderStr) === 'asc' 
			? SORT_ASC 
			: SORT_DESC;

		$domainObjects = array_map(
			function (DomainObject $domainObject) use ($field) {
				if ( ! $domainObject->has($field)) {
					return [ $field => '' ];
				}
				return [ $field => $domainObject->$field ];
			}, 
			$this->items
		);

		$fieldColumn  = array_column($domainObjects, $field);

		array_multisort($fieldColumn, $order, $this->items);

		return $this;
	}

    protected function filterByClosure($closure)
	{
		return $this->getNewInstance()->fill(
            array_filter($this->items, $closure)
        );
	}

    protected function getNewInstance()
    {
        return new static;
    }

    //

    private function normalizeDomainObject($item) : DomainObject
	{
		$targetClass = $this->getTargetClass();

		if ($item instanceof $targetClass) {
			return $item;
		}

        return $this->getNewDomainObjectInstance($item);
	}

    private function getNewDomainObjectInstance(array $attr = []) : DomainObject
	{
		$className = $this->getTargetClass();
		
		return new $className($attr);
	}

    abstract protected function getTargetClass();
}