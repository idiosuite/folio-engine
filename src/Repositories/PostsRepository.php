<?php

namespace App\Repositories;

use App\Discovery\Slug;
use App\Domain\Post;

interface PostsRepository
{
	public function setType($type);
	public function getType();
	public function find($slug) : Post;
}