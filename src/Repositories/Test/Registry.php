<?php

namespace App\Repositories\Test;

use App\Repositories\Array\ArrayRepository;
use App\Repositories\Registry as BaseRegistry;

class Registry extends BaseRegistry
{
	public function __construct()
	{
		$this->register('projects', (new PostsRepository)->setType('project'));
		$this->register('pages', (new PostsRepository)->setType('page'));
		
		$this->register('links', new ArrayRepository([
			(object)[
				'slug' => 'google', 
				'text' => 'Google', 
				'url' => 'https://google.com'
			],
			(object)[
				'slug' => 'facebook', 
				'text' => 'Facebook', 
				'url' => 'https://facebook.com'
			],
			(object)[
				'slug' => 'twitter', 
				'text' => 'Twitter', 
				'url' => 'https://twitter.com'
			],
		]));

		$this->register('settings', new ArrayRepository([
			'title' => 'Title',
			'description' => 'Lorem ipsum dolor sit amet consectetur adipiscing elit.',
			'logo' => 'https://ideosuite.com/_img/logo.svg',
			'defaultSlug' => 'welcome',
			'footer' => 'Lorem ipsum dolor',
			'theme' => 'folio',
			'email' => 'info@example.com',
			'keywords' => [
				'foo',
				'bar',
				'baz'
			]
		]));
	}
}