<?php

namespace App\Repositories;

use \App\Domain\Video;

class VideoCollection extends AbstractCollection
{
	protected function getTargetClass()
	{
		return Video::class;
	}

	public function filterByProvider($provider)
	{
		return $this->filterByKey('provider', $provider);
	}

	public function filterById($id)
	{
		return $this->filterByKey('id', $id);
	}

    private function filterByKey($key, $value)
    {
        return $this->filterByClosure(
			function (Video $video) use ($key, $value) {
				return $video->$key == $value;
			}
		);
    }
}