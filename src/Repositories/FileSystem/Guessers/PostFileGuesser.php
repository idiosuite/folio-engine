<?php

namespace App\Repositories\FileSystem\Guessers;

use App\Parsers\FileParser;

class PostFileGuesser implements Guesser
{
	private $basePath;
	private $result = [];

	public function __construct($basePath, array $result) {
		$this->basePath = $basePath;
		$this->result = $result;
	}
	
	public function get() : array
	{
		$fileParser = new FileParser($this->getMarkdownFilePath());

		$this->result['exists'] = $fileParser->exists();
		$this->result['content'] = $fileParser->getMarkdown();

		return array_merge(
			$this->result,
			$fileParser->getMeta()
		);
	}

	private function getMarkdownFilePath()
	{
		return $this->getBasePath() . '/' . $this->result['slug']->getFilePath();
	}

	public function getBasePath()
	{
		return "{$this->basePath}/{$this->result['type']}s";
	}
}