<?php

namespace App\Repositories\FileSystem\Guessers;

use App\Domain\Preview;

class MultiplePreviewsGuesser implements Guesser
{
    private $basePath;
    private $previewsBasePath;
	private $result = [];

	public function __construct($basePath, array $result)
	{
        $this->basePath = $basePath;
		$this->previewsBasePath = $basePath . '/previews';
		$this->result = $result;
	}
    
	public function get() : array 
    {
        $this->result['previews'] = [];

        $this->addPreviewIfGuessed();
        $this->addHeroIfGuessed();
        $this->addPreviewsFromJsonFile();

        return $this->result;
    }

    private function addPreviewsFromJsonFile()
    {
        if ( ! $this->fileExists()) {
            return;
        }

        foreach ($this->getFileContents() as $preview) {
            $this->result['previews'][] = $this->normalizeSrcAttribute($preview);
        }
    }

    private function addPreviewIfGuessed()
    {
        return $this->addImageIfGuessed(PreviewGuesser::class);
    }

    private function addHeroIfGuessed()
    {
        return $this->addImageIfGuessed(HeroGuesser::class);
    }

    private function addImageIfGuessed($guesserClass)
    {
        $guesser = new $guesserClass($this->basePath, $this->result);
        $key = $guesser->getKey();
        $result = $guesser->get();

        if ( ! isset($result[$key])) {
            return;
        }

        $this->result['previews'][] = [
            'src' => $result[$key], 
            'type' => 'image',
            'media' => [$key]
        ];
    }

    private function normalizeSrcAttribute(array $preview)
    {
        $preview['src'] = trim($preview['src'], '/');

        if ($this->isImageFileName($preview)) {
            $preview['src'] = $this->getSlugFolderPath() . $preview['src'];
        }

        return $preview;
    }

    private function isImageFileName(array $preview)
    {
        return $preview['type'] == 'image' && 
            ! $this->startsWithSlugFolder($preview);
    }

    private function startsWithSlugFolder(array $preview)
    {
        return stripos($preview['src'], $this->getSlugFolderPath()) === 0;
    }

    private function getSlugFolderPath()
    {
        return "{$this->result['slug']}/";
    }

    private function getFileContents()
    {
        return json_decode(
            file_get_contents($this->getFilePath()), 
            $assoc = true
        );
    }

    private function fileExists()
    {
        return file_exists($this->getFilePath());
    }

    private function getFilePath()
    {
        return "{$this->previewsBasePath}/{$this->result['slug']}.json";
    }
}