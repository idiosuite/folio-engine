<?php

namespace App\Repositories\FileSystem\Guessers;

use App\Parsers\TextBlocksParser;

class TextsGuesser implements Guesser 
{
	const KEY_CREDITS = 'credits';
	const KEY_TEXTS = 'texts';

	private $basePath;
	private $result = [];
	private $parser;

	public function __construct($basePath, array $result)
	{
		$this->basePath = $basePath;
		$this->result = $result;
	}

	public function get() : array
	{
		$this->parser = TextBlocksParser::fromString(
			$this->getFileContents(),
			$defaultSlug = static::KEY_CREDITS
		);

		$this->result[static::KEY_TEXTS] = $this->getAll();
		$this->result[static::KEY_CREDITS] = $this->getCredits();
		
		return $this->result;
	}

	private function getAll()
	{
		$result = [];
		
		foreach ($this->parser->get() as $block) {
			$result[$block->getSlug()]  = $block->getBody();
		}

		return $result;
	}

	private function getCredits()
	{
		$block = $this->parser->getWithSlug(static::KEY_CREDITS);

		if ( ! $block) {
			return;
		}

		return $block->getBody();
	}

	private function getFileContents()
	{
		if ( ! file_exists( $this->getCreditsPath() )) {
			return;
		}
		
		return file_get_contents( $this->getCreditsPath() );
	}

	private function getCreditsPath()
	{
		return sprintf(
			'%s/credits/%s', 
			$this->basePath,
			$this->result['slug']->getFilePath()
		);
	}
}