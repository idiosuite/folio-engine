<?php

namespace App\Repositories\FileSystem\Guessers;

class PathGuesser implements Guesser
{
	private $basePath;
	private $result = [];

	public function __construct($basePath, array $result)
	{
		$this->basePath = $basePath;
		$this->result = $result;
	}

	public function get() : array 
	{
		$this->result['path'] = $this->getPath();

		return $this->result;
	}

	private function getPath()
	{
		return sprintf(
			'/%s/%s', 
			$this->result['type'], 
			$this->result['slug']->getPath()
		);
	}
}