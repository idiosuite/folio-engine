<?php

namespace App\Repositories\FileSystem\Guessers;

class HeroGuesser extends AbstractImageGuesser
{
	public function getKey()
	{
		return 'hero';
	}
}