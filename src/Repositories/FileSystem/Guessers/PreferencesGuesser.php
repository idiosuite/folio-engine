<?php

namespace App\Repositories\FileSystem\Guessers;

class PreferencesGuesser implements Guesser
{
	private $basePath;
	private $result = [];

	public function __construct($basePath, array $result)
	{
		$this->basePath = $basePath;
		$this->result = $result;
	}

	public function get() : array
	{
		$this->result['preferences'] = $this->getPreferences();

		return $this->result;
	}

	private function getPreferences() : array
	{
		$preferences = $this->getDefaultPreferences();

		if ($this->qualifiedPreferencesFileExists()) {
			$preferences = array_merge_recursive(
				$preferences, 
				$this->getQualifiedPreferences()
			);
		}

		return $preferences;
	}

	private function getDefaultPreferences() : array
	{
		return json_decode(
			file_get_contents($this->getDefaultsFilePath()), 
			$assoc = true
		);
	}

	private function getQualifiedPreferences() : array
	{
		return json_decode(
			file_get_contents($this->getQualifiedFilePath()), 
			$assoc = true
		);
	}

	private function qualifiedPreferencesFileExists()
	{
		return file_exists($this->getQualifiedFilePath());
	}

	private function getQualifiedFilePath()
	{
		return sprintf(
			'%s/preferences/%s.json', 
			$this->basePath, 
			$this->result['slug']->getOriginal()
		);
	}

	private function getDefaultsFilePath()
	{
		return sprintf(
			'%s/preferences/defaults.json', 
			$this->basePath
		);
	}
}