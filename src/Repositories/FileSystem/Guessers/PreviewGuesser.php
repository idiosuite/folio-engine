<?php

namespace App\Repositories\FileSystem\Guessers;

class PreviewGuesser extends AbstractImageGuesser
{
	public function getKey()
	{
		return 'preview';
	}
}