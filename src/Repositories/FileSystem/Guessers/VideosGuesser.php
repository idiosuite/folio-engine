<?php

namespace App\Repositories\FileSystem\Guessers;

use App\Domain\Preview;

class VideosGuesser implements Guesser
{
    private $basePath;
    private $videosBasePath;
	private $result = [];

	public function __construct($basePath, array $result)
	{
        $this->basePath = $basePath;
		$this->videosBasePath = $basePath . '/videos';
		$this->result = $result;
	}
    
	public function get() : array 
    {
        $this->result['videos'] = [];

        $this->addVideosFromJsonFile();

        return $this->result;
    }

    private function addVideosFromJsonFile()
    {
        if ( ! $this->fileExists()) {
            return;
        }

        foreach ($this->getFileContents() as $video) {
            $this->result['videos'][] = $video;
        }
    }

    private function getFileContents()
    {
        return json_decode(
            file_get_contents($this->getFilePath()), 
            $assoc = true
        );
    }

    private function fileExists()
    {
        return file_exists($this->getFilePath());
    }

    private function getFilePath()
    {
        return "{$this->videosBasePath}/{$this->result['slug']}.json";
    }
}