<?php

namespace App\Repositories\FileSystem\Guessers;

class LayoutGuesser implements Guesser
{
	private $basePath;
	private $result = [];
	
	public function __construct($basePath, array $result)
	{
		$this->basePath = $basePath;
		$this->result = $result;
	}
	
	public function get() : array
	{
		if ($this->hasValidHardcodedLayout()) {
			return $this->result;
		}

		$this->result['layout'] = $this->result['type'];

		return $this->result;
	}

	private function hasValidHardcodedLayout() {
		return isset($this->result['layout']) && ! empty($this->result['layout']);
	}
}