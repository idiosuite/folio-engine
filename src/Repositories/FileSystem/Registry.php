<?php

namespace App\Repositories\FileSystem;

use App\Repositories\Json\JsonRepository;
use App\Repositories\Registry as BaseRegistry;

class Registry extends BaseRegistry
{
	private $basePath;
	
	public function __construct($basePath)
	{
		$this->basePath = $basePath;
		$this->register('clients', (new ClientsRepository($basePath)));
		$this->register('links', new JsonRepository($basePath . '/links.json', $assoc = false));
		$this->register('settings', new JsonRepository($basePath . '/settings.json'));
		$this->register('projects', (new PostsRepository($basePath))->setType('project'));
		$this->register('pages', (new PostsRepository($basePath))->setType('page'));
	}

	public function getBasePath()
	{
		return $this->basePath;
	}
}