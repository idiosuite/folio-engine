<?php

namespace App\Repositories\FileSystem;

use App\Discovery\Slug;
use App\Repositories\AbstractPostsRepository;

class PostsRepository extends AbstractPostsRepository
{
	private $basePath;
	private $slug;

	public function __construct($basePath)
	{
		$this->basePath = $basePath;
	}

	protected function doFind(Slug $slug) : array
	{
		$result = [
			'type' => $this->type,
			'slug' => $slug,
		];

		$delegates = [
			Guessers\PostFileGuesser::class,
			Guessers\PathGuesser::class,
			Guessers\PreferencesGuesser::class,
			Guessers\TextsGuesser::class,
			Guessers\MultiplePreviewsGuesser::class,
			Guessers\VideosGuesser::class,
			Guessers\LayoutGuesser::class,
		];

		foreach ($delegates as $delegate) {
			$result = (new $delegate($this->basePath, $result))->get();
		}

		return $result;
	}
}