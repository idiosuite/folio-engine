<?php

namespace App\Repositories\FileSystem;

class PathInspector
{
	private $basePath;
	private $path;

	public function __construct($path)
	{
		$this->basePath = env('BASE_PATH');
		$this->path = $path;
	}
	
	public static function fromPath($path)
	{
		return new static($path);
	}

	public function toString()
	{
		return $this->path;
	}

	public function getBasePath()
	{
		$basePath = str_replace(
			$this->getFileName(), 
			'', 
			$this->path
		);
		
		return rtrim($basePath, '/');
	}

	public function getRelativePath()
	{
		$relativePath = str_replace(
			$this->basePath,
			'',
			$this->path
		);

		return trim($relativePath, '/');
	}

	public function getFileNameWithoutExtension()
	{
		$parts = explode('.', $this->getFileName());

		$extension = array_pop($parts);

		return implode('.', $parts);
	}

	public function getExtension()
	{
		$parts = explode('.', $this->getFileName());

		$extension = array_pop($parts);

		return $extension;
	}

	public function getFileName()
	{
		return last(
			explode('/', $this->path)
		);
	}

	public function getCheckSum()
	{
		return md5(
			file_get_contents($this->path)
		);
	}

	public function isDSStoreFile()
	{
		return $this->getFileName() === '.DS_Store';
	}
}