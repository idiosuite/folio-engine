<?php

namespace App\Repositories;

use App\Domain\Client;

interface ClientsRepository
{
	public function findBySlug($slug) : Client;
}