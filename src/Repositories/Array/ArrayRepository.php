<?php

namespace App\Repositories\Array;

use Adbar\Dot;
use App\Repositories\Repository;
use App\Resources\ProvidesApiResource;
use App\Resources\RepositoryResource;

class ArrayRepository extends Dot implements Repository, ProvidesApiResource
{
	public function apiResource()
	{
		return new RepositoryResource($this);
	}
}