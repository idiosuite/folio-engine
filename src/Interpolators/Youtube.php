<?php

namespace App\Interpolators;

use App\Domain\Post;
use App\Preferences\VideoPreferences;
use App\Interpolators\Options\YouTubeOptions;

class Youtube extends VideoEmbed
{
	protected function getRegexPattern()
	{
		return '/{youtube:([a-zA-Z0-9_\-]+)}/';
	}
	
	protected function getEmbedIframe()
	{
		return '<iframe 
			width="{width}" 
			height="{height}" 
			src="https://www.youtube.com/embed/{id}?{optionsQuery}" 
			title="YouTube video player" 
			frameborder="0" 
			allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" 
			allowfullscreen
		></iframe>';
	}

	protected function getPreferencesInstance()
	{
		return new VideoPreferences($this->getPost(), 'youtube');
	}

	protected function getOptionsInstance()
	{
		return new YouTubeOptions;
	}
}