<?php

namespace App\Interpolators;

class Credits implements Interpolator
{
	public function text($content)
	{
		$content = str_replace('{credits}', '<div class="Credits">', $content);
		$content = str_replace('{/credits}', '</div>', $content);
		return $content;
	}
}