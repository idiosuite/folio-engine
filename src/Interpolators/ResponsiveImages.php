<?php

namespace App\Interpolators;

use App\Domain\ImageFactory;
use App\Domain\Post;
use App\Themes\Publishers\Manifests\Manifest;

class ResponsiveImages implements Interpolator
{
	private $manifest;

	public function __construct()
	{
		$this->manifest = app(Manifest::class)->loadIfNotLoaded();
	}

	public function text($content)
	{
		return preg_replace_callback(
			$this->getImageTagRegex(), 
			[$this, 'getImageMarkup'], 
			$content
		);
	}

	private function getImageMarkup(array $matches)
	{
		if ( ! $this->isResponsiveImage($matches)) {
			return $this->getOriginalImageMarkup($matches);
		}

		return $this->getResponsiveImageMarkup($matches);
	}

	private function getResponsiveImageMarkup(array $matches)
	{
		return ImageFactory::makeResponsive(
			$this->getRelativePath($imageUrl = $matches[1])
		)->toHtml();
	}

	private function getOriginalImageMarkup(array $matches)
	{
		return $originalImageMarkup = $matches[0];
	}

	private function getImageTagRegex()
	{
		$allowedCharacters = '[\w\d\/\-\.\:]+';
		
		return sprintf(
			'/<img src="(%s)" alt="%s" \/>/',
			$allowedCharacters,
			$allowedCharacters
		);
	}

	private function isResponsiveImage($matches)
	{
		return $this->manifest->has(
			$this->getAbsolutePath($imageUrl = $matches[1])
		);
	}

	private function getAbsolutePath($url)
	{
		return str_replace(
			env('IMAGES_DESTINATION_URL'), 
			env('IMAGES_SOURCE_PATH'), 
			$url
		);
	}

	private function getRelativePath($url)
	{
		return str_replace(
			env('IMAGES_DESTINATION_URL'),
			'',
			$url
		);
	}
}