<?php

namespace App\Interpolators;

use App\Domain\Post;
use App\Preferences\SlideshowPreferences;

class Slideshow implements Interpolator, RequiresPost
{
	use HasPost;
	
	private $id = null;

	public function __construct(Post $post = null)
	{
		$this->setPost($post ?: new Post);
	}

	public function text($content)
	{
		$content = $this->replaceAllOpeningTokens($content);
		$content = $this->replaceAllClosingTokens($content);

		return $content;
	}

	private function replaceAllOpeningTokens($content)
	{
		return preg_replace_callback(
			'/{slideshow(:([a-zA-Z0-9\-_]+))?}/', 
			[$this, 'replaceOpeningToken'], 
			$content
		);
	}

	private function replaceAllClosingTokens($content)
	{
		return str_replace('{/slideshow}', '</span>', $content);
	}

	private function replaceOpeningToken($matches)
	{
		$this->setId($matches);
		
		return sprintf(
			'<span class="Slideshow"%s>',
			$this->getDataOptionsAttribute()
		);
	}

	private function getDataOptionsAttribute()
	{
		if ( ! $this->hasPreferences()) {
			return;
		}

		return sprintf(
			' data-options="%s"', 
			$this->getEscapedPreferences()
		);
	}

	private function hasPreferences()
	{
		return !! $this->getPreferences();
	}

	private function getEscapedPreferences()
	{
		return htmlentities($this->getJsonPreferences());
	}

	private function getJsonPreferences()
	{
		return json_encode($this->getPreferences());
	}

	private function getPreferences()
	{
		return array_merge(
			(new SlideshowPreferences($this->post))->get('*'), 
			(new SlideshowPreferences($this->post))->get($this->getId())
		);
	}
	
	private function setId($matches)
	{
		$this->id = isset($matches[2])
			? $matches[2] 
			: null;
	}

	private function getId()
	{
		return $this->id;
	}
}