<?php

namespace App\Interpolators;

use App\Domain\Post;
use App\Preferences\YoutubePreferences;
use App\Interpolators\Options\VimeoOptions;

abstract class VideoEmbed implements Interpolator, RequiresPost
{
	use HasPost;
	
	public function __construct(Post $post = null)
	{
		$this->setPost($post ?: new Post);
	}

	public function text($content)
	{
		preg_match_all($this->getRegexPattern(), $content, $matches);
		
		foreach ($matches[0] as $i => $find) {
			$id = $matches[1][$i];
			$content = str_replace($find, $this->getMarkup($id), $content);
		}
		
		return $content;
	}

	abstract protected function getRegexPattern();

	private function getMarkup($id)
	{
		$preferences = $this->getPreferences($id);

		$dictionary = [
			'{id}' => $id,
			'{aspectRatioClass}' => $this->getAspectRatioCssClass($preferences),
			'{aspectRatioStyle}' => $this->getAspectRatioStyle($preferences),
			'{optionsQuery}' => $this->getOptionsQuery($preferences),
			'{width}' => $this->getWidth($preferences),
			'{height}' => $this->getHeight($preferences)
		];

		return str_replace(
			array_keys($dictionary),
			array_values($dictionary),
			'<div class="Template__video {aspectRatioClass}" {aspectRatioStyle}>' . 
				$this->getEmbedIframe() . 
				'</div>'
		);
	}

	abstract protected function getEmbedIframe();

	private function getAspectRatioCssClass(array $preferences)
	{
		$aspectRatioClassMap = [
			'4:3' => 'Template__video--isPal',
			'16:9' => 'Template__video--isWidescreen'
		];

		if ( ! array_key_exists($preferences['format'], $aspectRatioClassMap)) {
			return '';
		}

		return $aspectRatioClassMap[$preferences['format']];
	}

	private function getAspectRatioStyle(array $preferences)
	{
		return sprintf(
			'style="height: 0; padding-top: %s%%;"', 
			$this->calculateAspectRatio($preferences)
		);
	}

	private function getOptionsQuery(array $preferences)
	{
		$preferences = $this->getOptionsInstance()->get($preferences);
		
		$options = [];

		foreach ($preferences as $key => $value) {
			if (is_bool($value)) {
				$value = $value ? '1' : '0';
			}

			$options[] = $key . '=' . $value;
		}

		return implode('&', $options);
	}

	private function getWidth(array $preferences)
	{
		return $preferences['size'][0];
	}

	private function getHeight(array $preferences)
	{
		return $preferences['size'][1];
	}

	private function calculateAspectRatio(array $preferences)
	{
		$number = $this->getHeight($preferences) / $this->getWidth($preferences) * 100;

		return round($number, 1);
	}

	private function getPreferences($id)
	{
		return $this->getPreferencesInstance()->get($id);
	}

	abstract protected function getPreferencesInstance();
	abstract protected function getOptionsInstance();
}