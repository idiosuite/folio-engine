<?php

namespace App\Interpolators\Options;

class VimeoOptions extends VideoOptions
{
	protected function getCustomizedPreferences(array $preferences)
	{
		if ( isset($preferences['loop']) && $preferences['loop']) {
			$preferences = [
				'background' => true
			];
		}

		return $preferences;
	}
}