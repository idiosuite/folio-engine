<?php

namespace App\Interpolators\Options;

class YouTubeOptions extends VideoOptions
{
	protected function getCustomizedPreferences(array $preferences)
	{
		if ( isset($preferences['loop']) && $preferences['loop']) {
			$preferences = [
				'loop' => true,
                'autoplay' => true
			];
		}

		return $preferences;
	}
}