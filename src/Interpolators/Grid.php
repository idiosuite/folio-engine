<?php

namespace App\Interpolators;

class Grid implements Interpolator
{
	private $lookupTable = [
        ['openingToken' => '@grid(cols:3)', 'classModifier' => 'cols-3'],
        ['openingToken' => '@grid(cols:2)', 'classModifier' => 'cols-2'],
        ['openingToken' => '@grid', 'classModifier' => 'cols-3'],
    ];

	public function text($content)
	{
		$content = $this->replaceAllOpeningTokens($content);
		$content = $this->replaceAllClosingTokens($content);

		return $content;
	}

	private function replaceAllOpeningTokens($content)
	{
        return str_replace(
            $this->getOpeningTokens(), 
            $this->getOpeningTags(), 
            $content
        );
	}

    private function replaceAllClosingTokens($content)
	{
		return str_replace(
            '@endgrid', 
            '</div>', 
            $content
        );
	}

    private function getOpeningTokens()
    {
        return array_map(
            [$this, 'getOpeningToken'], 
            $this->lookupTable
        );
    }

    private function getOpeningTags()
    {
        return array_map(
            [$this, 'getOpeningTag'], 
            $this->lookupTable
        );
    }

    private function getOpeningToken(array $row)
    {
        return $row['openingToken'];
    }

    private function getOpeningTag(array $row)
    {
        return sprintf(
            '<div class="Post-grid Post-grid--%s">', 
            $row['classModifier']
        );
    }
}