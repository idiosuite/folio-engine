<?php

namespace App\Interpolators;

use App\Domain\Post;
use App\Domain\ImageFactory;

class Images implements Interpolator, RequiresPost
{
	use HasPost;

	private $basePath;
	private $baseUrl;
	
	public function __construct(Post $post = null)
	{
		$this->setPost($post ?: new Post);
		$this->basePath = env('IMAGES_SOURCE_PATH');
		$this->baseUrl = env('IMAGES_DESTINATION_URL');
	}
	
	public function text($content)
	{
		return $content . $this->getImageMarkdownFooter(
			$this->getImagesMap($content)
		);
	}

	public function getUsedImages($content)
	{
		return array_map(
			function ($array) {
				return ImageFactory::makeResponsive($array['relativePath']);
			},
			$this->getImagesMap($content)
		);
	}

	private function getImagesMap($content)
	{
		$imagesMap = [];

		foreach ($this->findImageIds($content) as $id) {
			$imagesMap[] = $this->resolveImage($id);
		}

		return array_filter($imagesMap);
	}

	private function getImageMarkdownFooter(array $imagesMap)
	{
		if ( ! $imagesMap) {
			return;
		}

		$markdownImagesMap = array_map(
			[$this, 'getMarkdownImageLine'], 
			$imagesMap
		);

		return "\r\n" . implode("\r\n", $markdownImagesMap);
	}

	private function getMarkdownImageLine(array $image) {
		return sprintf(
			'[%s]: %s', 
			$image['id'], 
			$image['url']
		);
	}

	private function findImageIds($content)
	{
		$pattern = '/!\[([\w\-\.]+)\]/';
		
		preg_match_all($pattern, (string)$content, $matches);
		
		return $matches[1];
	}

	private function resolveImage($id)
	{
		$imageSourcePath = $this->guessImageSourcePath($id);
		
		if ( ! $imageSourcePath) {
			return;
		}

		return [
			'id' => $id, 
			'sourcePath' => $imageSourcePath,
			'relativePath' => $this->getImageRelativePath($imageSourcePath),
			'url' => $this->getImageUrl($imageSourcePath), 
		];
	}

	private function getImageRelativePath($imageSourcePath)
	{
		return trim(
			str_replace($this->basePath, '', $imageSourcePath), 
			'/'
		);
	}

	private function getImageUrl($imageSourcePath)
	{
		return str_replace($this->basePath, $this->baseUrl, $imageSourcePath);
	}

	private function guessImageSourcePath($id)
	{
		foreach ($this->getSupportedFormats() as $extension) {
			$imageSourcePath = sprintf(
				'%s/%s/%s.%s',
				$this->basePath,
				$this->getPost()->slug,
				$id,
				$extension
			);

			if (file_exists($imageSourcePath)) {
				return $imageSourcePath;
			}
		}
	}

	private function getSupportedFormats()
	{
		return ['webp', 'jpg', 'png', 'gif', 'svg'];
	}
}