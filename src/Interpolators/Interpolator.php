<?php

namespace App\Interpolators;

interface Interpolator
{
	public function text($content);
}