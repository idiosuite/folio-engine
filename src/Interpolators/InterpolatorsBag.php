<?php

namespace App\Interpolators;

use \App\Domain\Post;

class InterpolatorsBag
{
	private $prepend = [];
	private $interpolators = [];
	private $append = [];
	
	public function register(Interpolator $interpolator)
	{
		$this->append($interpolator);

		return $this;
	}

	public function bootstrap(array $interpolators)
	{
		$this->interpolators = $interpolators;

		return $this;
	}

	public function prepend(Interpolator $interpolator)
	{
		$this->prepend[] = $interpolator;

		return $this;
	}

	public function append(Interpolator $interpolator)
	{
		$this->append[] = $interpolator;

		return $this;
	}

	public function setPost(Post $post)
	{
		$this->setInterpolatorsPost($this->prepend, $post);
		$this->setInterpolatorsPost($this->interpolators, $post);
		$this->setInterpolatorsPost($this->append, $post);

		return $this;
	}

	public function all()
	{
		return array_merge(
			$this->prepend,
			$this->interpolators,
			$this->append
		);
	}

	private function setInterpolatorsPost(array $interpolators, Post $post)
	{
		foreach ($interpolators as $interpolator) {
			$this->setInterpolatorPost($interpolator, $post);
		}
	}

	private function setInterpolatorPost(Interpolator $interpolator, Post $post)
	{
		if ( ! $interpolator instanceof RequiresPost) {
			return;
		}

		$interpolator->setPost($post);
	}
}