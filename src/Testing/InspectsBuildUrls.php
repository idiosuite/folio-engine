<?php

namespace App\Testing;

use App\Generators\FolioManifest;

use org\bovigo\vfs\vfsStream;

trait InspectsBuildUrls
{
    protected function getJsonEncodedUrl($url)
    {
        return str_replace('/', '\/', $url);
    }

	protected function createPageWithImage($slug)
    {
        return $this->createProjectWithImage($slug, 'page');
    }

	protected function createProjectWithImage($slug, $type = 'project')
    {
        $imageRelativePath = "{$slug}/{$slug}.jpg";
        
        $this->createImageFolderForSlug($slug);
        
        $this->createDummyImage(
            vfsStream::url("folio/contents/images/{$imageRelativePath}")
        );

		$methodName = $type === 'project'
			? 'createPublishedProjectFile'
			: 'createPublishedPageFile';

        $this->$methodName([
            'slug' => $slug, 
            'markdown' => "![$slug]"
        ]);

        return $imageRelativePath;
    }

	protected function defineLogoImageInSettings($imageRelativePath)
    {
        $settingsFilePath = vfsStream::url('folio/contents/settings.json');

        file_put_contents(
            $settingsFilePath, 
            json_encode([
                'theme' => 'test',
                'logo' => $imageRelativePath
            ])
        );
    }

    protected function customizeTemplateHomeViewToOutputLogo()
    {
        file_put_contents(
            vfsStream::url('folio/theme/resources/views/home.blade.php'), 
            '{{ $settings->logo }} {{ json_encode($settings->logo->getUrl()) }}'
        );
    }

    protected function customizeTemplateProjectViewToOutputImages()
    {
        file_put_contents(
            vfsStream::url('folio/theme/resources/views/project.blade.php'), 
            '{{ $post->html()->getBody() }} {{ json_encode($settings->logo->getUrl()) }}'
        );
    }
}