<?php

namespace App\Testing;

use org\bovigo\vfs\vfsStream;

class ClientBuilder
{
	private $slug;
	private $jsonFile;

	public function __construct($slug)
	{
        $this->jsonFile = vfsStream::url('folio/contents/clients.json');
        $this->slug = $slug;
	}

	public static function fromSlug($slug)
	{
		return new static($slug);
	}

	public function create()
	{
		$clientsJson = file_exists($this->jsonFile)
			? file_get_contents($this->jsonFile)
			: '[]';

		$clients = json_decode($clientsJson, $assoc = true);
		$clients[] = [
            'title' => 'My client', 
            'slug' => $this->slug
        ];
		
		file_put_contents(
            $this->jsonFile, 
            json_encode($clients)
        );

		return $this;
	}
}