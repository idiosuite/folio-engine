<?php

namespace App\Testing;

use org\bovigo\vfs\vfsStream;

trait InteractsWithMarkdownFiles
{
	private function createPublishedProjectFile(array $data) {
        $defaultData = [
           'type' => 'projects',
           'slug'=> 'foo',
           'meta' => ['published_at' => 'yesterday'],
           'markdown' => '', 
        ];
        
        $data = array_merge($defaultData, $data);
        
        return $this->createMarkdownFile($data);
    }

    private function createPublishedPageFile(array $data) {
        $defaultData = [
           'type' => 'pages',
           'slug'=> 'bar',
           'meta' => ['published_at' => 'yesterday'],
           'markdown' => '', 
        ];
        
        $data = array_merge($defaultData, $data);
        
        return $this->createMarkdownFile($data);
    }
	
	private function createPageFile(array $meta, $markdown = '') {
        return $this->createMarkdownFile([
            'type' => 'pages', 
            'slug' => 'foo',
            'meta' => $meta, 
            'markdown' => $markdown,
        ]);
    }

    private function createProjectFile(array $meta, $markdown = '') {
        return $this->createMarkdownFile([
            'type' => 'projects', 
            'slug' => 'foo',
            'meta' => $meta, 
            'markdown' => $markdown,
        ]);
    }

    private function createMarkdownFile(array $data) {
        file_put_contents(
            vfsStream::url($this->getMarkdownFileDestinationPath($data)), 
            $this->getMarkdownFileContents($data)
        );
    }

    private function getMarkdownFileDestinationPath(array $data) : string {
        return sprintf(
            'folio/contents/%s/%s.md', 
            $data['type'], 
            $data['slug']
        );
    }

    private function getMarkdownFileContents(array $data) : string {
        $lines = [
            '---',
            $this->getMarkdownFileMeta($data['meta']),
            '---',
            $data['markdown']
        ];

        return implode("\r\n", $lines);
    }

    private function getMarkdownFileMeta(array $meta) : string {
        return implode(
            "\r\n", 
            array_map(
                function ($value, $key) {
                    return $key . ': ' . $value;
                }, 
                $meta, 
                array_keys($meta)
            )
        );
    }
}