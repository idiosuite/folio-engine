<?php

namespace App\Testing;

use App\Domain\Preview;
use App\Repositories\PreviewCollection;
use org\bovigo\vfs\vfsStream;

class PreviewTest
{
	public static function createFolder($path = null)
	{
		$path = $path ?: vfsStream::url('folio/contents/previews');
		mkdir($path);
	}

	public static function jsonFilePath()
	{
		return vfsStream::url('folio/contents/previews/foo.json');
	}

	public static function writeJson(array $previews)
	{
		return static::write(
			static::jsonFilePath(),
			$previews
		);
	}

	private static function write($filePath, array $previews)
    {
        $collection = new PreviewCollection($previews);

		$array = array_map(
			function (Preview $preview) {
				return static::toRepositoryRepresentation($preview);
			},
			$collection->toArray()
		);

        file_put_contents(
            $filePath, 
            json_encode($array)
        );

        return $collection;
    }

	public static function toRepositoryRepresentation(Preview $preview)
	{
		return [
			'src' => $preview->src, 
			'type' => $preview->type, 
			'media'=> $preview->media
		];
	}

	public static function previewImage($imageFileName) : array
	{
		return [
			'src' => $imageFileName, 
			'type' => 'image', 
			'media' => ['preview']
		];
	}
	
	public static function heroImage($imageFileName) : array
	{
		return [
			'src' => $imageFileName, 
			'type' => 'image', 
			'media' => ['hero']
		];
	}

	public static function vimeoVideo($url = null) : array
	{
		$url = $url ?: 'https://www.vimeo.com/12345';
		
		return [
			'type' => 'video', 
			'src' => $url,
			'media' => ['preview']
		];
	}
}