<?php

namespace App\Testing;

use org\bovigo\vfs\vfsStream;
use \App\Testing\InteractsWithImage;

trait InteractsWithFileSystem
{
	private $root;
	
	/** @before */
	public function setUpVfsStream()
	{
		$this->root = vfsStream::setup('folio', null, $this->getStructure());

		$this->initializeInteractsWithTraits();
	}

	private function initializeInteractsWithTraits()
	{
		$uses = class_uses(static::class);

		if (isset($uses[InteractsWithImage::class])) {
			$this->bootstrapInteractsWithImage();
		}
	}

	protected function getStructure()
	{
		return [
			'contents' => [
				'credits' => [],
				'images' => [],
				'previews' => [],
				'projects' => [],
				'pages' => [],
				'preferences' => [
                    'defaults.json' => '[]',
                ],
				'settings.json' => json_encode([
					'theme' => 'test'
				]),
				'videos' => [],
			],
			'manifests' => [],
			'public' => [
			],
			'storage' => [
				'images' => []
			],
			'theme' => $this->getThemeFolder()
		];
	}

	private function getThemeFolder()
	{
		return [
			'assets' => [
				'app.css' => '',
			],
			'resources' => [
				'views' => [
					'home.blade.php' => 'Home',
					'404.blade.php' => 'Not found',
					'project.blade.php' => 'Project',
					'page.blade.php' => 'Page',
					'about.blade.php' => 'Qualified page',
					'special.blade.php' => 'Qualified project',
					'settings.blade.php' => '{{ $settings->theme }} {{ $settings->bar }}'
				]
			]
		];
	}

	protected function createImagesFolderIfDoesNotExist()
	{
		if (file_exists(vfsStream::url('folio/public/images'))) {
			return;
		}

		mkdir(vfsStream::url('folio/public/images'));
	}
}