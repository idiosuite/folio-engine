<?php

namespace App\Views;

use App\Domain\Post;
use App\Interpolators\Grid;
use App\Interpolators\Text;
use App\Interpolators\Vimeo;
use App\Interpolators\Images;
use App\Interpolators\Videos;
use App\Interpolators\Credits;
use App\Interpolators\Youtube;
use App\Interpolators\Markdown;
use App\Interpolators\Slideshow;
use App\Interpolators\InterpolatorsBag;
use App\Interpolators\ResponsiveImages;
use App\Themes\Publishers\Images\Used\UsedImagesFinder;
use App\Themes\Publishers\Images\Used\UsedImagesProvider;

class Content implements UsedImagesProvider
{
	const KEY_LEAD = 'lead';

	private $post;
	
	public function __construct(Post $post)
	{
		$this->post = $post;

		static::interpolators()
			->bootstrap([
				new Images,
				new Videos,
				new Vimeo,
				new Youtube,
				new Text,
				new Markdown,
				new Slideshow,
				new Grid,
				new Credits,
				new ResponsiveImages
			])
			->setPost($this->post);
	}

	public static function interpolators()
	{
		return app(InterpolatorsBag::class);
	}

	public function getLead()
	{
		$lead = $this->leadIsDefinedInTexts()
			? $this->getLeadFromTexts()
			: $this->getLeadFromContents();

		if ( ! $lead) {
			return;
		}
		
		return (new Markdown)->text($lead);
	}

	public function findUsedImages(UsedImagesFinder $usedImagesFinder)
	{
		$images = (new Images($this->post))->getUsedImages(
			$this->getContentWithoutLead()
		);

		foreach ($images as $image) {
			$image->findUsedImages($usedImagesFinder);
		}
	}

	public function getBody()
	{
		$contents = $this->getContentWithoutLead();
		
		foreach (static::interpolators()->all() as $interpolator) {
			$contents = $interpolator->text($contents);
		}
		
		return $contents;
	}

	public function getCredits()
	{
		if ( ! $this->post->credits) {
			return;
		}

		return (new Markdown)->text($this->post->credits);
	}

	private function getContentWithoutLead()
	{
		if ( ! $this->leadIsDefinedInContents()) {
			return $this->post->content;
		}

		list ($lead, $body) =  $this->getExplodedContent();
		
		return $body;
	}

	private function getLeadFromTexts()
	{
		return $this->post->texts->get(static::KEY_LEAD);
	}
	
	private function getLeadFromContents()
	{
		if ( ! $this->leadIsDefinedInContents()) {
			return;
		}

		list ($lead, $body) = $this->getExplodedContent();
		
		return $lead;
	}

	private function leadIsDefinedInContents()
	{
		return $this->hasLeadSeparator() && $this->hasLead();
	}

	private function hasLead()
	{
		return count($this->getExplodedContent()) > 1;
	}

	private function getExplodedContent()
	{
		return explode($this->getLeadSeparator(), $this->post->content);
	}

	private function hasLeadSeparator()
	{
		return !! $this->getLeadSeparator();
	}

	private function getLeadSeparator()
	{
		return $this->post->lead_separator;
	}

	private function leadIsDefinedInTexts()
	{
		return$this->post->texts->has(static::KEY_LEAD);
	}
}