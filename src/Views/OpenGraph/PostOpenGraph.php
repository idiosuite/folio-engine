<?php

namespace App\Views\OpenGraph;

use App\Context;
use App\Domain\Post;
use App\Domain\WebsiteFeatureImageFactory;

class PostOpenGraph extends BaseOpenGraph
{
    private $post;
    private $websiteFeatureImage;
    private $context;

    private function __construct(Post $post)
    {
        parent::__construct([
            'title' => $post->title,
            'description' => $post->excerpt,
        ]);

        $this->post = $post;
        $this->websiteFeatureImage = WebsiteFeatureImageFactory::fromPost($post);
        $this->context = app(Context::class);

        $this->bootstrapWithCallback([ $post ]);
    }

    public static function fromPost(Post $post) : OpenGraph
    {
        return new CombinedOpenGraph([
			new static($post),
			app(OpenGraph::class)
		]);
    }

    public function getUrl()
    {
        return ! $this->isHomepage()
            ? env('BASE_URL') . $this->getRelativePath() 
            : null;
    }

    public function getImageUrl()
    {
        return $this->websiteFeatureImage 
            ? $this->websiteFeatureImage->getUrl() 
            : null;
    }

    private function getRelativePath()
    {
        return $this->context->getPath($this->post);
    }

    private function isHomepage()
    {
        return $this->context->getContainerModifier() == 'home';
    }
}