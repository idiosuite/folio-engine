<?php

namespace App\Views\OpenGraph;

use App\Domain\Post;
use App\Domain\ImageFactory;
use App\Domain\Images\Image;
use App\Themes\Publishers\Manifests\Manifest;
use App\Themes\Publishers\Manifests\ManifestFactory;

class BaseOpenGraph implements OpenGraph
{
    private $attributes = [];

    public function __construct(array $attributes = [])
    {
        $this->attributes = [
            'url' => env('BASE_URL'),
            'image' => null,
            'type' => 'website',
            'title' => 'My portfolio',
            'description' => 'Welcome to my portfolio'
        ];

        $this->bootstrap($attributes);
    }

    public function bootstrapWithCallback(array $args)
    {
        return $this->bootstrap(
            app(OpenGraphConfig::class)->invokeCallback($this, $args)
        );
    }

    public function bootstrap(array $override)
    {
        $this->attributes = array_merge($this->attributes, $override);

        return $this;
    }

    public function getUrl()
    {
        return $this->attributes['url'];
    }

    public function getImageUrl()
    {
        if ( ! $this->attributes['image']) {
            return;
        }

        return image($this->attributes['image'])->getUrl();
    }

    public function getType()
    {
        return $this->attributes['type'];
    }

    public function getTitle()
    {
        return $this->attributes['title'];
    }

    public function getDescription()
    {
        return $this->attributes['description'];
    }
}