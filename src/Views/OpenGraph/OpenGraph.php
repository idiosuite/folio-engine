<?php

namespace App\Views\OpenGraph;

use App\Domain\Post;
use App\Domain\ImageFactory;
use App\Domain\Images\Image;
use App\Themes\Publishers\Manifests\Manifest;
use App\Themes\Publishers\Manifests\ManifestFactory;

interface OpenGraph
{
    public function getUrl();
    public function getImageUrl();
    public function getType();
    public function getTitle();
    public function getDescription();
}