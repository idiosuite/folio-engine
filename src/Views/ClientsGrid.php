<?php

namespace App\Views;

use App\Domain\Client;

class ClientsGrid
{
    private $clients = [];
    private $targetArea = 2500;
    private $wrapper = '%s';
    private $whitelist = [];

    public function __construct(array $clients = [])
    {
        $this->clients = $clients;
    }

    public static function fromRegistry()
    {
        $clients = registry()->clients()->all();

        $clients = array_map(
            function ($client) {
                if ( ! $client['logo'] instanceof \App\Domain\Images\Image) {
                    return $client;
                }

                $client['logo'] = $client['logo']->getRelativePath();
                
                return $client;
            },
            $clients
        );
        
        $clients = array_map(
            function ($client) {
                return new Client($client);
            },
            $clients
        );

        return new static($clients);
    }

    public function withCustomArea(int $targetArea) : static
    {
        $this->targetArea = $targetArea;

        return $this;
    }

    public function withWrapper(string $pattern)
    {
        $this->wrapper = $pattern;

        return $this;
    }

    public function withWhitelist(array $slugs)
    {
        $this->whitelist = $slugs;

        return $this;
    }

    public function getHtml() : string
    {
        $html = '';
        
        foreach ($this->getWhitelistedClientSlugs() as $slug) {
            $html.= $this->getClientHtml(
                $this->findClientBySlug($slug)
            );
        }
        
        return $html;
    }

    private function getWhitelistedClientSlugs() : array
    {
        if ( ! $this->whitelist) {
            return $this->getAllClientSlugs();
        }

        return $this->whitelist;
    }

    private function findClientBySlug(string $slug) : Client
    {
        $index = array_search(
            $slug, 
            $this->getAllClientSlugs()
        );

        return $this->clients[$index];
    }

    private function getAllClientSlugs() : array
    {
        return array_map(
            function (Client $client) {
                return $client->slug;
            },
            $this->clients
        );
    }

    private function getClientHtml(Client $client) : string
    {
        return sprintf(
            $this->wrapper, 
            $this->getImageHtml($client)
        );
    }

    private function getImageHtml(Client $client) : string|null
    {
        return $client->logo
            ->normalizeSize($this->targetArea, $client->scaleFactor)
            ->getHtml();
    }
}