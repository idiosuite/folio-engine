<?php

namespace App\Views;

use App\Portfolio;

class ThemeFacade
{
	public function assets($relativePath)
	{
		return app(Portfolio::class)
			->themeAssets()
			->getUrl($relativePath);
	}
}