<?php

namespace App\Views;

use Jenssegers\Blade\Blade;
use Exception;

class ViewFactory
{
	private $themeProviders;

	public function __construct(array $themeProviders = [])
	{
		$this->setThemeProviders($themeProviders);
	}

	public function setThemeProviders(array $themeProviders)
	{
		$this->themeProviders = $themeProviders;
	}

	public function getThemeProvider($identifier)
	{
		$this->guardThemeProvider($identifier);
		
		return new $this->themeProviders[$identifier];
	}

	public function make($themeId)
	{
		$this->guardThemeId($themeId);

		return new Blade(
			$this->getViewsPath($themeId), 
			$this->getCachePath()
		);
	}

	private function getViewsPath($themeId)
	{
		$themeBasePath = $this
			->getThemeProvider($themeId)
			->getBasePath();

		return $themeBasePath . '/resources/views';
	}

	private function getCachePath()
	{
		return env('BASE_PATH') . '/storage/cache';
	}

	private function guardThemeId($themeId)
	{
		if ( ! $themeId) {
			throw new Exception('Please provide a theme in your settings.json file.');
		}
	}

	private function guardThemeProvider($identifier)
	{
		if ( ! isset($this->themeProviders[$identifier])) {
			throw new Exception(
				'Please provide a theme provider through ::bootstrapThemes() method.'
			);
		}
	}
}