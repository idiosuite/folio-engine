<?php

namespace App\Parsers;

use App\Exceptions\NotFoundException;

class FileParser
{
	private $filePath;
	private $markdown;
	private $meta;

	public function __construct($filePath)
	{
		$this->filePath = $filePath;
		$this->parse();
	}

	public function exists()
	{
		return file_exists($this->filePath);
	}

	private function parse()
	{
		if ( ! $this->exists()) {
			throw new NotFoundException;
		}
		
		$fileContents = file_get_contents($this->filePath);

		$parts = explode('---', $fileContents);

		if (count($parts) >= 3) {
			$this->meta = $parts[1];
			$this->markdown = $parts[2];
		}

		return $this;
	}

	public function getMarkdown()
	{
		return $this->markdown;
	}

	public function getMeta()
	{
		return (new MetaParser($this->meta))->get();
	}
}