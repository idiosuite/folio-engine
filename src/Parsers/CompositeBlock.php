<?php

namespace App\Parsers;

class CompositeBlock implements Block
{
	private $blocks;

	public function __construct(array $blocks)
	{
		$this->blocks = array_values($blocks);
	}

	public function getType()
	{
		return $this->blocks[0]->getType();
	}

	public function getSlug()
	{
		return $this->blocks[0]->getSlug();
	}

	public function getBody()
	{
		return implode(
			"\n", 
			array_map(
				function (Block $block){
					return $block->getBody();
				}, 
				$this->blocks
			)
		);
	}
}