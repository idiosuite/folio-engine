<?php

namespace App\Parsers;

class TextBlocksParser
{
	private $defaultSlug;
	private $contents;
    private $blocks = [];

	private function __construct($contents, $defaultSlug = null)
	{
		$this->defaultSlug = $defaultSlug;
		$this->contents = (string)$contents;
        $this->blocks = $this->parseBlocks();
	}

	public static function fromString($string, $defaultSlug = null)
	{
		return new static($string, $defaultSlug);
	}

	public function get()
	{
		return $this->blocks;
	}

	public function getWithSlug($slug)
	{
		$blocks = $this->filterBlocksBySlug($this->blocks, $slug);
		
		if ( ! $blocks) {
			return;
		}

		return $blocks[0];
	}

	private function parseBlocks()
	{
		$blocks = $this->explodeByDelimiters();
		$blocks = $this->getBlockInstances($blocks);
		$blocks = $this->flushEmpty($blocks);
		$blocks = $this->mergeDuplicate($blocks);
		return $blocks;
	}

	private function explodeByDelimiters()
	{
		return preg_split('/\@/', $this->contents);
	}

	private function getBlockInstances(array $blocks) : array
	{
		return array_map(
			function ($block) {
				return new BaseBlock(
					'markdown', 
					$block, 
					$this->defaultSlug
				);
			},
			$blocks
		);
	}

	private function flushEmpty(array $blocks)
	{
		return array_filter(
			$blocks,
			function (BaseBlock $block) {
				return ! $block->isEmpty();
			}
		);
	}

	private function mergeDuplicate(array $blocks) 
	{
		$result = [];

		foreach ($this->getUniqueSlugs($blocks) as $slug) {
			$blocksForSlug = $this->filterBlocksBySlug($blocks, $slug);
			$result[] = $this->shouldBeMerged($blocksForSlug)
				? new CompositeBlock($blocksForSlug)
				: $blocksForSlug[0];
		}

		return $result;
	}

	private function shouldBeMerged(array $blocks)
	{
		return count($blocks) > 1;
	}

	private function filterBlocksBySlug(array $blocks, string $slug)
	{
		return array_values(
			array_filter(
				$blocks,
				function (Block $block) use ($slug) {
					return $block->getSlug() == $slug;
				}
			)
		);
	}

	private function getUniqueSlugs(array $blocks) : array
	{
		return array_unique(
			array_map(
				function($block) {
					return $block->getSlug();
				}, 
				$blocks
			)
		);
	}
}