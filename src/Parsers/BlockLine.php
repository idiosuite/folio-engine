<?php

namespace App\Parsers;

class BlockLine
{
	private $line;
	private $tokens;

	public function __construct($line)
	{
		$this->line = $line;
		$this->tokens = ['markdown', 'credits', 'title'];
	}

	public function isCreditsHeader()
	{
		return preg_match(
			$this->getStartsWithTokenRegex( ['credits'] ), 
			$this->line
		);
	}

	public function isTitleHeader()
	{
		return preg_match(
			$this->getStartsWithTokenRegex( ['title'] ), 
			$this->line
		);
	}

	public function isHeader()
	{
		return preg_match(
			$this->getStartsWithTokenRegex( $this->getStartTokens() ), 
			$this->line
		);
	}

	private function isFooter()
	{
		return preg_match(
			$this->getStartsWithTokenRegex( $this->getEndTokens() ), 
			$this->line
		);
	}

	public function getRaw()
	{
		return $this->line;
	}

	public function getType()
	{
		if ( ! $this->isHeader()) {
			return null;
		}

		return $this->parseHeader()['type'];
	}

	public function getSlug()
	{
		if ( ! $this->isHeader()) {
			return null;
		}

		return $this->parseHeader()['slug'];
	}

	public function getBody()
	{
		if ($this->isHeader()) {
			return $this->parseHeader()['body'];
		}

		if ($this->isFooter()) {
			return $this->parseFooter();
		}

		return $this->line;
	}

	private function parseHeader()
	{
		return (new HeaderLine($this->line))->parse();
	}

	private function parseFooter()
	{
		return trim(
			str_replace(
				$this->getEndTokens(), 
				'', 
				$this->line
			)
		);
	}

	private function getStartsWithTokenRegex(array $tokens)
	{
		$startsWithRegex = array_map(
			function ($token) {
				return '^' . $token;
			},
			$tokens
		);
		
		$orClauseRegex = implode('|', $startsWithRegex);

		return "/{$orClauseRegex}/";
	}

	private function getStartTokens()
	{
		return array_map(
			function ($token) {
				return $token . "\('";
			},
			$this->tokens
		);
	}

	private function getEndTokens()
	{
		return array_map(
			function ($token) {
				return 'end' . $token;
			},
			$this->tokens
		);
	}
}