<?php

namespace App\Parsers;

class HeaderLine
{
	private $line;

	public function __construct($line)
	{
		$this->line = $line;
	}

	public function parse()
	{
		list ($type, $params, $body) = $header = $this->parseHeader();
		
		list ($slug) = $params = $this->parseParams($params);

        $this->guardTrailingBody($params, $body);

		return [
			'type' => $type,
			'slug' => $slug,
			'body' => $this->paramsContainBody($params)
                ? $params[1] . $body
                : $body
		];
	}

    private function parseHeader()
    {
        return preg_split("/\('|'\)/", $this->line);
    }

	private function parseParams(string $params)
	{
		$params = $this->getCommaSeparatedParams($params);
		$params = $this->removeTrailingWhiteSpaces($params);
		$params = $this->removeTrailingQuotes($params);
		return $params;
	}

    private function guardTrailingBody(array $params, string $body)
    {
        if ( ! $body) {
            return;
        }

        if ( ! $this->paramsContainBody($params) ) {
            return;
        }

        throw new \Exception(
            '@token($slug, $body) syntax does not accept any content on the same line'
        );
    }

    private function paramsContainBody(array $paramsParts) {
		return count($paramsParts) > 1;
	}

	private function getCommaSeparatedParams(string $params)
	{
		return explode(',', $params);
	}

	private function removeTrailingWhiteSpaces(array $paramsParts)
	{
		return array_map(
			function ($value) {
				return trim($value);
			},
			$paramsParts
		);
	}

	private function removeTrailingQuotes(array $paramsParts)
	{
		return array_map(
			function ($value) {
				return trim($value, "'");
			},
			$paramsParts
		);
	}
}