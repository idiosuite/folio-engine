<?php

namespace App\Console\Commands;

use App\Portfolio;
use Psr\Log\LogLevel;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateDevelopmentPortfolio extends Command
{
    protected static $defaultName = 'portfolio:generate-development';

    protected function configure()
    {
        $this
            ->setDescription('Generates development files.')
            ->setHelp('This command allows you to have a dynamic version of your portfolio for development');
        
        $this->addOption(
            'destination',
            'dest',
            InputOption::VALUE_REQUIRED,
            'What is the destination folder?',
            null
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        return $this->getApplication()
            ->find('portfolio:generate')
            ->run(new ArrayInput([
                '--format' => 'develop',
                '--images' => true,
                '--images-publisher' => 'symlink',
                '--theme-publisher' => 'symlink',
                '--destination' => $input->getOption('destination'),
            ]), $output);
    }
}