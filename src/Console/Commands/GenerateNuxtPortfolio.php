<?php

namespace App\Console\Commands;

use App\Portfolio;
use Psr\Log\LogLevel;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateNuxtPortfolio extends Command
{
	protected static $defaultName = 'portfolio:generate-nuxt';

	protected function configure()
	{
		$this
			->setDescription('Generates a nuxt portfolio.')
			->setHelp('This command allows you to generate a static nuxt portfolio');

		$this->addOption(
			'destination',
			$shortcut = null,
			InputOption::VALUE_OPTIONAL,
			'Where should the json files be published?',
			null
		);

		$this->addOption(
			'theme-destination',
			$shortcut = null,
			InputOption::VALUE_OPTIONAL,
			'Where should the theme files be published?',
			null
		);
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		return $this->getApplication()
			->find('portfolio:generate')
			->run(new ArrayInput([
				'--build' => true,
				'--format' => 'json',
				'--images' => true,
				'--images-publisher' => 'copy',
				'--theme-publisher' => 'copy',
				'--destination' => $input->getOption('destination'),
				'--theme-destination' => $input->getOption('theme-destination')
			]), $output);
	}
}