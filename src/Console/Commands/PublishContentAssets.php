<?php

namespace App\Console\Commands;

use App\Portfolio;
use Psr\Log\LogLevel;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Output\OutputInterface;

class PublishContentAssets extends Command
{
    protected static $defaultName = 'publish:content';

    protected function configure()
    {
        $this
            ->setDescription('Generates content files.')
            ->setHelp('This command allows you to publish the content of your portfolio');

        $this->addOption(
            'format',
            $shorthand = null,
            InputOption::VALUE_REQUIRED,
            sprintf(
                'What is the format? (supported: %s)', 
                implode(',', $this->getSupportedFormats())
            ),
            'html'
        );

        $this->addOption(
            'destination',
            'dest',
            InputOption::VALUE_REQUIRED,
            'What is the destination folder?',
            null
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logger = new ConsoleLogger($output);

        $format = $input->getOption('format');
        $destination = $input->getOption('destination');

        try {
            $this->guardSupportedFormat($input);

            app(Portfolio::class)
                ->generate()
                ->$format($destination, $logger);
        } catch (\Exception $e) {
            dd($e->getMessage());
            return Command::FAILURE;
        }

        $output->writeln("<info>Portfolio successfully generated</>");
        return Command::SUCCESS;
    }

    private function guardSupportedFormat($input)
    {
        if ( ! $this->isSupportedFormat($input)) {
            throw new \Exception('Format is not supported.');
        }
    }

    private function isSupportedFormat($input)
    {
        return in_array(
            $input->getOption('format'), 
            $this->getSupportedFormats()
        );
    }

    private function getSupportedFormats()
    {
        return ['json', 'html', 'develop'];
    }
}