<?php

namespace App\Console\Commands;

use App\Portfolio;
use App\Themes\Publishers\PublisherFactory;
use Psr\Log\LogLevel;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Symfony\Component\Console\Output\OutputInterface;

class PublishThemeAssets extends Command
{
    protected static $defaultName = 'publish:theme';

    protected function configure()
    {
        $this
            ->setDescription('Publishes the current theme assets.')
            ->setHelp('This command allows you to publish the current theme assets.');

        $this
            ->addOption(
                'publisher',
                $shortcut = null,
                InputOption::VALUE_REQUIRED,
                'What publisher should be used?',
                'symlink'
            );

        $this
            ->addOption(
                'destination',
                $shortcut = null,
                InputOption::VALUE_OPTIONAL,
                'Where should the assets be published?',
                null
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logger = new ConsoleLogger($output);

        try {
            $publisher = PublisherFactory::make($input->getOption('publisher'));
            $destination = $input->getOption('destination');

            app(Portfolio::class)
                ->themeAssets()
                ->setDestinationFolder($destination)
                ->publish($publisher);
        } catch (\Exception $e) {
            $output->writeln("<error>{$e->getMessage()}</>");
        }

        $output->writeln("<info>Portfolio theme assets successfully published</>");
        
        return Command::SUCCESS;
    }
}