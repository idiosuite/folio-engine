<?php

namespace App\Console;

use App\Console\Commands\ClearImageAssets;
use App\Console\Commands\GeneratePortfolio;
use App\Console\Commands\ProcessImageAssets;
use App\Console\Commands\PublishImageAssets;
use App\Console\Commands\PublishThemeAssets;
use App\Console\Commands\GenerateAwsPortfolio;
use App\Console\Commands\PublishContentAssets;
use App\Console\Commands\GenerateNuxtPortfolio;
use App\Console\Commands\GenerateDevelopmentPortfolio;
use Symfony\Component\Console\Application as SymfonyApplication;

class Application
{
	private $application;

	public function __construct()
	{
		$this->application = new SymfonyApplication;
		$this->bootstrap();
	}

	public function find($commandName)
	{
		return $this->application->find($commandName);
	}

	private function bootstrap()
	{
		$this->application->add(new GeneratePortfolio);
		$this->application->add(new GenerateDevelopmentPortfolio);
		$this->application->add(new GenerateNuxtPortfolio);
		$this->application->add(new GenerateAwsPortfolio);
		$this->application->add(new PublishContentAssets);
		$this->application->add(new PublishImageAssets);
		$this->application->add(new PublishThemeAssets);
		$this->application->add(new ProcessImageAssets);
		$this->application->add(new ClearImageAssets);
	}

	public function run()
	{
		$this->application->run();
	}
}