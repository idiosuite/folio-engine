<?php

namespace App\Domain\Images;

class LocalImage extends AbstractImage
{
	public function __construct($relativePath)
	{
		$this->basePath = env('IMAGES_SOURCE_PATH');
		$this->baseDestinationPath = env('IMAGES_DESTINATION_PATH');
		$this->baseUrl = env('IMAGES_DESTINATION_URL');
		$this->relativePath = $relativePath;
	}

	protected function doExists()
	{
		if ( ! $this->getRelativePath()) {
			return false;
		}

		return file_exists($this->getPath());
	}
}