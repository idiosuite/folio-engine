<?php

namespace App\Domain\Images;

use App\Domain\ImageFactory;
use App\Domain\Images\Image;
use App\Domain\Images\LocalImage;
use Illuminate\Contracts\Support\Arrayable;
use App\Themes\Publishers\Manifests\Manifest;
use App\Domain\Images\Responsive\ImageHtmlHelper;
use App\Themes\Publishers\Images\Used\UsedImagesFinder;
use App\Themes\Publishers\Images\Used\UsedImagesProvider;
use App\Domain\Images\Responsive\FormatsHtmlProvider;

class ResponsiveImage implements Image, Arrayable, UsedImagesProvider
{
	private $manifest;
	private $image;
	private $htmlImageHelper;

	private $basePathSource;
	private $basePathDestination;
	private $baseUrlDestination;

	private $formats;
	
	public function __construct(Manifest $manifest, $image, array $config = [])
	{
		$this->manifest = $manifest;
		
		$this->basePathSource = $this->normalizeConfigOrFail($config, 'sourcePath', 'IMAGES_SOURCE_PATH');
		$this->basePathDestination = $this->normalizeConfigOrFail($config, 'destinationPath', 'IMAGES_DESTINATION_PATH');
		$this->baseUrlDestination = $this->normalizeConfigOrFail($config, 'destinationUrl', 'IMAGES_DESTINATION_URL');

		$this->image = $this->normalizeImage($image);
		$this->htmlImageHelper = new ImageHtmlHelper($this);
		$this->formats = new FormatsHtmlProvider(
			$this->getSourcePath(), 
			$this->manifest, 
			$this->htmlImageHelper
		);
	}

	public function findUsedImages(UsedImagesFinder $usedImagesFinder)
	{
		$this->image->findUsedImages($usedImagesFinder);
		$this->formats->findUsedImages($usedImagesFinder);
	}

	private function normalizeConfigOrFail($config, $key, $envKey)
	{
		$result = array_key_exists($key, $config)
			? $config[$key] 
			: env($envKey);

		if ( ! $result) {
			throw new \Exception("Please provide a {$envKey} environment variable");
		}

		return $result;
	}

	public function __toString()
	{
		return $this->getUrl();
	}

	public function setBasePaths($basePathSource, $basePathDestination)
	{
		$this->basePathSource = $basePathSource;
		$this->basePathDestination = $basePathDestination;

		return $this;
	}

	public function exists()
	{
		return $this->image->exists();
	}

	public function getHtml(array $attributes = [])
	{
		return $this->toHtml($attributes);
	}

	public function getBasePath()
	{
		return $this->basePathDestination;
	}

	public function getBaseUrl()
	{
		return $this->baseUrlDestination;
	}

	public function getRelativePath()
	{
		return $this->image->getRelativePath();
	}

	public function getWidth()
	{
		return $this->image->getWidth();
	}

	public function getHeight()
	{
		return $this->image->getHeight();
	}

	public function getUrl()
	{
		if ( ! $this->isResponsiveImage()) {
			return $this->image->getUrl();
		}

		foreach ($this->toResponsiveArray() as $output) {
			return $this->htmlImageHelper->getSrcAttribute($output);
		}
	}

	public function getPath()
	{
		foreach ($this->toResponsiveArray() as $output) {
			return $output['path'];
		}
	}

	public function getSourcePath()
	{
		return $this->image->getSourcePath();
	}

	public function toArray()
	{
		return $this->image->toArray();
	}

	public function normalizeSize($targetArea = null, $scaleFactor = null)
	{
		return new NormalizedImage($this, $targetArea, $scaleFactor);
	}

	public function toHtml(array $attributes = [])
	{
		if ( ! $this->isResponsiveImage()) {
			return $this->image->getHtml($attributes);
		}
		
		if ($this->formats->hasFormats()) {
			return $this->formats->getHtml($attributes);
		}

		if ($this->isSkippedFile()) {
			return;
		}

		$outputs = $this->toResponsiveArray();

		if ($this->isUncompressedImage()) {
			return $this->htmlImageHelper->getClassicImageMarkup($outputs[0], $attributes);
		}

		return $this->htmlImageHelper->getImagesMarkup($outputs, $attributes);
	}

	public function toResponsiveArray()
	{
		return $this->manifest->getEntry($this->getKey())->getOutputs();
	}

	private function isResponsiveImage()
	{
		return $this->manifest->has($this->getKey());
	}
	
	private function isSkippedFile()
	{
		 return $this->manifest->getEntry($this->getKey())->isSkipped();
	}

	public function isUncompressedImage()
	{
		return $this->manifest->getEntry($this->getKey())->isUncompressed();
	}
	
	private function getKey()
	{
		return $this->getSourcePath();
	}
	
	private function normalizeImage($image)
	{
		if (is_null($image)) {
			$image = '';
		}

		if (is_string($image)) {
			$image = ImageFactory::make($image);
		}

		if ( ! $image instanceof Image) {
			throw new \InvalidArgumentException(
				'Please provide either an Image instance or a string path'
			);
		}

		return $image;
	}
}