<?php

namespace App\Domain\Images;

class NullImage extends AbstractImage
{
	public function __construct($relativePath = null)
	{
		$this->basePath = env('IMAGES_SOURCE_PATH');
		$this->baseDestinationPath = env('IMAGES_DESTINATION_PATH');
		$this->baseUrl = env('IMAGES_DESTINATION_URL');
		$this->relativePath = $relativePath;
	}

	public function toArray()
	{
		return [
			'src' => $this->relativePath
				? $this->baseUrl . $this->relativePath
				: null, 
			'width' => 0, 
			'height' => 0
		];
	}

	protected function doExists()
	{
		return false;
	}
}