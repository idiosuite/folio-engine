<?php

namespace App\Domain\Images;

class RemoteImage extends AbstractImage
{
	public function __construct($relativePath)
	{
		$this->relativePath = $relativePath;
	}

	protected function doExists()
	{
		return ! (is_null($this->getWidth()) && is_null($this->getHeight()));
	}

	public function getUrl()
	{
		return trim(parent::getUrl(), '/');
	}

	public function getPath()
	{
		return trim(parent::getPath(), '/');
	}
}