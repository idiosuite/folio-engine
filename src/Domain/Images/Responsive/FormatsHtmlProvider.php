<?php

namespace App\Domain\Images\Responsive;

use App\Domain\ImageFactory;
use App\Domain\Images\Image;
use App\Domain\Images\LocalImage;
use App\Domain\Images\ResponsiveImage;
use Illuminate\Contracts\Support\Arrayable;
use App\Themes\Publishers\Manifests\Manifest;
use App\Themes\Publishers\Images\Crops\CroppedImagesCreator;
use App\Themes\Publishers\Images\Used\UsedImagesFinder;
use App\Themes\Publishers\Images\Used\UsedImagesProvider;
use App\Repositories\FileSystem\PathInspector;

class FormatsHtmlProvider implements UsedImagesProvider
{
	private $sourcePath;
	private $manifest;
	private $imageHtmlHelper;
	private $formatPaths;
	private $map = [];

	public function __construct(string $sourcePath, Manifest $manifest, ImageHtmlHelper $imageHtmlHelper)
	{
		$this->sourcePath = $sourcePath;
		$this->manifest = $manifest;
		$this->imageHtmlHelper = $imageHtmlHelper;

		$this->formatPaths = CroppedImagesCreator::fromSourceImagePath(
				$this->getSourcePath()
			)
			->usesStorage()
			->getAllPaths();

		$this->map = [
			['media' => '(max-aspect-ratio: 4/5)', 'format' => 'portrait'],
			['media' => '(max-aspect-ratio: 1/1)', 'format' => 'square'],
			['media' => '(min-aspect-ratio: 1/1)', 'format' => 'default'],
		];
	}

	public function findUsedImages(UsedImagesFinder $usedImagesFinder)
	{
		foreach ($this->formatPaths as $path) {
			$usedImagesFinder->add($path);
		}
	}
	
	public function hasFormats()
	{
		return !! $this->formatPaths;
	}

	public function getHtml(array $attributes) : string
	{
		$lines = [];

		foreach ($this->getMediaQueryMap() as $item) {
			$lines[] = $this->getCroppedImage($attributes, $item);
		}
		
		$lines[] = $this->getDefaultImage($attributes);
		
		return $this->imageHtmlHelper->getPicture($lines);
	}

	private function getMediaQueryMap() : array
	{
		$map = array_map(
			[$this, 'addSourcePathForFormat'],
			$this->map
		);

		return $map;
	}

	private function addSourcePathForFormat(array $item) : array
	{
		$item['path'] = $this->getSourcePathForFormat($item['format']);
		
		return $item;
	}

	private function getSourcePathForFormat(string $format) : string
	{
		return $this->hasSourcePathForFormat($format)
			? $this->formatPaths[$format]
			: $this->getSourcePathForNextAvailableFormat($format);
	}

	private function getSourcePathForNextAvailableFormat($format)
	{
		$fallbackFormat = $this->findFallbackFormat($format);

		return $fallbackFormat
			? $this->getSourcePathForFormat($fallbackFormat)
			: $this->getSourcePath();
	}

	private function findFallbackFormat($format)
	{
		$formats = $this->getFormatsInFallbackOrder();

		$nextFormatIndex = $this->findNextFormatIndexOrFail($format, $formats);

		if ( ! array_key_exists($nextFormatIndex, $formats)) {
			return;
		}

		return $formats[$nextFormatIndex];
	}

	private function findNextFormatIndexOrFail($format, $formats)
	{
		$currentFormatIndex = array_search($format, $formats);

		if ($currentFormatIndex === false) {
			throw new \Exception('Unknown format');
		}

		return $currentFormatIndex + 1;
	}

	private function getFormatsInFallbackOrder()
	{
		return array_map(
			function ($item) {
				return $item['format'];
			},
			$this->map
		);
	}

	private function hasSourcePathForFormat(string $format) : bool
	{
		return array_key_exists($format, $this->formatPaths);
	}

	private function getCroppedImage(array $attributes, array $item) : string
	{
		$outputs = $this->manifest->getEntry($item['path'])->getOutputs();

		$image = $this->manifest->getEntry($item['path'])->isUncompressed()
			? $outputs[0]
			: $outputs[1];

		$attributes = array_merge($attributes, ['media' => $item['media']]);

		return $this->imageHtmlHelper->getSourceImageMarkup((array)$image, $attributes);
	}

	private function getDefaultImage(array $attributes) : string
	{
		$outputs = $this->manifest->getEntry($this->getSourcePath())->getOutputs();
		
		$uncompressedImage = $outputs[0];
		
		return $this->imageHtmlHelper->getImageMarkup($uncompressedImage, $attributes);
	}

	private function getSourcePath()
	{
		return $this->sourcePath;
	}
}