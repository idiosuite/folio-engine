<?php

namespace App\Domain\Images;

use Illuminate\Contracts\Support\Arrayable;
use App\Themes\Publishers\Images\Used\UsedImagesFinder;
use App\Themes\Publishers\Images\Used\UsedImagesProvider;

abstract class AbstractImage implements Image, Arrayable, UsedImagesProvider
{
	protected $basePath;
	protected $baseDestinationPath;
	protected $baseUrl;
	protected $relativePath; 

	private $exists = null;
	private $size = null;

	public function toArray()
	{
		if ( ! $this->exists()) {
			return;
		}
		
		return [
			'src' => $this->getUrl(), 
			'width' => $this->getWidth(), 
			'height' => $this->getHeight()
		];
	}

	public function findUsedImages(UsedImagesFinder $usedImagesFinder)
	{
		$usedImagesFinder->add(
			$this->getSourcePath()
		);
	}

	public function exists()
	{
		if ( ! $this->existsIsCached()) {
			$this->exists = $this->doExists();
		}

		return $this->exists;
	}

	abstract protected function doExists();

	public function getHtml(array $attributes = [])
	{
		if ( ! $this->exists()) {
			return;
		}
		
		$attributes = array_merge([
			'src' => $this->getUrl(), 
			'width' => $this->getWidth(), 
			'height' => $this->getHeight()
		], $attributes);
		
		return sprintf(
			'<img %s />', 
			$this->getHtmlAttributes($attributes)
		);
	}

	private function getHtmlAttributes(array $attributes)
	{
		$result = [];
		
		foreach ($attributes as $attribute => $value) {
			$result[] = sprintf('%s="%s"', $attribute, $value);
		}
		
		return implode(' ', $result);
	}

	public function getUrl()
	{
		return $this->baseUrl
			? $this->getBaseUrl() . '/' . $this->getRelativePath()
			: $this->getRelativePath();
	}

	public function getPath()
	{
		return $this->basePath
			? $this->getBasePath() . '/' . $this->getRelativePath()
			: $this->getRelativePath();
	}

	public function getSourcePath()
	{
		return $this->getPath();
	}

	public function getBasePath()
	{
		return $this->basePath
			? rtrim($this->basePath, '/')
			: null;
	}

	public function getBaseUrl()
	{
		return $this->baseUrl 
			? rtrim($this->baseUrl, '/') 
			: null;
	}

	public function getRelativePath()
	{
		return trim((string)$this->relativePath, '/');
	}

	public function getWidth()
	{
		return $this->getSize()['width'];
	}

	public function getHeight()
	{
		return $this->getSize()['height'];
	}

	public function getSize()
	{
		if ( ! $this->sizeIsCached()) {
			$this->size = (new ImageSizeCalculator($this))->get();
		}

		return $this->size;
	}

	private function existsIsCached()
	{
		return $this->exists !== null;
	}

	private function sizeIsCached()
	{
		return $this->size !== null;
	}

	public function __toString()
	{
		return $this->getRelativePath() ?: '';
	}
}