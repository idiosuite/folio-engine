<?php

namespace App\Domain;

use App\Resources\ClientResource;
use App\Resources\ProvidesApiResource;
use Carbon\Carbon;

class Client extends DomainObject implements ProvidesApiResource
{
	public function __toString() {
		return $this->title;
	}

	public function apiResource()
	{
        return new ClientResource($this);
	}

    public function getTitleAttribute($value)
    {
		if ( ! $value && $this->slug) {
			return ucfirst($this->slug);
		}
        
		return $value;
    }
	
	public function getLogoAttribute($value)
	{
		return ImageFactory::makeResponsive($value);
	}

	public function getScaleFactorAttribute($value)
	{
		return $value ?: 1;
	}
}