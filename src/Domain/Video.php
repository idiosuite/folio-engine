<?php

namespace App\Domain;

use App\Resources\ProvidesApiResource;
use App\Resources\VideoResource;

class Video extends DomainObject implements ProvidesApiResource
{
	public function apiResource()
	{
		return new VideoResource($this);
	}

	public function __toString()
	{
		return $this->getUrl();
	}

	public static function fromUrl($url)
	{
		$parts = explode('/', $url);
		$id = last($parts);

		return new static([
			'provider' => 'vimeo',
			'id' => $id,
			'slug' => $id,
			'url' => $url
		]);
	}
}