<?php

namespace App\Domain\Previews;

use App\Domain\Video;
use Illuminate\Contracts\Support\Arrayable;

class VideoPreview implements Arrayable
{
	private $video;

	public function __construct(Video $video)
	{
		$this->video = $video;
	}

	public function __toString()
	{
		return $this->video->url;
	}

	public function toArray()
	{
		return [
			'src' => $this->video->url,
			'width' => 0,
			'height' => 0
		];
	}
}