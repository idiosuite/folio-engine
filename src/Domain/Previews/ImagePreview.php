<?php

namespace App\Domain\Previews;

use Illuminate\Contracts\Support\Arrayable;
use \App\Domain\Images\Image as ImageInterface;
use App\Themes\Publishers\Images\Used\UsedImagesFinder;
use App\Themes\Publishers\Images\Used\UsedImagesProvider;

class ImagePreview implements UsedImagesProvider, Arrayable
{
    private $image;

	public function __construct(ImageInterface $image)
	{
		$this->image = $image;
	}

    public function __toString()
	{
		return (string)$this->image;
	}

	public function findUsedImages(UsedImagesFinder $usedImagesFinder)
	{
		if ( ! $this->image instanceof UsedImagesProvider) {
			return;
		}
		
		$this->image->findUsedImages($usedImagesFinder);
	}
    
    public function getImage()
    {
        return $this->image;
    }
	
	public function toArray()
	{
		return $this->image->toArray();
	}
}