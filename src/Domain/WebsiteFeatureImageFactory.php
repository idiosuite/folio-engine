<?php

namespace App\Domain;

use App\Domain\Post;
use App\Domain\Images\Image;
use App\Themes\Publishers\Manifests\Manifest;
use App\Themes\Publishers\Manifests\ManifestFactory;

class WebsiteFeatureImageFactory
{
    private $manifest;
    private $websiteFeatureManifest;
    private $image;

    private function __construct(Image $image = null)
    {
        $this->websiteFeatureManifest = ManifestFactory::websiteFeatureImages()->loadIfNotLoaded();
        $this->manifest = app(Manifest::class);
        $this->image = $image;
    }

    public static function fromPost(Post $post)
    {
        return static::fromImage($post->websiteFeature);
    }

    private static function fromImage(Image $image = null)
    {
        return (new static($image))->get();
    }

    private function get()
    {
        if ( ! $this->image) {
            return;
        }

        $absolutePath = $this->getCompressedImageAbsolutePath();

        if (! $absolutePath || ! file_exists($absolutePath)) {
            return;
        }

        return ImageFactory::make(
            $this->getRelativePath($absolutePath)
        );
    }

    private function getCompressedImageAbsolutePath()
    {
        return $this->getAbsolutePathForFirstOutput(
            $this->manifest,
            $this->getAbsolutePath()
        );
    }

    private function getAbsolutePath()
    {
        return $this->getAbsolutePathForFirstOutput(
            $this->websiteFeatureManifest,
            $this->image->getSourcePath()
        );
    }

    private function getAbsolutePathForFirstOutput($manifest, $key)
    {
        $outputs = $manifest
            ->getEntry($key)
            ->getOutputs();

        if ( ! $outputs) {
            return;
        }
        
        return $outputs[0]['path'];
    }

    private function getRelativePath(string $absolutePath)
    {
        return str_replace(
            env('IMAGES_DESTINATION_PATH'),
            '',
            $absolutePath
        );
    }
}