<?php

namespace Tests;

use App\App;
use App\Portfolio;
use App\Themes\Publishers\Images\ImagePublisherFactory;
use App\Themes\Publishers\Images\Testing\InteractsWithManifest;
use Illuminate\Testing\TestResponse;
use Symfony\Component\HttpFoundation\Request;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

abstract class FeatureTestCase extends TestCase
{
	use InteractsWithManifest;

	private $root;

	/** @before */
	public function setUpVfsStream()
	{
		$structure = [
			'contents' => [],
			'public' => [
				'images' => [],
			],
			'theme' => [],
		];

		$this->root = vfsStream::setup('folio', null, $structure);

		$this->bootstrapContents();
		$this->getPublishedManifestInstance();
	}

	/** @before */
	public function setUpImagePublisherFactory()
	{
		ImagePublisherFactory::test();
	}
	

	/** @after */
	public function tearDownImagePublisherFactory()
	{
		ImagePublisherFactory::reset();
	}
	
	
	private function bootstrapContents()
	{
		vfsStream::copyFromFileSystem(
			__DIR__ . '/../contents/', 
			$this->root->getChild('contents')
		);
		
		vfsStream::copyFromFileSystem(
			__DIR__ . '/../themes/Folio/', 
			$this->root->getChild('theme')
		);

		$this->overrideSettings(['theme' => 'test']);
	}

	protected function overrideSettings(array $settingsOverride)
	{
		$settingsFilePath = vfsStream::url('folio/contents/settings.json');
		$settingsJsonInput = file_get_contents($settingsFilePath);
		$settingsArray = json_decode($settingsJsonInput, $assoc = true);
		$settingsArray = array_merge($settingsArray, $settingsOverride);
		$settingsJsonOutput = json_encode($settingsArray);
		file_put_contents($settingsFilePath, $settingsJsonOutput);
	}

	protected function assertSeeSettings($response)
	{
		$settingsArray = $this->getSettings();

		$response->assertSee($settingsArray['title']);
		$response->assertSee($settingsArray['description']);
		$response->assertSee($settingsArray['logo']);
		$response->assertSee($settingsArray['footer']);
		$response->assertSee($settingsArray['email']);
		$response->assertSee(implode(', ', $settingsArray['keywords']));
	}

	protected function assertSeeUtilities($response, $modifier = 'page')
	{
		$response->assertSee(sprintf('2007 &mdash; %s', date('Y')), $escape = false);
		$response->assertSee(env('BASE_URL'));
		$response->assertSee(sprintf('Container--%s', $modifier));
	}

	protected function assertSeeAssets($response)
	{
		$response->assertSee(env('BASE_URL') . '/_assets/test/template.css');
	    $response->assertSee(env('BASE_URL') . '/_assets/test/template.js');
	}

	protected function assertSeeMenu($response)
	{
		$response->assertSee('Selected Works');
		$response->assertSee('My Test Page');
		$response->assertSee('Contact');
	}

	protected function assertDontSeeMenuItem($title)
	{
		$posts = app(Portfolio::class)
			->getMenu()
			->toArray();

		$titles = array_map(
			function ($post) {
				return $post->title;
			},
			$posts
		);

		return $this->assertNotContains($title, $titles);
	}

	protected function assertDontSeeDefaultPage($response)
	{
		$response->assertDontSee('Welcome');
		$response->assertDontSee('Fusce rhoncus elit at malesuada mollis');
	}

	protected function assertSeeDefaultPage($response)
	{
		$response->assertSee('Welcome');
		$response->assertSee('Fusce rhoncus elit at malesuada mollis');
	}

	protected function assertDontSeeGallery($response)
	{
		$response->assertDontSee('My Test project');
	}

	protected function assertSeeGallery($response)
	{
		$response->assertSee(env('BASE_PATH_URL') . '/test-project/preview.svg');
		$response->assertSee('/project/test-project');
		$response->assertSee('My Test project');
		$response->assertSee('November 2021');
	    $response->assertSee('Lorem ipsum dolor sit amet consectetur adipiscing elit.');
	}

	protected function assertSeeLinks($response)
	{
		$response->assertSee('email');
		$response->assertSee('linkedin');
		$response->assertSee('vimeo');
		$response->assertSee('behance');
		$response->assertSee('instagram');
	}

	protected function getSettings()
	{
		$settingsFilePath = vfsStream::url('folio/contents/settings.json');
		$settingsJsonInput = file_get_contents($settingsFilePath);
		return json_decode($settingsJsonInput, $assoc = true);
	}
}