<?php

namespace Tests\Unit\Repositories\FileSystem;

use App\Repositories\Json\JsonRepository;
use App\Repositories\Repository;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class SettingsRepositoryTest extends TestCase
{
	private $root;
	private $basePath;
	private $instance;

	/** @before */
	public function setUpVfsStream()
	{
		$structure = [
			'contents' => []
		];

		$this->root = vfsStream::setup('folio', null, $structure);
		$this->basePath = vfsStream::url('folio') . '/contents';
		$this->instance = new JsonRepository($this->basePath . '/settings.json');
	}
	
	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull($this->instance);
	}

	/** @test */
	public function it_implements_repository_interface()
	{
	    $this->assertInstanceOf(Repository::class, $this->instance);
	}

	/** @test */
	public function it_returns_empty_settings_if_json_file_does_not_exists()
	{
		$this->assertFileDoesNotExist(vfsStream::url('folio/contents/settings.json'));

	    $this->assertEquals([], $this->instance->all());
	}

	/** @test */
	public function it_returns_empty_settings_if_json_file_exists_but_is_empty()
	{
		$this->eraseSettings();

	    $this->assertEquals([], $this->instance->all());
	}

	/** @test */
	public function it_returns_settings_if_json_file_exists()
	{
		$this->setSettings(['foo' => 'bar']);

		$this->instance->refresh();

	    $this->assertEquals(['foo' => 'bar'], $this->instance->all());
	}

	/** @test */
	public function it_allows_to_add_settings_at_runtime()
	{
		$this->setSettings(['foo' => 'bar']);

		$this->instance->refresh();

		$this->instance->merge(['bar' => 'baz']);

	    $this->assertEquals(
	    	['foo' => 'bar', 'bar' => 'baz'], 
	    	$this->instance->all()
	    );
	}

	private function eraseSettings()
	{
		return $this->setSettings([]);
	}

	private function setSettings(array $settings)
	{
		file_put_contents(
			vfsStream::url('folio/contents/settings.json'), 
			json_encode($settings)
		);
	}
}