<?php

namespace Tests\Unit\Repositories\FileSystem\Guessers;

use App\Discovery\SlugFactory;
use App\Domain\Image;
use App\Domain\Post;
use App\Repositories\FileSystem\Guessers\Guesser;
use App\Repositories\FileSystem\Guessers\HeroGuesser;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class HeroGuesserTest extends TestCase
{
	private $root;
	private $basePath;
	
	/** @before */
	public function setUpVfsStream()
	{
		$structure = [
			'contents' => [
				'images' => [
					'baz' => [],
					'barsvg' => [
						'hero.svg' => ''
					],
					'barjpg' => [
						'hero.jpg' => ''
					],
					'barpng' => [
						'hero.png' => ''
					]
				]
			]
		];

		$this->root = vfsStream::setup('folio', null, $structure);
		$this->basePath = vfsStream::url('folio') . '/contents';
	}
	
	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new HeroGuesser($this->basePath, []));
	}

	/** @test */
	public function it_implements_guesser_interface()
	{
	    $this->assertInstanceOf(Guesser::class, new HeroGuesser($this->basePath, []));
	}

	/** @test */
	public function it_returns_hardcoded_hero_relative()
	{
	    $instance = new HeroGuesser(
	    	$this->basePath, 
	    	['slug' => SlugFactory::fromString('foo'), 'hero' => 'foobar.jpg']
	    );

	    $this->assertEquals(
	    	'foobar.jpg', 
	    	$instance->get()['hero']
	    );
	}

	/** @test */
	public function it_returns_hardcoded_hero_url()
	{
	    $instance = new HeroGuesser(
	    	$this->basePath, 
	    	['slug' => SlugFactory::fromString('foo'), 'hero' => 'http://exmaple.com/foobar.jpg']
	    );

	    $this->assertEquals(
	    	'http://exmaple.com/foobar.jpg', 
	    	$instance->get()['hero']
	    );
	}

	/** @test */
	public function it_returns_guessed_hero_svg()
	{
	    $instance = new HeroGuesser(
	    	$this->basePath, 
	    	['slug' => SlugFactory::fromString('barsvg')]
	    );

	    $this->assertEquals(
	    	'barsvg/hero.svg', 
	    	$instance->get()['hero']
	    );
	}

	/** @test */
	public function it_returns_guessed_hero_jpg()
	{
	    $instance = new HeroGuesser(
	    	$this->basePath, 
	    	['slug' => SlugFactory::fromString('barjpg')]
	    );

	    $this->assertEquals(
	    	'barjpg/hero.jpg', 
	    	$instance->get()['hero']
	    );
	}

	/** @test */
	public function it_returns_guessed_hero_png()
	{
	    $instance = new HeroGuesser(
	    	$this->basePath, 
	    	['slug' => SlugFactory::fromString('barpng')]
	    );

	    $this->assertEquals(
	    	'barpng/hero.png', 
	    	$instance->get()['hero']
	    );
	}

	/** @test */
	public function it_returns_null_if_hero_cannot_be_guessed()
	{
	    $instance = new HeroGuesser(
	    	$this->basePath, 
	    	['slug' => SlugFactory::fromString('baz')]
	    );

	    $this->assertArrayNotHasKey('hero', $instance->get());
	}

	/** @test */
	public function it_gives_precedence_to_hardcoded_hero()
	{
	    $instance = new HeroGuesser(
	    	$this->basePath, 
	    	['slug' => SlugFactory::fromString('barsvg'), 'hero' => 'foobar.png']
	    );

	    $this->assertEquals(
	    	'foobar.png', 
	    	$instance->get()['hero']
	    );
	}

	/** @test */
	public function it_guesses_hero_if_hero_is_hardcoded_but_is_null()
	{
	    $instance = new HeroGuesser(
	    	$this->basePath, 
	    	['slug' => SlugFactory::fromString('barsvg'), 'hero' => null]
	    );

	    $this->assertEquals(
	    	'barsvg/hero.svg', 
	    	$instance->get()['hero']
	    );
	}
}