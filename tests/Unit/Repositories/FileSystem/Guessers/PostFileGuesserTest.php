<?php

namespace Tests\Unit\Repositories\FileSystem\Guessers;

use App\Discovery\SlugFactory;
use App\Domain\Image;
use App\Domain\Post;
use App\Repositories\FileSystem\Guessers\Guesser;
use App\Repositories\FileSystem\Guessers\PostFileGuesser;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class PostFileGuesserTest extends TestCase
{
	private $root;
	private $basePath;
	
	/** @before */
	public function setUpVfsStream()
	{
		$structure = [
			'contents' => [
				'pages' => [
					'foo.md' => '---
foo: bar
---
foobar',
					'nested' => [
						'bar.md' => '---
foo: bar
---
barbaz'
					]
				]
			]
		];

		$this->root = vfsStream::setup('folio', null, $structure);
		$this->basePath = vfsStream::url('folio') . '/contents';
	}
	
	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new PostFileGuesser($this->basePath, []));
	}

	/** @test */
	public function it_implements_guesser_interface()
	{
	    $this->assertInstanceOf(Guesser::class, new PostFileGuesser($this->basePath, []));
	}

	/** @test */
	public function it_returns_array()
	{
		$instance = new PostFileGuesser($this->basePath, [
			'type' => 'page',
			'slug' => SlugFactory::fromString('foo')
		]);
	    
	    $result = $instance->get();

	    $this->assertIsArray($result);
	    $this->assertArrayHasKey('exists', $result);
	    $this->assertTrue($result['exists']);
	    $this->assertArrayHasKey('content', $result);
	    $this->assertEquals('foobar', trim($result['content']));
	}

	/** @test */
	public function it_returns_array_for_nested()
	{
		$instance = new PostFileGuesser($this->basePath, [
			'type' => 'page',
			'slug' => SlugFactory::fromFile([
				'path' => 'nested', 
				'filename' => 'bar.md'
			])
		]);
	    
	    $result = $instance->get();

	    $this->assertIsArray($result);
	    $this->assertArrayHasKey('exists', $result);
	    $this->assertTrue($result['exists']);
	    $this->assertArrayHasKey('content', $result);
	    $this->assertEquals('barbaz', trim($result['content']));
	}
}