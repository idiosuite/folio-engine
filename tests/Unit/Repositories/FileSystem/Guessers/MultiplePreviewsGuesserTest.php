<?php

namespace Tests\Unit\Repositories\FileSystem\Guessers;

use App\Discovery\SlugFactory;
use App\Testing\InteractsWithFileSystem;
use App\Testing\InteractsWithImage;
use App\Domain\Image;
use App\Testing\PreviewTest;
use App\Domain\Post;
use App\Repositories\FileSystem\Guessers\Guesser;
use App\Repositories\FileSystem\Guessers\MultiplePreviewsGuesser;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class MultiplePreviewsGuesserTest extends TestCase
{
    use InteractsWithFileSystem;
    use InteractsWithImage;

    /** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new MultiplePreviewsGuesser($basePath = null, []));
	}

	/** @test */
	public function it_implements_guesser_interface()
	{
	    $this->assertInstanceOf(Guesser::class, new MultiplePreviewsGuesser($basePath = null, []));
	}

    /** @test */
	public function it_returns_previous_keys()
	{
	    $instance = new MultiplePreviewsGuesser(
	    	vfsStream::url('folio/contents'), 
	    	[
	    		'slug' => SlugFactory::fromString('foo'), 
	    	]
	    );

	    $this->assertArrayHasKey('slug', $instance->get());
	}

	/** @test */
	public function it_returns_empty_array_by_default()
	{
	    $instance = new MultiplePreviewsGuesser(
	    	vfsStream::url('folio/contents'), 
	    	[ 'slug' => SlugFactory::fromString('foo') ]
	    );

        $previews = $instance->get()['previews'];

        $this->assertIsArray($previews);
	    $this->assertCount(0, $previews);
	}

    /** @test */
	public function it_returns_array_from_qualified_json_file_for_image_with_relative_path()
	{
		PreviewTest::writeJson(
			[ PreviewTest::previewImage('foo/foo.jpg') ]
		);

		$instance = new MultiplePreviewsGuesser(
	    	vfsStream::url('folio/contents'), 
	    	[ 'slug' => SlugFactory::fromString('foo') ]
	    );

        $previews = $instance->get()['previews'];

		$this->assertIsArray($previews);
	    $this->assertCount(1, $previews);
        
        $this->assertArrayHasKey('type', $previews[0]);
        $this->assertEquals('image', $previews[0]['type']);
        
        $this->assertArrayHasKey('src', $previews[0]);
        $this->assertEquals('foo/foo.jpg', $previews[0]['src']);
	}

    /** @test */
	public function it_returns_array_from_qualified_json_file_for_image_with_relative_path_leading_slash()
	{
		PreviewTest::writeJson(
			[ PreviewTest::previewImage('/foo/foo.jpg') ]
		);
       
	    $instance = new MultiplePreviewsGuesser(
	    	vfsStream::url('folio/contents'), 
	    	[ 'slug' => SlugFactory::fromString('foo') ]
	    );

        $previews = $instance->get()['previews'];

        $this->assertIsArray($previews);
	    $this->assertCount(1, $previews);
        
        $this->assertArrayHasKey('type', $previews[0]);
        $this->assertEquals('image', $previews[0]['type']);
        
        $this->assertArrayHasKey('src', $previews[0]);
        $this->assertEquals('foo/foo.jpg', $previews[0]['src']);
	}

    /** @test */
	public function it_returns_array_from_qualified_json_file_for_image_with_image_file_name()
	{
		PreviewTest::writeJson(
			[ PreviewTest::previewImage('foo.jpg') ]
		);

	    $instance = new MultiplePreviewsGuesser(
	    	vfsStream::url('folio/contents'), 
	    	[ 'slug' => SlugFactory::fromString('foo') ]
	    );

        $previews = $instance->get()['previews'];

        $this->assertIsArray($previews);
	    $this->assertCount(1, $previews);
        
        $this->assertArrayHasKey('type', $previews[0]);
        $this->assertEquals('image', $previews[0]['type']);
        
        $this->assertArrayHasKey('src', $previews[0]);
        $this->assertEquals('foo/foo.jpg', $previews[0]['src']);
	}

    /** @test */
	public function it_returns_array_from_qualified_json_file_for_vimeo_video()
	{
        PreviewTest::writeJson(
			[ PreviewTest::vimeoVideo('http://www.vimeo.com/12345') ]
		);
		
	    $instance = new MultiplePreviewsGuesser(
	    	vfsStream::url('folio/contents'), 
	    	[ 'slug' => SlugFactory::fromString('foo') ]
	    );

        $previews = $instance->get()['previews'];

        $this->assertIsArray($previews);
	    $this->assertCount(1, $previews);
        
        $this->assertArrayHasKey('type', $previews[0]);
        $this->assertEquals('video', $previews[0]['type']);
        
        $this->assertArrayHasKey('src', $previews[0]);
        $this->assertEquals('http://www.vimeo.com/12345', $previews[0]['src']);
	}

	/** @test */
	public function it_returns_array_from_qualified_json_file_for_hero_image()
	{
		PreviewTest::writeJson(
			[ PreviewTest::heroImage('foo/foo.jpg') ]
		);

		$instance = new MultiplePreviewsGuesser(
	    	vfsStream::url('folio/contents'), 
	    	[ 'slug' => SlugFactory::fromString('foo') ]
	    );

        $previews = $instance->get()['previews'];

		$this->assertIsArray($previews);
	    $this->assertCount(1, $previews);
        
        $this->assertArrayHasKey('type', $previews[0]);
        $this->assertEquals('image', $previews[0]['type']);
        
        $this->assertArrayHasKey('src', $previews[0]);
        $this->assertEquals('foo/foo.jpg', $previews[0]['src']);

		$this->assertArrayHasKey('media', $previews[0]);
        $this->assertEquals(['hero'], $previews[0]['media']);
	}
}