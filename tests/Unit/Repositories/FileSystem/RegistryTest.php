<?php

namespace Tests\Unit\Repositories\FileSystem;

use App\Repositories\FileSystem\Registry;
use App\Repositories\PostsRepository;
use App\Repositories\Repository;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class RegistryTest extends TestCase
{
	private $root;
	private $basePath;

	/** @before */
	public function setUpVfsStream()
	{
		$structure = [
			'contents' => []
		];

		$this->root = vfsStream::setup('folio', null, $structure);
		$this->basePath = vfsStream::url('folio') . '/contents';
	}
	
	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new Registry($this->basePath));
	}

	/** @test */
	public function it_implements_registry_interface()
	{
	    $this->assertInstanceOf(
	    	\App\Repositories\Registry::class, 
	    	new Registry($this->basePath)
	    );
	}

	/** @test */
	public function it_returns_base_path()
	{
		$instance = new Registry($this->basePath);

	    $this->assertEquals(
	    	$this->basePath, 
	    	$instance->getBasePath()
	    );
	}

	/** @test */
	public function it_returns_instance_of_links_repository()
	{
		$instance = new Registry($this->basePath);

	    $this->assertInstanceOf(
	    	Repository::class, 
	    	$instance->links()
	    );
	}

	/** @test */
	public function it_returns_same_instance_of_links_repository()
	{
		$instance = new Registry($this->basePath);

	    $this->assertSame(
	    	$instance->links(), 
	    	$instance->links()
	    );
	}

	/** @test */
	public function it_returns_instance_of_settings_repository()
	{
		$instance = new Registry($this->basePath);

	    $this->assertInstanceOf(
	    	Repository::class, 
	    	$instance->settings()
	    );
	}

	/** @test */
	public function it_returns_same_instance_of_settings_repository()
	{
		$instance = new Registry($this->basePath);

	    $this->assertSame(
	    	$instance->settings(), 
	    	$instance->settings()
	    );
	}

	/** @test */
	public function it_returns_instance_of_posts_repository_for_projects()
	{
		$instance = new Registry($this->basePath);

		$this->assertInstanceOf(PostsRepository::class, $instance->projects());
		$this->assertEquals('project', $instance->projects()->getType());
	}

	/** @test */
	public function it_returns_same_instance_of_posts_repository_for_projects()
	{
		$instance = new Registry($this->basePath);

		$this->assertSame(
	    	$instance->projects(), 
	    	$instance->projects()
	    );
	}

	/** @test */
	public function it_returns_instance_of_posts_repository_for_pages()
	{
		$instance = new Registry($this->basePath);

		$this->assertInstanceOf(PostsRepository::class, $instance->pages());
		$this->assertEquals('page', $instance->pages()->getType());
	}

	/** @test */
	public function it_returns_same_instance_of_posts_repository_for_pages()
	{
		$instance = new Registry($this->basePath);

		$this->assertSame(
	    	$instance->pages(), 
	    	$instance->pages()
	    );
	}

	/** @test */
	public function it_returns_instance_of_clients_repository()
	{
		$instance = new Registry($this->basePath);

	    $this->assertInstanceOf(
	    	Repository::class, 
	    	$instance->clients()
	    );
	}

	/** @test */
	public function it_returns_same_instance_of_clients_repository()
	{
		$instance = new Registry($this->basePath);

	    $this->assertSame(
	    	$instance->clients(), 
	    	$instance->clients()
	    );
	}
}