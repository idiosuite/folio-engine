<?php

namespace Tests\Unit\Repositories\Array;

use App\Repositories\Json\JsonRepository;
use App\Repositories\Registry;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class JsonRepositoryTest extends TestCase
{
	private $root;
	private $basePath;
	
	/** @before */
	public function setUpVfsStream()
	{
		$structure = [
			'foo.json' => '{"foo": {"bar": "baz"}}',
		];

		$this->root = vfsStream::setup('folio', null, $structure);
		$this->basePath = vfsStream::url('folio') . '/foo.json';
	}

	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new JsonRepository($this->basePath));
	}

	/** @test */
	public function it_implements_repository_interface()
	{
	    $this->assertInstanceOf(
	    	\App\Repositories\Repository::class, 
	    	new JsonRepository($this->basePath)
	    );
	}

	/** @test */
	public function it_provides_accessor()
	{
	    $instance = new JsonRepository($this->basePath);

	    $this->assertEquals(['foo' => ['bar' => 'baz']], $instance->all());
	}

	/** @test */
	public function it_returns_data_subtree()
	{
	    $instance = new JsonRepository($this->basePath);

	    $this->assertEquals(['bar' => 'baz'], $instance->get('foo'));
	}

	/** @test */
	public function it_returns_data_subtree_with_dot_notation()
	{
	    $instance = new JsonRepository($this->basePath);

	    $this->assertEquals('baz', $instance->get('foo.bar'));
	}

	/** @test */
	public function it_returns_empty_array_if_file_not_exists()
	{
		$instance = new JsonRepository('vfs://folio/notexists.json');

	    $this->assertEquals([], $instance->all());
	}

	/** @test */
	public function it_returns_null_for_attribute_if_file_not_exists()
	{
		$instance = new JsonRepository('vfs://folio/notexists.json');

	    $this->assertNull($instance->get('baz'));
	}

	/** @test */
	public function it_allows_to_merge_data_at_runtime_and_returns_self()
	{
		$instance = new JsonRepository($this->basePath);

		$this->assertSame(
			$instance, 
			$instance->merge(['foobar' => 'foobaz'])
		);

	    $this->assertEquals(
	    	['foo' => ['bar' => 'baz'], 'foobar' => 'foobaz'], 
	    	$instance->all()
	    );
	}
}