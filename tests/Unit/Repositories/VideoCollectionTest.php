<?php

namespace Tests\Unit\Repositories;

use App\Domain\Video;
use App\Repositories\VideoCollection;
use App\Testing\VideoTest;
use Tests\TestCase;

class VideoCollectionTest extends TestCase
{
	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new VideoCollection);
	}

    /** @test */
	public function it_implements_countable()
	{
	    $this->assertInstanceOf(\Countable::class, new VideoCollection);
	}

    /** @test */
	public function it_implements_iterator()
	{
	    $this->assertInstanceOf(\Iterator::class, new VideoCollection);
	}

    /** @test */
	public function it_implements_arrayable()
	{
	    $this->assertInstanceOf(\Illuminate\Contracts\Support\Arrayable::class, new VideoCollection);
	}

    /** @test */
	public function it_implements_provides_api_resource()
	{
	    $this->assertInstanceOf(\App\Resources\ProvidesApiResource::class, new VideoCollection);
	}

    /** @test */
	public function it_is_empty_by_default()
	{
	    $this->assertCount(0, new VideoCollection);
	}

    /** @test */
	public function it_assert_it_is_empty()
	{
        $instance = new VideoCollection;
	    $this->assertTrue($instance->isEmpty());
	}

    /** @test */
	public function it_can_be_instantiated_with_array_of_videos_as_arrays()
	{
        $collection = new VideoCollection([
            $expected = VideoTest::vimeoVideo()
        ]);

	    $this->assertCount(1, $collection);
        $this->assertSame($expected, $collection->getFirst()->toArray());
	}

    /** @test */
	public function it_can_be_instantiated_with_array_of_videos()
	{
        $collection = new VideoCollection([
            $expected = new Video( VideoTest::vimeoVideo() )
        ]);

	    $this->assertCount(1, $collection);
        $this->assertSame($expected, $collection->getFirst());
	}

    /** @test */
	public function it_asserts_it_is_not_empty()
	{
        $instance = new VideoCollection([new Video()]);
	    $this->assertFalse($instance->isEmpty());
	}

    /** @test */
	public function it_returns_empty_video_as_first_item_if_empty()
	{
        $instance = new VideoCollection();

        $firstVideo = $instance->getFirst();

	    $this->assertInstanceOf(Video::class, $firstVideo);
        $this->assertEquals([], $firstVideo->toArray());
	}

    /** @test */
	public function it_returns_video_as_first_item()
	{
        $instance = new VideoCollection([
            $expected = VideoTest::vimeoVideo()
        ]);

        $firstVideo = $instance->getFirst();

	    $this->assertInstanceOf(Video::class, $firstVideo);
        $this->assertEquals($expected, $firstVideo->toArray());
	}

    /** @test */
	public function it_returns_video_at_index()
	{
        $instance = new VideoCollection([
            VideoTest::vimeoVideo('12345'),
            $expected = VideoTest::vimeoVideo('56789'),
        ]);

        $video = $instance->getRow(1);

	    $this->assertInstanceOf(Video::class, $video);
        $this->assertEquals($expected, $video->toArray());
	}

    /** @test */
	public function it_can_be_converted_to_array()
	{
        $videos = new VideoCollection([
            $expectedVideo1 = VideoTest::vimeoVideo('12345'),
            $expectedVideo2 = VideoTest::vimeoVideo('56789'),
        ]);

        $this->assertCount(2, $videos);

        $array = $videos->toArray();

	    $this->assertIsArray($array);
        $this->assertCount(2, $array);
        $this->assertInstanceOf(Video::class, $array[0]);
        $this->assertEquals($expectedVideo1, $array[0]->toArray());
        $this->assertInstanceOf(Video::class, $array[1]);
        $this->assertEquals($expectedVideo2, $array[1]->toArray());
	}

    /** @test */
	public function it_returns_api_resource()
	{
        $videos = new VideoCollection([
            VideoTest::vimeoVideo('12345'),
            VideoTest::vimeoVideo('56789'),
        ]);

        $apiResource = $videos->apiResource();

        $this->assertInstanceOf(
            \App\Resources\CollectionResource::class, 
            $apiResource
        );
	}

	/** @test */
	public function it_filters_videos_by_provider()
	{
        $videos = new VideoCollection([
            $expected = VideoTest::vimeoVideo(),
            VideoTest::youtubeVideo(),
        ]);

        $this->assertCount(2, $videos);

        $vimeoVideos = $videos->filterByProvider('vimeo');

	    $this->assertCount(1, $vimeoVideos);
        $this->assertNotSame($videos, $vimeoVideos);
        $this->assertEquals($expected, $vimeoVideos->getFirst()->toArray());
	}

    /** @test */
	public function it_filters_videos_by_id()
	{
        $videos = new VideoCollection([
            VideoTest::vimeoVideo('12345'),
            $expected = VideoTest::vimeoVideo('56789'),
        ]);

        $this->assertCount(2, $videos);

        $videoWithId = $videos->filterById('56789');

	    $this->assertCount(1, $videoWithId);
        $this->assertNotSame($videos, $videoWithId);
        $this->assertEquals($expected, $videoWithId->getFirst()->toArray());
	}
}