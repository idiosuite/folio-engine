<?php

namespace Tests\Unit\Repositories\Array;

use App\Repositories\Array\ArrayRepository;
use App\Repositories\Registry;
use Tests\TestCase;

class ArrayRepositoryTest extends TestCase
{
	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new ArrayRepository);
	}

	/** @test */
	public function it_implements_repository_interface()
	{
	    $this->assertInstanceOf(
	    	\App\Repositories\Repository::class, 
	    	new ArrayRepository
	    );
	}

	/** @test */
	public function it_provides_accessor()
	{
	    $instance = new ArrayRepository;

	    $this->assertEquals([], $instance->all());
	}

	/** @test */
	public function it_can_be_instantiated_with_array()
	{
	    $instance = new ArrayRepository(['foo' => 'bar']);

	    $this->assertEquals(['foo' => 'bar'], $instance->all());
	}

	/** @test */
	public function it_returns_data_subtree()
	{
	    $instance = new ArrayRepository(['foo' => 'bar']);

	    $this->assertEquals('bar', $instance->get('foo'));
	}

	/** @test */
	public function it_returns_data_subtree_with_dot_notation()
	{
	    $instance = new ArrayRepository(['foo' => ['bar' => 'baz']]);

	    $this->assertEquals('baz', $instance->get('foo.bar'));
	}

	/** @test */
	public function it_throws_if_data_subtree_not_found()
	{
		$instance = new ArrayRepository(['foo' => 'bar']);

	    $this->assertNull($instance->get('baz'));
	}
}