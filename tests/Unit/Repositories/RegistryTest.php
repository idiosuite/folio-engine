<?php

namespace Tests\Unit\Repositories;

use App\Testing\InteractsWithImage;
use App\Repositories\Array\ArrayRepository;
use App\Repositories\Registry;
use App\Repositories\Repository;
use Tests\TestCase;

class RegistryTest extends TestCase
{
	use InteractsWithImage;

	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new Registry);
	}

	/** @test */
	public function it_supports_magic_methods()
	{
		$this->expectException(\Exception::class);
		$this->expectExceptionMessage("Please register repository 'foo'");

		$instance = new Registry;

	    $instance->foo();
	}

	/** @test */
	public function it_registers_a_bespoke_repository()
	{
	    $instance = new Registry;

	    $delegate = new ArrayRepository;
	    
	    $this->assertNull($instance->register('foo', $delegate));
	}

	/** @test */
	public function it_returns_registered_bespoke_repository()
	{
	    $instance = new Registry;

	    $delegate = new ArrayRepository;
	    $instance->register('foo', $delegate);

	    $this->assertInstanceOf(Repository::class, $instance->foo());
	}

	/** @test */
	public function it_returns_hydrated_images()
	{
	    $instance = new Registry;

	    $delegate = new ArrayRepository([
	    	'bar' => '/path/to/image.png'
	    ]);
	    $instance->register('foo', $delegate);

	    $this->assertInstanceOf(
	    	\App\Domain\Images\Image::class, 
	    	$instance->foo()->get('bar')
	    );
	}

	/** @test */
	public function it_returns_repository_keys()
	{
	    $instance = new Registry;

	    $this->assertEquals([], $instance->getRepositoryKeys());

	    $delegate = new ArrayRepository;
	    $instance->register('foo', $delegate);

	    $this->assertEquals(['foo'], $instance->getRepositoryKeys());
	}
}