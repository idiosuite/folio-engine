<?php

namespace Tests\Unit\Parsers;

use App\Parsers\TextBlocksParser;
use App\Parsers\Block;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class TextBlocksParserTest extends TestCase
{
    /** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(TextBlocksParser::fromString(''));
	}

    /** @test */
	public function it_returns_empty_array_with_empty_string()
	{
        $result = TextBlocksParser::fromString('')->get();

	    $this->assertIsArray($result);
        $this->assertCount(0, $result);
	}

    /** @test */
	public function it_returns_default_key_without_blocks()
	{
        $result = TextBlocksParser::fromString('foobar')->get();

	    $this->assertIsArray($result);
        $this->assertCount(1, $result);
        $this->assertInstanceOf(Block::class, $result[0]);
        $this->assertEquals('markdown', $result[0]->getType());
        $this->assertEquals('default', $result[0]->getSlug());
        $this->assertEquals('foobar', $result[0]->getBody());
	}

    /** @test */
	public function it_returns_custom_key_without_blocks()
	{
        $result = TextBlocksParser::fromString('foobar', 'foo')->get();

	    $this->assertIsArray($result);
        $this->assertCount(1, $result);
        $this->assertInstanceOf(Block::class, $result[0]);
        $this->assertEquals('markdown', $result[0]->getType());
        $this->assertEquals('foo', $result[0]->getSlug());
        $this->assertEquals('foobar', $result[0]->getBody());
	}

    /** @test */
	public function it_returns_block_before_default_block()
	{
        $result = TextBlocksParser::fromString($this->getLines([
            "@markdown('foo')",
            'Foo',
            "@endmarkdown",
            'Bar'
        ]))->get();

	    $this->assertIsArray($result);
        $this->assertCount(2, $result);

        $this->assertInstanceOf(Block::class, $result[0]);
        $this->assertEquals('markdown', $result[0]->getType());
        $this->assertEquals('foo', $result[0]->getSlug());
        $this->assertEquals('Foo', $result[0]->getBody());

        $this->assertInstanceOf(Block::class, $result[1]);
        $this->assertEquals('markdown', $result[1]->getType());
        $this->assertEquals('default', $result[1]->getSlug());
        $this->assertEquals('Bar', $result[1]->getBody());
	}

    /** @test */
	public function it_returns_block_after_default_block()
	{
        $result = TextBlocksParser::fromString($this->getLines([
            'Bar',
            "@markdown('foo')",
            'Foo',
            "@endmarkdown",
        ]))->get();

	    $this->assertIsArray($result);
        $this->assertCount(2, $result);

        $this->assertInstanceOf(Block::class, $result[0]);
        $this->assertEquals('markdown', $result[0]->getType());
        $this->assertEquals('default', $result[0]->getSlug());
        $this->assertEquals('Bar', $result[0]->getBody());

        $this->assertInstanceOf(Block::class, $result[1]);
        $this->assertEquals('markdown', $result[1]->getType());
        $this->assertEquals('foo', $result[1]->getSlug());
        $this->assertEquals('Foo', $result[1]->getBody());
	}

    /** @test */
	public function it_returns_default_block_between_blocks()
	{
        $result = TextBlocksParser::fromString($this->getLines([
            "@markdown('foo')",
            'Foo',
            "@endmarkdown",
            'Bar',
            "@markdown('baz')",
            'Baz',
            "@endmarkdown",
        ]))->get();

	    $this->assertIsArray($result);
        $this->assertCount(3, $result);

        $this->assertInstanceOf(Block::class, $result[0]);
        $this->assertEquals('markdown', $result[0]->getType());
        $this->assertEquals('foo', $result[0]->getSlug());
        $this->assertEquals('Foo', $result[0]->getBody());

        $this->assertInstanceOf(Block::class, $result[1]);
        $this->assertEquals('markdown', $result[1]->getType());
        $this->assertEquals('default', $result[1]->getSlug());
        $this->assertEquals('Bar', $result[1]->getBody());

        $this->assertInstanceOf(Block::class, $result[2]);
        $this->assertEquals('markdown', $result[2]->getType());
        $this->assertEquals('baz', $result[2]->getSlug());
        $this->assertEquals('Baz', $result[2]->getBody());
	}

    /** @test */
	public function it_returns_default_block_between_blocks_same_line()
	{
        $result = TextBlocksParser::fromString($this->getLines([
            "@markdown('foo')",
            'Foo',
            "@endmarkdown Bar @markdown('baz')",
            'Baz',
            "@endmarkdown",
        ]))->get();

	    $this->assertIsArray($result);
        $this->assertCount(3, $result);

        $this->assertInstanceOf(Block::class, $result[0]);
        $this->assertEquals('markdown', $result[0]->getType());
        $this->assertEquals('foo', $result[0]->getSlug());
        $this->assertEquals('Foo', $result[0]->getBody());

        $this->assertInstanceOf(Block::class, $result[1]);
        $this->assertEquals('markdown', $result[1]->getType());
        $this->assertEquals('default', $result[1]->getSlug());
        $this->assertEquals('Bar', $result[1]->getBody());

        $this->assertInstanceOf(Block::class, $result[2]);
        $this->assertEquals('markdown', $result[2]->getType());
        $this->assertEquals('baz', $result[2]->getSlug());
        $this->assertEquals('Baz', $result[2]->getBody());
	}

    /** @test */
	public function it_returns_multiple_blocks_without_default_block()
	{
        $result = TextBlocksParser::fromString($this->getLines([
            "@markdown('foo')",
            'Foo',
            "@endmarkdown",
            "@markdown('bar')",
            'Bar',
            "@endmarkdown",
        ]))->get();

	    $this->assertIsArray($result);
        $this->assertCount(2, $result);

        $this->assertInstanceOf(Block::class, $result[0]);
        $this->assertEquals('markdown', $result[0]->getType());
        $this->assertEquals('foo', $result[0]->getSlug());
        $this->assertEquals('Foo', $result[0]->getBody());

        $this->assertInstanceOf(Block::class, $result[1]);
        $this->assertEquals('markdown', $result[1]->getType());
        $this->assertEquals('bar', $result[1]->getSlug());
        $this->assertEquals('Bar', $result[1]->getBody());
	}

    /** @test */
	public function it_provides_a_way_to_find_block_by_slug()
	{
        $instance = TextBlocksParser::fromString($this->getLines([
            "@markdown('foo')",
            'Foo',
            "@endmarkdown",
            "@markdown('bar')",
            'Bar',
            "@endmarkdown",
            "@markdown('baz')",
            'Baz',
            "@endmarkdown"
        ]));

        $result = $instance->getWithSlug('bar');

        $this->assertNotNull($result);
        $this->assertInstanceOf(Block::class, $result);
        $this->assertEquals('markdown', $result->getType());
        $this->assertEquals('bar', $result->getSlug());
        $this->assertEquals('Bar', $result->getBody());
	}

    /** @test */
	public function it_returns_null_if_slug_cannot_be_found()
	{
        $instance = TextBlocksParser::fromString($this->getLines([
            "@markdown('foo')",
            'Foo',
            "@endmarkdown"
        ]));

        $this->assertNull($instance->getWithSlug('bar'));
	}

    /** @test */
	public function it_merges_broken_default_blocks_into_one()
	{
        $instance = TextBlocksParser::fromString($this->getLines([
            'Bar',
            "@markdown('foo')",
            'Foo',
            "@endmarkdown",
            'Baz'
        ]));

        $result = $instance->get();

	    $this->assertIsArray($result);
        $this->assertCount(2, $result);

        $this->assertInstanceOf(Block::class, $result[0]);
        $this->assertEquals('markdown', $result[0]->getType());
        $this->assertEquals('default', $result[0]->getSlug());
        $this->assertEquals($this->getLines(['Bar', 'Baz']), $result[0]->getBody());

        $this->assertInstanceOf(Block::class, $result[1]);
        $this->assertEquals('markdown', $result[1]->getType());
        $this->assertEquals('foo', $result[1]->getSlug());
        $this->assertEquals('Foo', $result[1]->getBody());

        $block = $instance->getWithSlug('default');
        $this->assertEquals('markdown', $block->getType());
        $this->assertEquals('default', $block->getSlug());
        $this->assertEquals($this->getLines(['Bar', 'Baz']), $block->getBody());
	}

    /** @test */
	public function it_returns_blocks_with_same_slug_as_one()
	{
        $instance = TextBlocksParser::fromString($this->getLines([
            "@markdown('foo')",
            'Foo',
            "@endmarkdown",
            "@markdown('foo')",
            'Bar',
            "@endmarkdown",
        ]));

        $result = $instance->get();

	    $this->assertIsArray($result);
        $this->assertCount(1, $result);

        $this->assertInstanceOf(Block::class, $result[0]);
        $this->assertEquals('markdown', $result[0]->getType());
        $this->assertEquals('foo', $result[0]->getSlug());
        $this->assertEquals($this->getLines(['Foo', 'Bar']), $result[0]->getBody());

        $block = $instance->getWithSlug('foo');
        $this->assertInstanceOf(Block::class, $block);
        $this->assertEquals('markdown', $block->getType());
        $this->assertEquals('foo', $block->getSlug());
        $this->assertEquals($this->getLines(['Foo', 'Bar']), $block->getBody());
	}

    private function getLines(array $lines)
    {
        return implode("\n", $lines);
    }
}