<?php

namespace Tests\Unit\Testing;

use Tests\TestCase;
use org\bovigo\vfs\vfsStream;
use App\Testing\ProjectBuilder;
use App\Testing\InteractsWithFileSystem;

class ProjectBuilderTest extends TestCase
{
    use InteractsWithFileSystem;
    
	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(
            new ProjectBuilder($slug = 'foo')
        );
	}

    /** @test */
	public function it_can_be_instantiated_from_slug()
	{
	    $this->assertInstanceOf(
            ProjectBuilder::class,
            ProjectBuilder::fromSlug('foo')
        );
	}

    /** @test */
	public function it_provides_a_method_to_create_a_project_and_returns_self()
	{
        $instance = ProjectBuilder::fromSlug('foo');
        
        $this->assertSame(
            $instance, 
            $instance->create()
        );
    }

    /** @test */
	public function it_creates_images_folder_for_slug()
	{
        $instance = ProjectBuilder::fromSlug('foo');
        $imagesFolderPath = vfsStream::url('folio/contents/images/foo');

        $this->assertFileDoesNotExist($imagesFolderPath);

        $instance->create();

        $this->assertFileExists($imagesFolderPath);
    }

    /** @test */
	public function it_creates_used_image_added_as_a_string()
	{
        $instance = ProjectBuilder::fromSlug('foo')
            ->withUsedImages('foo')
            ->create();

        $this->assertEquals(
            ['foo.jpg'], 
            $this->getAllSourceImages('foo')
        );
	}

    /** @test */
	public function it_creates_used_images_added_as_an_array()
	{
        $instance = ProjectBuilder::fromSlug('foo')
            ->withUsedImages(['foo', 'bar'])
            ->create();

        $this->assertEquals(
            ['bar.jpg', 'foo.jpg'], 
            $this->getAllSourceImages('foo')
        );
	}

    /** @test */
	public function it_creates_not_used_image_added_as_a_string()
	{
        $instance = ProjectBuilder::fromSlug('foo')
            ->withNotUsedImages('foo')
            ->create();

        $this->assertEquals(
            ['foo.jpg'], 
            $this->getAllSourceImages('foo')
        );
	}

    /** @test */
	public function it_creates_not_used_image_added_as_an_array()
	{
        $instance = ProjectBuilder::fromSlug('foo')
            ->withNotUsedImages(['foo', 'bar'])
            ->create();

        $this->assertEquals(
            ['bar.jpg', 'foo.jpg'], 
            $this->getAllSourceImages('foo')
        );
	}

    /** @test */
	public function it_creates_squared_images_by_default()
	{
        $instance = ProjectBuilder::fromSlug('foo')
            ->withUsedImages('foo')
            ->withNotUsedImages('bar')
            ->create();

        $this->assertSquare(
            vfsStream::url('folio/contents/images/foo/foo.jpg')
        );

        $this->assertSquare(
            vfsStream::url('folio/contents/images/foo/bar.jpg')
        );
    }

    /** @test */
	public function it_allows_to_create_landscape_used_image()
	{
        $instance = ProjectBuilder::fromSlug('foo')
            ->withLandscapeUsedImage('foo')
            ->create();

        $this->assertLandscape(
            vfsStream::url('folio/contents/images/foo/foo.jpg')
        );
    }

    /** @test */
	public function it_creates_project_file()
	{
        $instance = ProjectBuilder::fromSlug('foo');
        $projectMarkdownFilePath = vfsStream::url('folio/contents/projects/foo.md');

        $this->assertFileDoesNotExist($projectMarkdownFilePath);

        $instance->create();

        $this->assertFileExists($projectMarkdownFilePath);
    }

    /** @test */
	public function it_creates_published_project_file_with_used_images_only()
	{
        $instance = ProjectBuilder::fromSlug('foo')
            ->withUsedImages(['foo', 'baz'])
            ->withNotUsedImages('bar')
            ->create();
        
        $projectMarkdownFilePath = vfsStream::url('folio/contents/projects/foo.md');

        $markdown = file_get_contents($projectMarkdownFilePath);
        
        $this->assertStringContainsString('published_at: yesterday', $markdown);
        $this->assertStringContainsString('![foo]', $markdown);
        $this->assertStringNotContainsString('![bar]', $markdown);
        $this->assertStringContainsString('![baz]', $markdown);
    }

    /** @test */
	public function it_deletes_source_image()
	{
        $instance = ProjectBuilder::fromSlug('foo')
            ->withUsedImages('foo')
            ->create();
        
        $imageSourcePath = vfsStream::url('folio/contents/images/foo/foo.jpg');

        $this->assertFileExists($imageSourcePath);

        $instance->deleteSourceImage('foo');

        $this->assertFileDoesNotExist($imageSourcePath);
    }

    /** @test */
	public function it_deletes_source_folder()
	{
        $instance = ProjectBuilder::fromSlug('foo')
            ->create();
        
        $imagesFolderPath = vfsStream::url('folio/contents/images/foo');

        $this->assertFileExists($imagesFolderPath);

        $instance->deleteSourceFolder();

        $this->assertFileDoesNotExist($imagesFolderPath);
    }

    /** @test */
	public function it_replaces_used_image()
	{
        $instance = ProjectBuilder::fromSlug('foo')
            ->withUsedImages('foo')
            ->create();
        
        $projectMarkdownFilePath = vfsStream::url('folio/contents/projects/foo.md');
        $fooPath = vfsStream::url('folio/contents/images/foo/foo.jpg');
        $barPath = vfsStream::url('folio/contents/images/foo/bar.jpg');

        $this->assertFileExists($fooPath);
        $this->assertFileDoesNotExist($barPath);

        $instance->replaceUsedImage('foo', 'bar');

        $this->assertFileDoesNotExist($fooPath);
        $this->assertFileExists($barPath);
        
        $markdown = file_get_contents($projectMarkdownFilePath);
        $this->assertStringContainsString('published_at: yesterday', $markdown);
        $this->assertStringNotContainsString('![foo]', $markdown);
        $this->assertStringContainsString('![bar]', $markdown);
    }

    /** @test */
	public function it_returns_expected_files()
	{
        $instance = ProjectBuilder::fromSlug('foo');
        
        $this->assertEquals([], $instance->getExpectedFiles());

        $instance->withNotUsedImages('bar');

        $this->assertEquals([], $instance->getExpectedFiles());

        $instance->withUsedImages('foo');
        
        $this->assertEquals([
            vfsStream::url('folio/public/images/foo/foo-compressed.jpg'),
            vfsStream::url('folio/public/images/foo/foo-compressed.webp'),
            vfsStream::url('folio/public/images/foo/foo-portrait-compressed.jpg'),
            vfsStream::url('folio/public/images/foo/foo-portrait-compressed.webp'),
        ], $instance->getExpectedFiles());
    }

    /** @test */
	public function it_returns_not_expected_files()
	{
        $instance = ProjectBuilder::fromSlug('foo');
        
        $this->assertEquals([], $instance->getNotExpectedFiles());

        $instance->withUsedImages('foo');

        $this->assertEquals([], $instance->getNotExpectedFiles());
        
        $instance->withNotUsedImages('bar');
        
        $this->assertEquals([
            vfsStream::url('folio/public/images/foo/bar-compressed.jpg'),
            vfsStream::url('folio/public/images/foo/bar-compressed.webp'),
            vfsStream::url('folio/public/images/foo/bar-portrait-compressed.jpg'),
            vfsStream::url('folio/public/images/foo/bar-portrait-compressed.webp'),
        ], $instance->getNotExpectedFiles());
    }

    private function getAllSourceImages($slug)
    {
        return array_values(
            array_diff(
                scandir(vfsStream::url("folio/contents/images/{$slug}")),
                ['.', '..']
            )
        );
    }

    private function assertSquare($path)
    {
        list ($width, $height) = getimagesize($path);

        $this->assertEquals(100, $width);
        $this->assertEquals(100, $height);
    }

    private function assertLandscape($path)
    {
        list ($width, $height) = getimagesize($path);

        $this->assertEquals(200, $width);
        $this->assertEquals(100, $height);
    }
}