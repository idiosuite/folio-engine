<?php

namespace Tests\Unit;

use App\Domain\Post;
use App\Portfolio;
use App\PortfolioGenerator;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class PortfolioGeneratorTest extends TestCase
{
	private $root;
	private $basePath;

	/** @before */
	public function setUpVfsStream()
	{
		$structure = [
			'contents' => [
				'pages' => [
					'bar.md' => '---
layout: page
title: Bar
---
Barbaz'
				],
				'projects' => [
					'foo.md' => '---
layout: project
title: Foo
---
Foobar'
				],
				'preferences' => [
					'defaults.json' => '[]'
				],
				'settings.json' => json_encode(['theme' => 'test']),
			],
			'theme' => [
				'resources' => [
					'views' => [
						'home.blade.php' => 'Homepage',
						'project.blade.php' => 'Project',
						'page.blade.php' => 'Page',
						'404.blade.php' => 'Not Found',
					]
				]
			],
			'public' => []
		];

		$this->root = vfsStream::setup('folio', null, $structure);
		$this->basePath = vfsStream::url('folio');
	}
	
	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new PortfolioGenerator(new Portfolio($this->basePath)));
	}

	/** @test */
	public function it_provides_a_method_to_generate_html_website()
	{
	    $portfolioGenerator = new PortfolioGenerator(
	    	new Portfolio($this->basePath)
	    );

	    $this->assertCount(0, $this->getPublicHtmlFiles());

	    $portfolioGenerator->html(vfsStream::url('folio/public'));

	    $files = $this->getPublicHtmlFiles();
	    $this->assertCount(3, $files);
	    $this->assertEquals([
	    	'index.html', 
	    	'page', 
	    	'project'
	    ], $files);
	}

	/** @test */
	public function it_provides_a_method_to_generate_json_website()
	{
	    $portfolioGenerator = new PortfolioGenerator(
	    	new Portfolio($this->basePath)
	    );

	    $this->assertCount(0, $this->getPublicHtmlFiles());

		$portfolioGenerator->json(vfsStream::url('folio/public'));

		$files = $this->getPublicJsonFiles();
		
	    $this->assertCount(5, $files);
	    $this->assertEquals([
	    	'clients.json', 
			'links.json', 
	    	'pages.json', 
	    	'projects.json',
	    	'settings.json'
	    ], $files);
	}
}