<?php

namespace Tests\Unit\Interpolators;

use App\Domain\Post;
use App\Interpolators\Interpolator;
use App\Interpolators\Slideshow;
use Tests\TestCase;

class SlideshowTest extends TestCase
{
	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new Slideshow);
	}

	/** @test */
	public function it_implements_interpolator_interface()
	{
	    $this->assertInstanceOf(Interpolator::class, new Slideshow);
	}

	/** @test */
	public function it_replaces_slideshow_tokens()
	{
	    $slideshow = new Slideshow;

	    $content = $slideshow->text('foo
	    	{slideshow}
	    	bar
	    	{/slideshow}
	    	baz');

	    $this->assertStringNotContainsString('{slideshow}', $content);
	    $this->assertStringNotContainsString('{/slideshow}', $content);

	    $this->assertStringContainsString('<span class="Slideshow">', $content);
	    $this->assertStringContainsString('</span>', $content);
	}

	/** 
	 * @test 
	 * @dataProvider slideshowIdProvider
	 * */
	public function it_replaces_slideshow_tokens_with_id($id)
	{
	    $slideshow = new Slideshow;

	    $content = $slideshow->text("foo
	    	{slideshow:{$id}}
	    	bar
	    	{/slideshow}
	    	baz");

	    $this->assertStringNotContainsString("{slideshow:{$id}}", $content);
	    $this->assertStringNotContainsString('{/slideshow}', $content);

	    $this->assertStringContainsString('<span class="Slideshow">', $content);
	    $this->assertStringContainsString('</span>', $content);
	}

	public function slideshowIdProvider()
	{
		return [
			['cars'],
			['my-cars'],
			['my_cars'],
			['cars01'],
			['12345'],
			['MyCars'],
		];
	}

	/** @test */
	public function it_provides_data_options_attribute_if_attributes_are_set()
	{
		$preferences = ['autoplay' => false];

	    $slideshow = $this->getInstanceWithPreferences([
			'slideshow.foo' => $preferences
		]);

	    $content = $slideshow->text('{slideshow:foo}foo{/slideshow}');
	    
	    $this->assertStringContainsSlideshow('foo', $preferences, $content);
	}

	/** @test */
	public function it_does_not_provide_data_options_attribute_if_attributes_are_not_set()
	{
		$preferences = ['autoplay' => false];

	    $slideshow = $this->getInstanceWithPreferences([
			'slideshow.bar' => $preferences
		]);

	    $content = $slideshow->text('{slideshow:foo}foo{/slideshow}');
	    
	    $this->assertStringContainsString(
	    	'<span class="Slideshow">foo</span>', 
	    	$content
	    );
	}

	/** @test */
	public function it_provides_data_options_attribute_if_attributes_are_set_multiple()
	{
		$preferences = ['autoplay' => false];

	    $slideshow = $this->getInstanceWithPreferences([
			'slideshow.bar' => $preferences
		]);

	    $content = $slideshow->text('{slideshow}foo{/slideshow}
	    {slideshow:bar}bar{/slideshow}');
	    
	    $this->assertStringContainsString(
	    	'<span class="Slideshow">foo</span>', 
	    	$content
	    );

	    $this->assertStringContainsSlideshow('bar', $preferences, $content);
	}

	/** @test */
	public function it_allows_to_specify_global_preferences_for_slideshow_with_id()
	{
	    $globalPreferences = ['autoplay' => false];

	    $slideshow = $this->getInstanceWithPreferences([
			'slideshow.*' => $globalPreferences
		]);

	    $content = $slideshow->text('{slideshow:bar}bar{/slideshow}');
	    
	    $this->assertStringContainsSlideshow('bar', $globalPreferences, $content);
	}

	/** @test */
	public function it_allows_to_specify_global_preferences_for_slideshow_without_id()
	{
	    $globalPreferences = ['autoplay' => false];

	    $slideshow = $this->getInstanceWithPreferences([
			'slideshow.*' => $globalPreferences
		]);

	    $content = $slideshow->text('{slideshow}foo{/slideshow}');
	    
	    $this->assertStringContainsSlideshow('foo', $globalPreferences, $content);
	}

	/** @test */
	public function it_allows_to_specify_global_preferences_for_multiple_slideshows()
	{
	    $globalPreferences = ['autoplay' => false];

	    $slideshow = $this->getInstanceWithPreferences([
			'slideshow.*' => $globalPreferences
		]);

	    $content = $slideshow->text('{slideshow}foo{/slideshow}
	    {slideshow:bar}bar{/slideshow}');
	    
	    $this->assertStringContainsSlideshow('foo', $globalPreferences, $content);
	    $this->assertStringContainsSlideshow('bar', $globalPreferences, $content);
	}

	/** @test */
	public function global_preferences_are_overridden_by_qualified_preferences()
	{
	    $globalPreferences = ['autoplay' => false, 'foo' => 'bar'];
	    $qualifiedPreferences = ['autoplay' => true, 'bar' => 'baz'];

	    $slideshow = $this->getInstanceWithPreferences([
			'slideshow.*' => $globalPreferences,
			'slideshow.foo' => $qualifiedPreferences
		]);

	    $content = $slideshow->text('{slideshow:foo}foo{/slideshow}');
	    
	    $this->assertStringContainsSlideshow(
	    	'foo',
	    	['autoplay' => true, 'foo' => 'bar', 'bar' => 'baz'],
	    	$content
	    );
	}

	private function getInstanceWithPreferences(array $preferences)
	{
		return new Slideshow(
	    	new Post([
	    		'preferences' => $preferences
	    	])
	    );
	}

	private function assertStringContainsSlideshow($string, array $data, $content)
	{
		$this->assertStringContainsString(
	    	sprintf(
	    		'<span class="Slideshow" data-options="%s">%s</span>', 
	    		htmlentities(json_encode($data)),
	    		$string
	    	),
	    	$content
	    );
	}
}