<?php

namespace Tests\Unit\Interpolators;

use App\Domain\Post;
use App\Interpolators\Interpolator;
use App\Interpolators\Youtube;
use Tests\TestCase;

class YoutubeTest extends TestCase
{
	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new Youtube);
	}

	/** @test */
	public function it_implements_interpolator_interface()
	{
	    $this->assertInstanceOf(Interpolator::class, new Youtube);
	}

	/** @test */
	public function it_replaces_vimeo_video_tokens()
	{
	    $youtube = new Youtube(new Post);

	    $content = $youtube->text('foo
	    	{youtube:12345}
	    	bar
	    	{youtube:56789}
	    	baz');

	    $this->assertStringNotContainsString('{youtube:12345}', $content);
	    $this->assertStringNotContainsString('{youtube:56789}', $content);

	    $this->assertStringContainsString(
	    	'<div class="Template__video Template__video--isWidescreen"', 
	    	$content
	    );
	}

	/** 
	 * @test 
	 * @dataProvider provideVideoIds
	 */
	public function it_customizes_video_id($videoId)
	{
	    $youtube = new Youtube(new Post);

	    $this->assertStringContainsString(
	    	"https://www.youtube.com/embed/{$videoId}", 
	    	$youtube->text("{youtube:{$videoId}}")
	    );
	}

	public function provideVideoIds()
	{
		return [
			['a8eU6hTAHEE'],
			['7_pRiUfp938'],
			['hPr-Yc92qaY'],
		];
	}

	/** @test */
	public function it_customizes_class_according_to_format()
	{
	    $youtubePal = new Youtube(
	    	new Post(['preferences' => [
	    		'youtube.12345' => ['format' => '4:3']
	    	]])
	    );

	    $this->assertStringContainsString(
	    	'Template__video--isPal"', 
	    	$youtubePal->text('{youtube:12345}')
	    );

	    $youtubeWidescreen = new Youtube(
	    	new Post(['preferences' => [
	    		'youtube.12345' => ['format' => '16:9']
	    	]])
	    );

	    $this->assertStringContainsString(
	    	'Template__video--isWidescreen"', 
	    	$youtubeWidescreen->text('{youtube:12345}')
	    );
	}

	/** @test */
	public function it_customizes_style_according_for_pal_format()
	{
	    $youtube = new Youtube(
	    	new Post(['preferences' => [
	    		'youtube.12345' => ['format' => '4:3']
	    	]])
	    );

	    $this->assertStringContainsString(
	    	'style="height: 0; padding-top: 75%;"', 
	    	$youtube->text('{youtube:12345}')
	    );
	}

	/** @test */
	public function it_customizes_style_according_for_widescreen_format()
	{
	    $youtube = new Youtube(
	    	new Post(['preferences' => [
	    		'youtube.12345' => ['format' => '16:9']
	    	]])
	    );

	    $this->assertStringContainsString(
	    	'style="height: 0; padding-top: 56.3%;"', 
	    	$youtube->text('{youtube:12345}')
	    );
	}

	/** @test */
	public function it_customizes_loop_option()
	{
	    $youtube = new Youtube(
	    	new Post(['preferences' => [
	    		'youtube.12345' => ['loop' => true]
	    	]])
	    );

	    $this->assertStringContainsString(
	    	'loop=1', 
	    	$youtube->text('{youtube:12345}')
	    );

		$this->assertStringContainsString(
	    	'autoplay=1', 
	    	$youtube->text('{youtube:12345}')
	    );
	}

	/** @test */
	public function it_customizes_width_and_height()
	{
	    $youtube = new Youtube(
	    	new Post(['preferences' => [
	    		'youtube.12345' => ['format' => '', 'size' => [640, 320]]
	    	]])
	    );

	    $this->assertStringContainsString(
	    	'width="640', 
	    	$youtube->text('{youtube:12345}')
	    );

	    $this->assertStringContainsString(
	    	'height="320', 
	    	$youtube->text('{youtube:12345}')
	    );
	}
}