<?php

namespace Tests\Unit\Interpolators;

use App\Domain\Post;
use App\Interpolators\Interpolator;
use App\Interpolators\Vimeo;
use App\Testing\VideoTest;
use Tests\TestCase;

class VimeoTest extends TestCase
{
	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new Vimeo);
	}

	/** @test */
	public function it_implements_interpolator_interface()
	{
	    $this->assertInstanceOf(Interpolator::class, new Vimeo);
	}

	/** @test */
	public function it_replaces_vimeo_video_tokens()
	{
	    $vimeo = new Vimeo(new Post);

	    $content = $vimeo->text('foo
	    	{vimeo:12345}
	    	bar
	    	{vimeo:56789}
	    	baz');

	    $this->assertStringNotContainsString('{vimeo:12345}', $content);
	    $this->assertStringNotContainsString('{vimeo:56789}', $content);

	    $this->assertStringContainsString(
	    	'<div class="Template__video Template__video--isWidescreen"', 
	    	$content
	    );
	}

	/** @test */
	public function it_customizes_video_id()
	{
	    $vimeo = new Vimeo(new Post);

	    $this->assertStringContainsString(
	    	'https://player.vimeo.com/video/12345', 
	    	$vimeo->text('{vimeo:12345}')
	    );
	}

	/** @test */
	public function it_customizes_class_according_to_format()
	{
	    $vimeoPal = new Vimeo(
	    	new Post(['preferences' => [
	    		'vimeo.12345' => ['format' => '4:3']
	    	]])
	    );

	    $this->assertStringContainsString(
	    	'Template__video--isPal"', 
	    	$vimeoPal->text('{vimeo:12345}')
	    );

	    $vimeoWidescreen = new Vimeo(
	    	new Post(['preferences' => [
	    		'vimeo.12345' => ['format' => '16:9']
	    	]])
	    );

	    $this->assertStringContainsString(
	    	'Template__video--isWidescreen"', 
	    	$vimeoWidescreen->text('{vimeo:12345}')
	    );
	}

	/** @test */
	public function it_customizes_style_for_pal_format()
	{
	    $vimeo = new Vimeo(
	    	new Post(['preferences' => [
	    		'vimeo.12345' => ['format' => '4:3']
	    	]])
	    );

	    $this->assertStringContainsString(
	    	'style="height: 0; padding-top: 75%;"', 
	    	$vimeo->text('{vimeo:12345}')
	    );
	}

	/** @test */
	public function it_customizes_style_for_widescreen_format()
	{
	    $vimeo = new Vimeo(
	    	new Post(['preferences' => [
	    		'vimeo.12345' => ['format' => '16:9']
	    	]])
	    );

	    $this->assertStringContainsString(
	    	'style="height: 0; padding-top: 56.3%;"', 
	    	$vimeo->text('{vimeo:12345}')
	    );
	}

	/** @test */
	public function it_customizes_loop_option()
	{
	    $vimeo = new Vimeo(
	    	new Post(['preferences' => [
	    		'vimeo.12345' => ['loop' => true]
	    	]])
	    );

	    $this->assertStringContainsString(
	    	'background=1', 
	    	$vimeo->text('{vimeo:12345}')
	    );
	}

	/** @test */
	public function it_customizes_width_and_height()
	{
	    $vimeo = new Vimeo(
	    	new Post(['preferences' => [
	    		'vimeo.12345' => ['format' => '', 'size' => [640, 320]]
	    	]])
	    );

	    $this->assertStringContainsString(
	    	'width="640', 
	    	$vimeo->text('{vimeo:12345}')
	    );

	    $this->assertStringContainsString(
	    	'height="320', 
	    	$vimeo->text('{vimeo:12345}')
	    );
	}

	/** @test */
	public function it_resolves_preferences_using_video_slug_instead_of_id()
	{
	    $vimeo = new Vimeo(
	    	new Post([
				'preferences' => [
	    			'vimeo.foo-video' => ['format' => '', 'size' => [640, 320]]
	    		],
				'videos' => [
					VideoTest::vimeoVideo()
				]
			])
	    );

	    $this->assertStringContainsString(
	    	'width="640', 
	    	$vimeo->text('{vimeo:12345}')
	    );

	    $this->assertStringContainsString(
	    	'height="320', 
	    	$vimeo->text('{vimeo:12345}')
	    );
	}
}