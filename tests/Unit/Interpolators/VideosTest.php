<?php

namespace Tests\Unit\Interpolators;

use App\Domain\Post;
use App\Interpolators\Videos;
use App\Interpolators\Interpolator;
use App\Testing\VideoTest;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class VideosTest extends TestCase
{
	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new Videos);
	}

	/** @test */
	public function it_implements_interpolator_interface()
	{
	    $this->assertInstanceOf(Interpolator::class, new Videos);
	}

    /** @test */
	public function it_handles_no_videos()
	{
	    $content = new Videos(
	    	new Post(['slug' => 'yolo'])
	    );

	    $content = $content->text('foobar');

	    $this->assertEquals('foobar', $content);
	}

    /** @test */
	public function it_finds_one_video()
	{
	    $content = new Videos(
	    	new Post([
                'videos' => [
                    VideoTest::vimeoVideo('12345', ['slug' => 'foo'])
                ]
            ])
	    );

	    $content = $content->text("@video('foo')");

	    $this->assertStringContainsString(
	    	'{vimeo:12345}', 
	    	$content
	    );
	}

    /** @test */
	public function it_finds_one_video_dash()
	{
	    $content = new Videos(
	    	new Post([
                'videos' => [
                    VideoTest::vimeoVideo('12345', ['slug' => 'foo-video'])
                ]
            ])
	    );

	    $content = $content->text("@video('foo-video')");

	    $this->assertStringContainsString(
	    	'{vimeo:12345}', 
	    	$content
	    );
	}

    /** @test */
	public function it_finds_one_video_dot()
	{
	    $content = new Videos(
	    	new Post([
                'videos' => [
                    VideoTest::vimeoVideo('12345', ['slug' => 'foo.foo'])
                ]
            ])
	    );

	    $content = $content->text("@video('foo.foo')");

	    $this->assertStringContainsString(
	    	'{vimeo:12345}', 
	    	$content
	    );
	}

    /** @test */
	public function it_finds_one_video_numbers()
	{
	    $content = new Videos(
	    	new Post([
                'videos' => [
                    VideoTest::vimeoVideo('12345', ['slug' => 'foo-2017'])
                ]
            ])
	    );

	    $content = $content->text("@video('foo-2017')");

	    $this->assertStringContainsString(
	    	'{vimeo:12345}', 
	    	$content
	    );
	}

    /** @test */
	public function it_finds_two_videos()
	{
	    $content = new Videos(
	    	new Post([
                'videos' => [
                    VideoTest::vimeoVideo('12345', ['slug' => 'foo-video']),
                    VideoTest::youtubeVideo('abc123', ['slug' => 'bar-video'])
                ]
            ])
	    );

	    $content = $content->text("@video('foo-video') @video('bar-video')");

	    $this->assertStringContainsString(
	    	'{vimeo:12345}', 
	    	$content
	    );

	    $this->assertStringContainsString(
	    	'{youtube:abc123}', 
	    	$content
	    );
	}

    /** @test */
	public function it_handles_video_that_cannot_be_resolved()
	{
	    $content = new Videos(
	    	new Post(['slug' => 'yolo'])
	    );

	    $content = $content->text("@video('foo-video')");

	    $this->assertEquals("@video('foo-video')", $content);
	}

    /** @test */
	public function it_handles_video_that_cannot_be_resolved_and_existing_video()
	{
	    $content = new Videos(
	    	new Post([
                'videos' => [
                    VideoTest::vimeoVideo('12345', ['slug' => 'foo-video']),
                ]
            ])
	    );

	    $content = $content->text("@video('foo-video') @video('bar-video')");

	    $this->assertStringContainsString(
	    	'{vimeo:12345}', 
	    	$content
	    );
	    $this->assertStringContainsString(
	    	"@video('bar-video')", 
	    	$content
	    );
	}

    /** @test */
	public function it_finds_vimeo_video()
	{
	    $content = new Videos(
	    	new Post([
                'videos' => [
                    VideoTest::vimeoVideo('12345', ['slug' => 'foo-video'])
                ]
            ])
	    );

	    $content = $content->text("@video('foo-video')");

	    $this->assertStringContainsString(
	    	'{vimeo:12345}', 
	    	$content
	    );
	}

    /** @test */
	public function it_finds_youtube_video()
	{
        $content = new Videos(
	    	new Post([
                'videos' => [
                    VideoTest::youtubeVideo('abc123', ['slug' => 'foo-video'])
                ]
            ])
	    );

	    $content = $content->text("@video('foo-video')");

	    $this->assertStringContainsString(
	    	'{youtube:abc123}', 
	    	$content
	    );
	}

    /** @test */
	public function it_will_not_handle_unsupported_provider()
	{
        $content = new Videos(
	    	new Post([
                'videos' => [
                    VideoTest::video([
                        'provider' => 'foobar', 
                        'slug' => 'foo-video',
                        'id' => '12345'
                    ])
                ]
            ])
	    );

	    $content = $content->text("@video('foo-video')");

	    $this->assertStringNotContainsString(
	    	'{foobar:12345}', 
	    	$content
	    );
	}
}