<?php

namespace Tests\Unit\Interpolators;

use App\Testing\InteractsWithFileSystem;
use App\Testing\InteractsWithImage;
use App\Interpolators\ResponsiveImages;
use App\Themes\Publishers\Images\Testing\InteractsWithManifest;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class ResponsiveImagesTest extends TestCase
{
	use InteractsWithFileSystem;
	use InteractsWithImage;
	use InteractsWithManifest;

	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new ResponsiveImages);
	}

	/** @test */
	public function it_does_not_affect_normal_text()
	{
	    $instance = new ResponsiveImages;

	    $this->assertEquals(
	    	'foo bar baz', 
	    	$instance->text('foo bar baz')
	    );
	}

	/** @test */
	public function it_does_not_interpolate_images_tags_if_manifest_is_not_published()
	{
		$this->createDummyImage($original = vfsStream::url('folio/contents/images/foo.png'));

		$instance = new ResponsiveImages;

		$this->assertEquals(
	    	'<img src="vfs://folio/contents/images/foo.png" alt="foo" />', 
	    	$instance->text('<img src="' . $original . '" alt="foo" />')
	    );
	}

	/** @test */
	public function it_does_interpolates_images_tags_if_manifest_is_published()
	{
		$this->unlinkDefaultImage();
		$this->createDummyImage(vfsStream::url('folio/contents/images/foo.png'));

		$this->getPublishedManifestInstance();

	    $instance = new ResponsiveImages;

	    $this->assertEquals(
	    	'<picture><source srcset="' . $this->baseUrlImages . '/foo-compressed.webp" width="50" height="50" type="image/webp"></source><img src="' . $this->baseUrlImages . '/foo-compressed.jpg" width="50" height="50" /></picture>', 
	    	$instance->text('<img src="' . $this->baseUrlImages . '/foo.png" alt="foo" />')
	    );
	}
}