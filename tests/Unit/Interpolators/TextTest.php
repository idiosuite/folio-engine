<?php

namespace Tests\Unit\Interpolators;

use App\Domain\Post;
use App\Interpolators\Interpolator;
use App\Interpolators\RequiresPost;
use App\Interpolators\Text;
use Tests\TestCase;

class TextTest extends TestCase
{
	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new Text);
	}

	/** @test */
	public function it_implements_interpolator_interface()
	{
	    $this->assertInstanceOf(Interpolator::class, new Text);
	}

	/** @test */
	public function it_implements_requires_post_interface()
	{
	    $this->assertInstanceOf(RequiresPost::class, new Text);
	}

    /** @test */
	public function it_replaces_single_text_token()
	{
	    $instance = new Text(new Post([
            'texts' => [
                'bar' => 'BAR'
            ]
        ]));

	    $content = $instance->text("foo
	    	@text('bar')
	    	baz");

        $this->assertStringNotContainsString('@text', $content);
        $this->assertStringNotContainsString("@text('bar')", $content);
	    
	    $this->assertStringContainsString('BAR', $content);
	}

    /** @test */
	public function it_replaces_single_text_token_with_numbers_and_dashes()
	{
	    $instance = new Text(new Post([
            'texts' => [
                'bar-01' => 'BAR'
            ]
        ]));

	    $content = $instance->text("foo
	    	@text('bar-01')
	    	baz");

        $this->assertStringNotContainsString('@text', $content);
        $this->assertStringNotContainsString("@text('bar')", $content);
	    
	    $this->assertStringContainsString('BAR', $content);
	}

    /** @test */
	public function it_replaces_multiple_text_token()
	{
	    $instance = new Text(new Post([
            'texts' => [
                'bar' => 'Lorem ipsum dolor sit amet',
                'credits' => 'My credits here'
            ]
        ]));

	    $content = $instance->text("foo
	    	@text('bar')
	    	baz
            @text('credits')");

        $this->assertStringNotContainsString('@text', $content);
        $this->assertStringNotContainsString("@text('bar')", $content);
        $this->assertStringNotContainsString("@text('credits')", $content);
	    
	    $this->assertStringContainsString('Lorem ipsum dolor sit amet', $content);
        $this->assertStringContainsString('My credits here', $content);
	}

	/** @test */
	public function it_replaces_credits_token()
	{
	    $instance = new Text(new Post([
            'texts' => [
                'credits-foobar' => 'Lorem ipsum dolor sit amet',
            ]
        ]));

	    $content = $instance->text("foo
	    	@credits('foobar')
	    	baz");

        $this->assertStringNotContainsString("@credits('foobar')", $content);
	    
	    $this->assertStringContainsString('Lorem ipsum dolor sit amet', $content);
	}

	/** @test */
	public function it_replaces_title_token()
	{
	    $instance = new Text(new Post([
            'texts' => [
                'title-foobar' => 'Lorem ipsum dolor sit amet',
            ]
        ]));

	    $content = $instance->text("foo
	    	@title('foobar')
	    	baz");

        $this->assertStringNotContainsString("@title('foobar')", $content);
	    
	    $this->assertStringContainsString('Lorem ipsum dolor sit amet', $content);
	}
}