<?php

namespace Tests\Unit\Console\Commands;

use App\Console\Commands\GenerateDevelopmentPortfolio;
use App\Generators\FolioManifest;
use App\Themes\Publishers\Manifests\ManifestFactory;
use App\Testing\InteractsWithFileSystem;
use App\Testing\InteractsWithImage;
use App\Testing\InteractsWithMarkdownFiles;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use App\Themes\Publishers\SymlinkPublisher;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class GenerateDevelopmentPortfolioTest extends TestCase
{
	use InteractsWithFileSystem;
    use InteractsWithImage;
    use InteractsWithMarkdownFiles;

    private $testSymlinkPublisher;

    /** @before */
    public function setUpSymlinkPublisher()
    {
        $this->testSymlinkPublisher = SymlinkPublisher::test();
    }

    /** @after */
    public function tearDownSymlinkPublisher()
    {
        return SymlinkPublisher::test()->reset();
    }
	
	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new GenerateDevelopmentPortfolio);
	}

    /** @test */
	public function it_is_instance_of_command()
	{
	    $this->assertInstanceOf(Command::class, new GenerateDevelopmentPortfolio);
	}

	/** @test */
	public function it_has_correct_name()
	{
	    $this->assertEquals('portfolio:generate-development', GenerateDevelopmentPortfolio::getDefaultName());
	}

    /** @test */
	public function it_publishes_index_php_file()
	{
	    $instance = new GenerateDevelopmentPortfolio;

        $this->createConsoleApplication($instance);

        $this->assertFileDoesNotExist(vfsStream::url('folio/public/index.php'));

        $result = $instance->run(new ArrayInput([]), new NullOutput);

        $this->assertFileExists(vfsStream::url('folio/public/index.php'));
        $this->assertNotEquals('', file_get_contents(vfsStream::url('folio/public/index.php')));
	}

    /** @test */
	public function it_publishes_symlink_to_images_folder()
	{
        $instance = new GenerateDevelopmentPortfolio;

        $this->createConsoleApplication($instance);

        $this->assertFalse($this->testSymlinkPublisher->hasPublished(
            vfsStream::url('folio/contents/images'),
            vfsStream::url('folio/public/images')
        ));

        $instance->run(new ArrayInput([]), new NullOutput);

        $this->assertTrue($this->testSymlinkPublisher->hasPublished(
            vfsStream::url('folio/contents/images'),
            vfsStream::url('folio/public/images')
        ));
	}

    /** @test */
	public function it_publishes_symlink_to_theme_assets_folder()
	{
        $instance = new GenerateDevelopmentPortfolio;

        $this->createConsoleApplication($instance);

        $this->assertFalse($this->testSymlinkPublisher->hasPublished(
            vfsStream::url('folio/theme/assets'),
            vfsStream::url('folio/public/_assets/test')
        ));

        $instance->run(new ArrayInput([]), new NullOutput);
        
        $this->assertTrue($this->testSymlinkPublisher->hasPublished(
            vfsStream::url('folio/theme/assets'),
            vfsStream::url('folio/public/_assets/test')
        ));
	}

    /** @test */
	public function it_deletes_previous_static_version()
	{
        $manifest = FolioManifest::make()
            ->addFile($index = vfsStream::url('folio/public/index.html'))
            ->addFolder($projectsFolder = vfsStream::url('folio/public/project'))
            ->addFile(vfsStream::url('folio/public/project/foo.html'))
            ->addFolder($pagesFolder = vfsStream::url('folio/public/page'))
            ->addFile(vfsStream::url('folio/public/page/bar.html'))
            ->addFolder($imagesFolder = vfsStream::url('folio/public/images'))
            ->addFolder($projectImageFolder = vfsStream::url('folio/public/images/foo'))
            ->addFile(vfsStream::url('folio/public/images/foo/foo.jpg'))
            ->addFolder($assetsFolder = vfsStream::url('folio/public/_assets'))
            ->addFolder($themeFolder = vfsStream::url('folio/public/_assets/test'))
            ->write()
            ->createFilesForTest();
        
        $this->assertFileExists($index);
        $this->assertFileExists($projectsFolder);
        $this->assertFileExists($pagesFolder);
        $this->assertFileExists($imagesFolder);
        $this->assertFileExists($projectImageFolder);
        $this->assertFileExists($themeFolder);
        $this->assertFileExists($assetsFolder);
        
        $instance = new GenerateDevelopmentPortfolio;
        $this->createConsoleApplication($instance);
        $instance->run(new ArrayInput([]), new NullOutput);
        
        $this->assertFileDoesNotExist($index);
        $this->assertFileDoesNotExist($projectsFolder);
        $this->assertFileDoesNotExist($pagesFolder);
        $this->assertFileDoesNotExist($imagesFolder);
        $this->assertFileDoesNotExist($projectImageFolder);
        $this->assertFileDoesNotExist($themeFolder);
        $this->assertFileExists($assetsFolder);
	}

    /** @test */
	public function it_does_not_delete_files_not_generated_by_portfolio()
	{
        $manifest = FolioManifest::make()
            ->addFile($index = vfsStream::url('folio/public/index.html'))
            ->addFolder($projectsFolder = vfsStream::url('folio/public/project'))
            ->addFile(vfsStream::url('folio/public/project/foo.html'))
            ->addFolder($pagesFolder = vfsStream::url('folio/public/page'))
            ->addFile(vfsStream::url('folio/public/page/bar.html'))
            ->addFolder($imagesFolder = vfsStream::url('folio/public/images'))
            ->addFolder(vfsStream::url('folio/public/images/foo'))
            ->addFile(vfsStream::url('folio/public/images/foo/foo.jpg'))
            ->addFolder($themeAssetsFolder = vfsStream::url('folio/public/_assets'))
            ->addFolder($themeAssetsFolder = vfsStream::url('folio/public/_assets/test'))
            ->write()
            ->createFilesForTest();
        
        file_put_contents($foreignFile = vfsStream::url('folio/public/important.html'), 'important data');
        file_put_contents($foreignProjectFile = vfsStream::url('folio/public/project(important.html'), 'important data');
        $this->createDummyImage($foreignImage = vfsStream::url('folio/public/images/important.jpg'));
        
        $instance = new GenerateDevelopmentPortfolio;
        $this->createConsoleApplication($instance);

        $this->assertFileExists($foreignFile);
        $this->assertFileExists($foreignProjectFile);
        $this->assertFileExists($foreignImage);

        $instance->run(new ArrayInput([]), new NullOutput);
        
        $this->assertFileExists($foreignFile);
        $this->assertFileExists($foreignProjectFile);
        $this->assertFileExists($foreignImage);
	}

    /** @test */
	public function it_generates_a_manifest()
	{
        $manifest = FolioManifest::fromJson();
        $this->assertCount(0, $manifest);

        $this->createImageFolderForSlug('my-project');
        $this->createDummyImage(vfsStream::url('folio/contents/images/my-project/my-image.jpg'));
        $this->createPublishedProjectFile(['slug' => 'my-project', 'markdown' => '![my-image]']);
        $this->createPublishedPageFile(['slug' => 'my-page']);

        $instance = new GenerateDevelopmentPortfolio;
        $this->createConsoleApplication($instance);
        $instance->run(new ArrayInput([]), new NullOutput);

        $manifest = FolioManifest::fromJson();
        $this->assertCount(4, $manifest);
        $this->assertTrue($manifest->hasFolder(vfsStream::url('folio/public/_assets')));
        $this->assertTrue($manifest->hasSymlink(vfsStream::url('folio/public/_assets/test')));
        $this->assertTrue($manifest->hasSymlink(vfsStream::url('folio/public/images')));
        $this->assertTrue($manifest->hasFile(vfsStream::url('folio/public/index.php')));
	}

    /** @test */
	public function it_should_not_delete_root_folder()
	{
        // Given that I give a wrong destination path
        mkdir($importantFolder = vfsStream::url('folio/important'));
        file_put_contents($importantFile = vfsStream::url('folio/important/important.txt'), 'important data');

        $this->assertFileExists($importantFolder);
        $this->assertFileExists($importantFile);

        // When I generate a development portfolio
        $instance = new GenerateDevelopmentPortfolio;
        $this->createConsoleApplication($instance);
        $instance->run(new ArrayInput(['--destination' => $importantFolder]), new NullOutput);

        // Nothing should be erased
        $this->assertFileExists($importantFolder);
        $this->assertFileExists($importantFile);
	}

    private function createConsoleApplication(GenerateDevelopmentPortfolio $instance)
	{
		$application = new Application;

		$application->add( new \App\Console\Commands\GeneratePortfolio );
        $application->add( new \App\Console\Commands\PublishImageAssets );
        $application->add( new \App\Console\Commands\PublishThemeAssets );
        $application->add( new \App\Console\Commands\PublishContentAssets );

		$instance->setApplication($application);

		return $application;
	}
}