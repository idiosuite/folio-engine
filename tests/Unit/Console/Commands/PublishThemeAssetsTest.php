<?php

namespace Tests\Unit\Console\Commands;

use App\Console\Commands\PublishThemeAssets;
use Symfony\Component\Console\Application;
use App\Themes\Publishers\Manifests\Manifest;
use App\Themes\Publishers\Manifests\ManifestFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class PublishThemeAssetsTest extends TestCase
{
	private $root;
	
	/** @before */
	public function setUpVfsStream()
	{
		$structure = [
			'contents' => [
				'projects' => [
					'foo.md' => ''
				],
				'pages' => [
					'about.md' => ''
				],
				'preferences' => [
					'defaults.json' => json_encode(['vimeo' => []])
				],
				'settings.json' => json_encode(['theme' => 'test'])
			],
			'public' => [
				'_assets' => []
			],
			'theme' => [],
		];

		$this->root = vfsStream::setup('folio', null, $structure);

		vfsStream::copyFromFileSystem(
			env('FILE_SYSTEM_BASE_PATH') . '/themes/Folio/', 
			$this->root->getChild('theme')
		);
	}
	
	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new PublishThemeAssets);
	}

	/** @test */
	public function it_is_instance_of_command()
	{
	    $this->assertInstanceOf(Command::class, new PublishThemeAssets);
	}

	/** @test */
	public function it_has_correct_name()
	{
	    $this->assertEquals('publish:theme', PublishThemeAssets::getDefaultName());
	}

	/** @test */
	public function it_can_be_executed()
	{
		$instance = new PublishThemeAssets;

	    $this->assertFileDoesNotExist(vfsStream::url('folio/public/_assets/test'));
	    
	    $instance->run(new ArrayInput(['--publisher' => 'copy']), new NullOutput);

	    $this->assertFileExists(vfsStream::url('folio/public/_assets/test'));
	}

	/** @test */
	public function it_generates_manifest_file()
	{
		$instance = new PublishThemeAssets;

		$manifest = ManifestFactory::assets();
		$this->assertFalse($manifest->exists());

		$instance->run(new ArrayInput(['--publisher' => 'copy']), new NullOutput);

		$sourceFolder = app(\App\Portfolio::class)->themeAssets()->getSourceFolder();
		
		$this->assertTrue($manifest->exists());
		$manifest->loadIfNotLoaded();
		$this->assertCount(3, $manifest);
		$this->assertTrue($manifest->has($sourceFolder));
		$this->assertTrue($manifest->has($sourceFolder . '/template.css'));
		$this->assertTrue($manifest->has($sourceFolder . '/template.js'));
	}
}