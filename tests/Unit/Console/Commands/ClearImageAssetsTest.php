<?php

namespace Tests\Unit\Console\Commands;

use Tests\TestCase;
use org\bovigo\vfs\vfsStream;
use App\Testing\ProjectBuilder;
use App\Testing\InteractsWithFileSystem;
use App\Console\Commands\ClearImageAssets;
use App\Console\Commands\PublishImageAssets;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;

class ClearImageAssetsTest extends TestCase
{
	use InteractsWithFileSystem;
	
	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new ClearImageAssets);
	}

    /** @test */
	public function it_is_instance_of_command()
	{
	    $this->assertInstanceOf(Command::class, new ClearImageAssets);
	}

	/** @test */
	public function it_has_correct_name()
	{
	    $this->assertEquals('images:clear', ClearImageAssets::getDefaultName());
	}

    /** @test */
	public function it_can_be_executed_with_root()
	{
        ProjectBuilder::fromSlug('foo')
            ->withUsedImages('foo')
            ->withNotUsedImages('bar')
            ->create();
        
        (new PublishImageAssets)->run(
            new ArrayInput([
                '--publisher' => 'optimized', 
                '--only-used' => false
            ]),
            new NullOutput
        );

        $this->assertEquals(
            [
                'bar-compressed.jpg',
                'bar-compressed.webp',
                'bar-portrait-compressed.jpg',
                'bar-portrait-compressed.webp',
                'bar-portrait.jpg',
                'foo-compressed.jpg',
                'foo-compressed.webp',
                'foo-portrait-compressed.jpg',
                'foo-portrait-compressed.webp',
                'foo-portrait.jpg',
            ],
            $this->getAllImagesInStorage('foo')
        );
        
        (new ClearImageAssets)->run(
            new ArrayInput([]), 
            new NullOutput
        );
        
        $this->assertEquals(
            [
                'foo-compressed.jpg',
                'foo-compressed.webp',
                'foo-portrait-compressed.jpg',
                'foo-portrait-compressed.webp',
                'foo-portrait.jpg',
            ],
            $this->getAllImagesInStorage('foo')
        );
	}

    /** @test */
	public function it_can_be_executed_with_relative_path()
	{
        ProjectBuilder::fromSlug('foo')
            ->withUsedImages('foo')
            ->withNotUsedImages('foobar')
            ->create();

        ProjectBuilder::fromSlug('bar')
            ->withUsedImages('bar')
            ->withNotUsedImages('barbaz')
            ->create();
        
        (new PublishImageAssets)->run(
            new ArrayInput([
                '--publisher' => 'optimized', 
                '--only-used' => false
            ]),
            new NullOutput
        );

        $this->assertEquals(
            [
                'foo-compressed.jpg',
                'foo-compressed.webp',
                'foo-portrait-compressed.jpg',
                'foo-portrait-compressed.webp',
                'foo-portrait.jpg',
                'foobar-compressed.jpg',
                'foobar-compressed.webp',
                'foobar-portrait-compressed.jpg',
                'foobar-portrait-compressed.webp',
                'foobar-portrait.jpg',
            ],
            $this->getAllImagesInStorage('foo')
        );

        $this->assertEquals(
            [
                'bar-compressed.jpg',
                'bar-compressed.webp',
                'bar-portrait-compressed.jpg',
                'bar-portrait-compressed.webp',
                'bar-portrait.jpg',
                'barbaz-compressed.jpg',
                'barbaz-compressed.webp',
                'barbaz-portrait-compressed.jpg',
                'barbaz-portrait-compressed.webp',
                'barbaz-portrait.jpg',
            ],
            $this->getAllImagesInStorage('bar')
        );
        
        (new ClearImageAssets)->run(
            new ArrayInput(['--path' => 'foo']), 
            new NullOutput
        );
        
        $this->assertEquals(
            [
                'foo-compressed.jpg',
                'foo-compressed.webp',
                'foo-portrait-compressed.jpg',
                'foo-portrait-compressed.webp',
                'foo-portrait.jpg',
            ],
            $this->getAllImagesInStorage('foo')
        );

        $this->assertEquals(
            [
                'bar-compressed.jpg',
                'bar-compressed.webp',
                'bar-portrait-compressed.jpg',
                'bar-portrait-compressed.webp',
                'bar-portrait.jpg',
                'barbaz-compressed.jpg',
                'barbaz-compressed.webp',
                'barbaz-portrait-compressed.jpg',
                'barbaz-portrait-compressed.webp',
                'barbaz-portrait.jpg',
            ],
            $this->getAllImagesInStorage('bar')
        );
	}

    /** @test */
	public function it_can_be_executed_twice()
	{
        ProjectBuilder::fromSlug('foo')
            ->withUsedImages('foo')
            ->withNotUsedImages('bar')
            ->create();
        
        (new PublishImageAssets)->run(
            new ArrayInput([
                '--publisher' => 'optimized', 
                '--only-used' => false
            ]),
            new NullOutput
        );

        $this->assertEquals(
            [
                'bar-compressed.jpg',
                'bar-compressed.webp',
                'bar-portrait-compressed.jpg',
                'bar-portrait-compressed.webp',
                'bar-portrait.jpg',
                'foo-compressed.jpg',
                'foo-compressed.webp',
                'foo-portrait-compressed.jpg',
                'foo-portrait-compressed.webp',
                'foo-portrait.jpg',
            ],
            $this->getAllImagesInStorage('foo')
        );
        
        (new ClearImageAssets)->run(
            new ArrayInput([]), 
            new NullOutput
        );
        
        $this->assertEquals(
            [
                'foo-compressed.jpg',
                'foo-compressed.webp',
                'foo-portrait-compressed.jpg',
                'foo-portrait-compressed.webp',
                'foo-portrait.jpg',
            ],
            $this->getAllImagesInStorage('foo')
        );

        (new ClearImageAssets)->run(
            new ArrayInput([]), 
            new NullOutput
        );
        
        $this->assertEquals(
            [
                'foo-compressed.jpg',
                'foo-compressed.webp',
                'foo-portrait-compressed.jpg',
                'foo-portrait-compressed.webp',
                'foo-portrait.jpg',
            ],
            $this->getAllImagesInStorage('foo')
        );
	}

    private function getAllImagesInStorage($slug)
    {
        return array_values(
            array_diff(
                scandir(
                    vfsStream::url("folio/storage/images/{$slug}")
                ),
                ['.', '..']
            )
        );
    }
}