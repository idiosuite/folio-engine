<?php

namespace Tests\Unit\Console\Commands;

use App\Console\Commands\GenerateAwsPortfolio;
use App\Generators\FolioManifest;
use App\Themes\Publishers\Manifests\ManifestFactory;
use App\Testing\InteractsWithFileSystem;
use App\Testing\InteractsWithImage;
use App\Testing\InteractsWithMarkdownFiles;
use App\Testing\InteractsWithFolioManifest;
use App\Testing\InspectsBuildUrls;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use App\Themes\Publishers\SymlinkPublisher;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class GenerateAwsPortfolioTest extends TestCase
{
	use InteractsWithFileSystem;
    use InteractsWithImage;
    use InteractsWithMarkdownFiles;
    use InteractsWithFolioManifest;
    use InspectsBuildUrls;

    /** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new GenerateAwsPortfolio);
	}

    /** @test */
	public function it_is_instance_of_command()
	{
	    $this->assertInstanceOf(Command::class, new GenerateAwsPortfolio);
	}

    /** @test */
	public function it_has_correct_name()
	{
	    $this->assertEquals('portfolio:generate-aws', GenerateAwsPortfolio::getDefaultName());
	}

    /** @test */
	public function it_publishes_html_files()
	{
	    $instance = new GenerateAwsPortfolio;

        $this->createConsoleApplication($instance);

        $this->assertFileDoesNotExist(vfsStream::url('folio/public/index.html'));

        $result = $instance->run(new ArrayInput([]), new NullOutput);
        
        $this->assertFileExists(vfsStream::url('folio/public/index.html'));
        $this->assertNotEquals('', file_get_contents(vfsStream::url('folio/public/index.html')));
	}

    /** @test */
	public function it_publishes_optimized_images_folder()
	{
        $instance = new GenerateAwsPortfolio;

        $this->createConsoleApplication($instance);

        $this->assertFileDoesNotExist(vfsStream::url('folio/public/images'));
        $this->assertFileDoesNotExist(ManifestFactory::images()->getFilePath());

        $instance->run(new ArrayInput([]), new NullOutput);

        $this->assertFileExists(vfsStream::url('folio/public/images'));
        $this->assertFileExists(ManifestFactory::images()->getFilePath());
	}

    /** @test */
	public function it_publishes_theme_assets_folder()
	{
        $instance = new GenerateAwsPortfolio;

        $this->createConsoleApplication($instance);

        $this->assertFileDoesNotExist(vfsStream::url('folio/public/_assets'));
        $this->assertFileDoesNotExist(vfsStream::url('folio/public/test'));
        $this->assertFileDoesNotExist(vfsStream::url('folio/public/test/app.css'));

        $instance->run(new ArrayInput([]), new NullOutput);

        $this->assertFileExists(vfsStream::url('folio/public/_assets'));
        $this->assertFileExists(vfsStream::url('folio/public/_assets/test'));
        $this->assertFileExists(vfsStream::url('folio/public/_assets/test/app.css'));
	}

    /** @test */
	public function it_deletes_previous_development_version()
	{
        $manifest = FolioManifest::make()
            ->addFile($index = vfsStream::url('folio/public/index.php'))
            ->addFolder($imagesFolder = vfsStream::url('folio/public/images'))
            ->addFolder($assetsFolder = vfsStream::url('folio/public/_assets'))
            ->write()
            ->createFilesForTest();
        
        $this->assertFileExists($index);
        $this->assertFileExists($imagesFolder);
        $this->assertFileExists($assetsFolder);
        
        $instance = new GenerateAwsPortfolio;
        $this->createConsoleApplication($instance);
        $instance->run(new ArrayInput([]), new NullOutput);
        
        $this->assertFileDoesNotExist($index);
        $this->assertFileExists($imagesFolder);
        $this->assertFileExists($assetsFolder);
	}

    /** @test */
	public function it_deletes_previous_static_version()
	{
        $manifest = FolioManifest::make()
            ->addFile($index = vfsStream::url('folio/public/index.html'))
            ->addFolder($projectsFolder = vfsStream::url('folio/public/project'))
            ->addFile($projectFile = vfsStream::url('folio/public/project/foo.html'))
            ->addFolder($pagesFolder = vfsStream::url('folio/public/page'))
            ->addFile($pageFile = vfsStream::url('folio/public/page/bar.html'))
            ->addFolder($imagesFolder = vfsStream::url('folio/public/images'))
            ->addFolder($projectImagesFolder = vfsStream::url('folio/public/images/foo'))
            ->addFile(vfsStream::url('folio/public/images/foo/foo.jpg'))
            ->addFolder($assetsFolder = vfsStream::url('folio/public/_assets'))
            ->addFolder($themeFolder = vfsStream::url('folio/public/_assets/theme-name'))
            ->write()
            ->createFilesForTest();
        
        $instance = new GenerateAwsPortfolio;
        $this->createConsoleApplication($instance);
        
        $indexBefore = file_get_contents($index);
        $this->assertFileExists($projectsFolder);
        $this->assertFileExists($pagesFolder);
        $this->assertFileExists($projectFile);
        $this->assertFileExists($pageFile);
        $this->assertFileExists($projectImagesFolder);
        $this->assertFileExists($assetsFolder);
        $this->assertFileExists($themeFolder);
        
        $instance->run(new ArrayInput([]), new NullOutput);

        $this->assertFileExists($index);
        $this->assertNotEquals($indexBefore, file_get_contents($index));
        $this->assertFileExists($projectsFolder);
        $this->assertFileExists($pagesFolder);
        $this->assertFileDoesNotExist($projectFile);
        $this->assertFileDoesNotExist($pageFile);
        $this->assertFileDoesNotExist($projectImagesFolder);
        $this->assertFileExists($assetsFolder);
        $this->assertFileDoesNotExist($themeFolder);
	}

    /** @test */
	public function it_handles_publication_of_html_without_images()
	{
        $this->createImageFolderForSlug('foo');
        $this->createDummyImage(vfsStream::url('folio/contents/images/foo/foo.jpg'));
        $this->createPublishedProjectFile(['slug' => 'foo', 'markdown' => '![foo]']);

        $imagesManifest = ManifestFactory::images()->getFilePath();

        $imageFiles = [
            'imagesFolder' => vfsStream::url('folio/public/images'),
            'projectImagesFolder' => vfsStream::url('folio/public/images/foo'),
            'jpgImage' => vfsStream::url('folio/public/images/foo/foo-compressed.jpg'),
            'webpImage' => vfsStream::url('folio/public/images/foo/foo-compressed.webp'),
        ];
        
        $instance = new GenerateAwsPortfolio;
        $this->createConsoleApplication($instance);

        $this->assertFolioManifestDoesNotContainFiles($imageFiles);
        $this->assertFilesDoNotExist($imageFiles);
        
        $instance->run(new ArrayInput(['--images' => true]), new NullOutput);
        
        $this->assertFolioManifestContainsFiles($imageFiles);
        $this->assertFilesExist($imageFiles);
        
        $originalImagesManifest = file_get_contents($imagesManifest);

        $this->tearDownApp();
        $this->setupApp();

        $instance->run(new ArrayInput(['--no-images' => true]), new NullOutput);
        
        $this->assertFolioManifestContainsFiles($imageFiles);
        $this->assertFilesExist($imageFiles);

        $this->assertEquals(
            file_get_contents($imagesManifest),
            $originalImagesManifest
        );
	}

    /** @test */
	public function it_does_not_delete_files_not_generated_by_portfolio()
	{
        $manifest = FolioManifest::make()
            ->addFile($index = vfsStream::url('folio/public/index.html'))
            ->addFolder($projectsFolder = vfsStream::url('folio/public/project'))
            ->addFile(vfsStream::url('folio/public/project/foo.html'))
            ->addFolder($pagesFolder = vfsStream::url('folio/public/page'))
            ->addFile(vfsStream::url('folio/public/page/bar.html'))
            ->addFolder($imagesFolder = vfsStream::url('folio/public/images'))
            ->addFolder(vfsStream::url('folio/public/images/foo'))
            ->addFile(vfsStream::url('folio/public/images/foo/foo.jpg'))
            ->addFolder($themeAssetsFolder = vfsStream::url('folio/public/_assets'))
            ->addFolder($themeAssetsFolder = vfsStream::url('folio/public/_assets/test'))
            ->write()
            ->createFilesForTest();
        
        file_put_contents($foreignFile = vfsStream::url('folio/public/important.html'), 'important data');
        file_put_contents($foreignProjectFile = vfsStream::url('folio/public/project/important.html'), 'important data');
        $this->createDummyImage($foreignImage = vfsStream::url('folio/public/images/important.jpg'));
        $this->createDummyImage($foreignImageNested = vfsStream::url('folio/public/images/foo/important-foo.jpg'));
        
        $instance = new GenerateAwsPortfolio;
        $this->createConsoleApplication($instance);

        $this->assertFileExists($foreignFile);
        $this->assertFileExists($foreignProjectFile);
        $this->assertFileExists($foreignImage);
        $this->assertFileExists($foreignImageNested);

        $instance->run(new ArrayInput([]), new NullOutput);

        $this->assertFileExists($foreignFile);
        $this->assertFileExists($foreignProjectFile);
        $this->assertFileExists($foreignImage);
        $this->assertFileExists($foreignImageNested);
	}

    /** @test */
	public function it_does_not_overrides_newly_created_images()
	{
        $manifest = FolioManifest::make()
            ->addFolder($imagesFolder = vfsStream::url('folio/public/images'))
            ->addFolder($projectImagesFolder = vfsStream::url('folio/public/images/foo'))
            ->addFile($projectImage = vfsStream::url('folio/public/images/foo/foo-compressed.jpg'))
            ->write()
            ->createFilesForTest();

        $originalProjectImage = file_get_contents($projectImage);
        
        $this->createImageFolderForSlug('foo');
        $this->createDummyImage(vfsStream::url('folio/contents/images/foo/foo.jpg'));
        $this->createPublishedProjectFile(['slug' => 'foo', 'markdown' => '![foo]']);
        
        $instance = new GenerateAwsPortfolio;
        $this->createConsoleApplication($instance);

        $this->assertFileExists($projectImagesFolder);
        $this->assertFileExists($projectImage);

        $instance->run(new ArrayInput([]), new NullOutput);

        $this->assertFileExists($projectImagesFolder);
        $this->assertFileExists($projectImage);
        $this->assertNotEquals($originalProjectImage, file_get_contents($projectImage));
	}

    /** @test */
	public function it_generates_a_manifest()
	{
        $this->createImageFolderForSlug('my-project');
        $this->createDummyImage(vfsStream::url('folio/contents/images/my-project/my-compressed-image.jpg'));
        $this->createDummyImage(vfsStream::url('folio/contents/images/my-project/my-uncompressed-image.svg'));
        $this->createPublishedProjectFile(['slug' => 'my-project', 'markdown' => '![my-compressed-image] ![my-uncompressed-image]']);
        $this->createPublishedPageFile(['slug' => 'my-page']);

        $manifest = FolioManifest::fromJson();
        $this->assertCount(0, $manifest);

        $instance = new GenerateAwsPortfolio;
        $this->createConsoleApplication($instance);
        $instance->run(new ArrayInput([]), new NullOutput);
        
        $manifest = FolioManifest::fromJson();
        $this->assertCount(16, $manifest);
        
        // Assets
        $this->assertTrue($manifest->hasFolder(vfsStream::url('folio/public/_assets')));
        $this->assertTrue($manifest->hasFolder(vfsStream::url('folio/public/_assets/test')));
        $this->assertTrue($manifest->hasFile(vfsStream::url('folio/public/_assets/test/app.css')));
        // Images
        $this->assertTrue($manifest->hasFolder(vfsStream::url('folio/public/images')));
        $this->assertTrue($manifest->hasFolder(vfsStream::url('folio/public/images/my-project')));
        $this->assertTrue($manifest->hasFile(vfsStream::url('folio/public/images/my-project/my-uncompressed-image.svg')));
        $this->assertTrue($manifest->hasFile(vfsStream::url('folio/public/images/my-project/my-uncompressed-image-portrait.svg')));
        $this->assertTrue($manifest->hasFile(vfsStream::url('folio/public/images/my-project/my-compressed-image-compressed.jpg')));
        $this->assertTrue($manifest->hasFile(vfsStream::url('folio/public/images/my-project/my-compressed-image-portrait-compressed.jpg')));
        $this->assertTrue($manifest->hasFile(vfsStream::url('folio/public/images/my-project/my-compressed-image-compressed.webp')));
        $this->assertTrue($manifest->hasFile(vfsStream::url('folio/public/images/my-project/my-compressed-image-portrait-compressed.webp')));
        // Html
        $this->assertTrue($manifest->hasFile(vfsStream::url('folio/public/index.html')));
        $this->assertTrue($manifest->hasFolder(vfsStream::url('folio/public/project')));
        $this->assertTrue($manifest->hasFile(vfsStream::url('folio/public/project/my-project.html')));
        $this->assertTrue($manifest->hasFolder(vfsStream::url('folio/public/page')));
        $this->assertTrue($manifest->hasFile(vfsStream::url('folio/public/page/my-page.html')));
	}

    /** @test */
	public function it_should_not_delete_root_folder()
	{
        mkdir($importantFolder = vfsStream::url('folio/important'));
        file_put_contents($importantFile = vfsStream::url('folio/important/important.txt'), 'important data');

        $this->assertFileExists($importantFolder);
        $this->assertFileExists($importantFile);

        $instance = new GenerateAwsPortfolio;
        $this->createConsoleApplication($instance);
        $instance->run(new ArrayInput(['--destination' => $importantFolder]), new NullOutput);

        $this->assertFileExists($importantFolder);
        $this->assertFileExists($importantFile);
	}

    /** @test */
	public function it_replaces_all_base_urls_with_base_url_build()
	{
        $imageRelativePath = $this->createProjectWithImage('foo');
        $this->defineLogoImageInSettings($imageRelativePath);
        $this->customizeTemplateHomeViewToOutputLogo();
        $this->customizeTemplateProjectViewToOutputImages();

        $this->assertNotNull($baseUrlBuild = env('BASE_URL_BUILD'));
        $this->assertNotNull($baseUrl = env('BASE_URL'));
        
	    $instance = new GenerateAwsPortfolio;

        $this->createConsoleApplication($instance);

        $result = $instance->run(new ArrayInput([]), new NullOutput);
        
        $htmlFiles = FolioManifest::fromJson()
            ->filterByType('file')
            ->filterByFileExtension('html')
            ->toArray();

        $this->assertCount(2, $htmlFiles);

        foreach ($htmlFiles as $htmlFile) {
            $html = file_get_contents(
                vfsStream::url('folio/' . $htmlFile['path'])
            );

            $this->assertStringContainsString($baseUrlBuild, $html);
            $this->assertStringNotContainsString($baseUrl, $html);

            $this->assertStringContainsString(
                $this->getJsonEncodedUrl($baseUrlBuild), 
                $html
            );
            $this->assertStringNotContainsString(
                $this->getJsonEncodedUrl($baseUrl), 
                $html
            );
        }
	}

    private function createConsoleApplication(GenerateAwsPortfolio $instance)
	{
		$application = new Application;

		$application->add( new \App\Console\Commands\GeneratePortfolio );
        $application->add( new \App\Console\Commands\PublishImageAssets );
        $application->add( new \App\Console\Commands\PublishThemeAssets );
        $application->add( new \App\Console\Commands\PublishContentAssets );

		$instance->setApplication($application);

		return $application;
	}

    private function assertFilesExist(array $imageFiles)
    {
        foreach ($imageFiles as $key => $path) {
            $this->assertFileExists($path);
        }
    }

    private function assertFilesDoNotExist(array $imageFiles)
    {
        foreach ($imageFiles as $key => $path) {
            $this->assertFileDoesNotExist($path);
        }
    }
}