<?php

namespace Tests\Unit\Generators\Html;

use App\Generators\FolioManifest;
use App\Testing\InteractsWithFileSystem;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class FolioManifestTest extends TestCase
{
    use InteractsWithFileSystem;

	/** @test */
    public function it_can_be_instantiated()
    {
        $this->assertNotNull(new FolioManifest);
    }

    /** @test */
    public function it_implements_countable_interface()
    {
        $this->assertInstanceOf(\Countable::class, new FolioManifest);
    }

    /** @test */
    public function it_implements_arrayable_interface()
    {
        $this->assertInstanceOf(\Illuminate\Contracts\Support\Arrayable::class, new FolioManifest);
    }

    /** @test */
    public function it_is_empty_by_default()
    {
        $this->assertCount(0, new FolioManifest);
    }

    /** @test */
    public function it_can_be_instantiated_with_an_array()
    {
        $this->assertCount(3, FolioManifest::fromArray([
            ['path' => 'path/to/file', 'type' => 'file'],
            ['path' => 'path/to/folder', 'type' => 'folder'],
            ['path' => 'path/to/symlink', 'type' => 'symlink'],
        ]));
    }

    /** @test */
    public function it_will_accept_array_without_type_if_file_exists()
    {
        $absolutePathIndex = vfsStream::url('folio/public/index.html');
        $relativePathIndex = 'public/index.html';

        $this->assertCount(0, FolioManifest::fromArray([
            ['path' => $relativePathIndex],
        ]));

        file_put_contents($absolutePathIndex, '');

        $this->assertCount(1, FolioManifest::fromArray([
            ['path' => $relativePathIndex],
        ]));
    }

    /** @test */
    public function it_will_not_accept_array_without_path()
    {
        $this->assertCount(0, FolioManifest::fromArray([
            ['type' => 'folder'],
        ]));
    }

    /** @test */
    public function it_will_not_accept_invalid_type()
    {
        $this->assertCount(0, FolioManifest::fromArray([
            ['path' => 'path/to/symlink', 'type' => 'invalid-type'],
        ]));
    }

    /** @test */
    public function it_writes_json_file_to_default_destination()
    {
        $destinationFolder = vfsStream::url('folio/manifests');

        $this->assertFileDoesNotExist($destinationFolder . '/manifest.folio.json');

        $instance = (new FolioManifest)
            ->addFile('path/to/file')
            ->write();
        
        $this->assertFileExists($destinationFolder . '/manifest.folio.json');
        
        $this->assertEquals(
            [['path' => 'path/to/file', 'type' => 'file']], 
            json_decode(file_get_contents($destinationFolder . '/manifest.folio.json'), $assoc = true)
        );
    }

    /** @test */
    public function it_writes_json_file_to_specific_destination()
    {
        $destinationFolder = vfsStream::url('folio/public');

        $this->assertFileDoesNotExist($destinationFolder . '/manifest.folio.json');

        $instance = (new FolioManifest)
            ->addFile('path/to/file')
            ->write($destinationFolder);
        
        $this->assertFileExists($destinationFolder . '/manifest.folio.json');
        
        $this->assertEquals(
            [['path' => 'path/to/file', 'type' => 'file']], 
            json_decode(file_get_contents($destinationFolder . '/manifest.folio.json'), $assoc = true)
        );
    }

    /** @test */
    public function it_creates_files_for_tests()
    {
        $projectsFolder = vfsStream::url('folio/public/project');
        $projectFile = vfsStream::url('folio/public/project/foo.html');
        $index = vfsStream::url('folio/public/index.html');

        $this->assertFileDoesNotExist($projectsFolder);
        $this->assertFileDoesNotExist($projectFile);
        $this->assertFileDoesNotExist($index);
        
        $manifest = (new FolioManifest)
            ->addFile($index)
            ->addFolder($projectsFolder)
            ->addFile($projectFile)
            ->createFilesForTest();

        $this->assertFileExists($index);
        $this->assertFileExists($projectFile);
        $this->assertFileExists($projectsFolder);
    }

    /** @test */
    public function it_erases_files_and_folders_in_the_manifest_file()
    {
        $projectsFolder = vfsStream::url('folio/public/project');
        $projectFile = vfsStream::url('folio/public/project/foo.html');
        $index = vfsStream::url('folio/public/index.html');

        $foreignFile = vfsStream::url('folio/public/important.html');

        $notExistingFile = vfsStream::url('folio/public/not-existing.html');
        
        mkdir($projectsFolder);
        file_put_contents($projectFile, 'hello world');
        file_put_contents($index, 'hello world');
        file_put_contents($foreignFile, 'important data');

        $this->assertFileExists($projectsFolder);
        $this->assertFileExists($projectFile);
        $this->assertFileExists($index);
        $this->assertFileExists($foreignFile);
        $this->assertFileDoesNotExist($notExistingFile);

        $manifest = (new FolioManifest)
            ->addFile($index)
            ->addFolder($projectsFolder)
            ->addFile($projectFile)
            ->eraseAllFilesAndFolders();

        $this->assertFileDoesNotExist($index);
        $this->assertFileDoesNotExist($projectFile);
        $this->assertFileDoesNotExist($projectsFolder);
        $this->assertFileExists($foreignFile);
        $this->assertFileDoesNotExist($notExistingFile);
    }

    /** @test */
    public function it_can_be_instantiated_with_a_json_file_in_a_default_folder()
    {
        $destinationFolder = vfsStream::url('folio');

        $this->assertCount(0, FolioManifest::fromJson());

        $instance = (new FolioManifest)
            ->addFile(vfsStream::url('folio/public/index.html'))
            ->addFolder(vfsStream::url('folio/public/project'))
            ->addSymlink(vfsStream::url('folio/public/images'))
            ->write();
        
        $this->assertCount(3, FolioManifest::fromJson());
    }

    /** @test */
    public function it_can_be_instantiated_with_a_json_file_in_a_specific_folder()
    {
        $destinationFolder = vfsStream::url('folio/public');

        $this->assertCount(0, FolioManifest::fromJson(
            $destinationFolder
        ));

        $instance = (new FolioManifest)
            ->addFile(vfsStream::url('folio/public/index.html'))
            ->addFolder(vfsStream::url('folio/public/project'))
            ->addSymlink(vfsStream::url('folio/public/images'))
            ->write($destinationFolder);
        
        $this->assertCount(3, FolioManifest::fromJson(
            $destinationFolder
        ));
    }

    /** @test */
    public function it_accepts_a_file()
    {
        $instance = new FolioManifest;

        $this->assertCount(0, $instance);

        $instance->addFile('path/to/file');

        $this->assertCount(1, $instance);
    }

    /** @test */
    public function it_accepts_a_folder()
    {
        $instance = new FolioManifest;

        $this->assertCount(0, $instance);

        $instance->addFolder('path/to/folder');

        $this->assertCount(1, $instance);
    }

    /** @test */
    public function it_accepts_a_symlink()
    {
        $instance = new FolioManifest;

        $this->assertCount(0, $instance);

        $instance->addSymlink('path/to/symlink');

        $this->assertCount(1, $instance);
    }

    /** @test */
    public function it_asserts_it_has_a_file()
    {
        $instance = new FolioManifest;

        $path = 'path/to/file';

        $this->assertFalse($instance->hasFile($path));
        $this->assertFalse($instance->hasFolder($path));
        $this->assertFalse($instance->hasSymlink($path));

        $instance->addFile($path);

        $this->assertTrue($instance->hasFile($path));
        $this->assertFalse($instance->hasFolder($path));
        $this->assertFalse($instance->hasSymlink($path));
    }

    /** @test */
    public function it_asserts_it_has_a_folder()
    {
        $instance = new FolioManifest;

        $path = 'path/to/folder';

        $this->assertFalse($instance->hasFile($path));
        $this->assertFalse($instance->hasFolder($path));
        $this->assertFalse($instance->hasSymlink($path));

        $instance->addFolder($path);

        $this->assertFalse($instance->hasFile($path));
        $this->assertTrue($instance->hasFolder($path));
        $this->assertFalse($instance->hasSymlink($path));
    }

    /** @test */
    public function it_asserts_it_has_a_symlink()
    {
        $instance = new FolioManifest;

        $path = 'path/to/symlink';

        $this->assertFalse($instance->hasFile($path));
        $this->assertFalse($instance->hasFolder($path));
        $this->assertFalse($instance->hasSymlink($path));

        $instance->addSymlink($path);

        $this->assertFalse($instance->hasFile($path));
        $this->assertFalse($instance->hasFolder($path));
        $this->assertTrue($instance->hasSymlink($path));
    }

    /** @test */
    public function it_does_not_accept_duplicates()
    {
        $instance = new FolioManifest;

        $path = 'path/to/file';

        $this->assertCount(0, $instance);

        $instance->addFile($path);
        $this->assertCount(1, $instance);

        $instance->addFile($path);
        $this->assertCount(1, $instance);

        $instance->addFolder($path);
        $this->assertCount(1, $instance);

        $instance->addSymlink($path);
        $this->assertCount(1, $instance);
    }

    /** @test */
    public function it_does_not_store_absolute_path_in_json_file()
    {
        $instance = new FolioManifest;

        $path = vfsStream::url('folio/public/index.html');

        $this->assertCount(0, $instance);

        $instance->addFile($path);
        $this->assertCount(1, $instance);

        $this->assertTrue($instance->hasFile($path));

        $this->assertEquals('public/index.html', $instance->toArray()[0]['path']);
    }

    /** @test */
    public function it_allows_only_real_path_when_adding()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Only real path allowed.');

        $instance = new FolioManifest;

        $path = vfsStream::url('folio/public/../contents/images');
        
        $this->assertFileExists($path);

        $this->assertCount(0, $instance);

        $instance->addFile($path);
        $this->assertCount(0, $instance);

        $this->assertFalse($instance->hasFile($path));
    }

    /** @test */
    public function it_allows_only_real_path_when_reading()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Only real path allowed.');

        file_put_contents(
            vfsStream::url('folio/public/manifest.folio.json'),
            json_encode([['path' => '../contents/images', 'type' => 'folder']])
        );

        $this->assertFileExists(
            vfsStream::url('folio/contents/images')
        );

        $instance = FolioManifest::fromJson(vfsStream::url('folio/public'));
    }
}