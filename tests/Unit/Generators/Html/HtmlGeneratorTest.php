<?php

namespace Tests\Unit\Generators\Html;

use App\Generators\Html\HtmlGenerator;
use App\Portfolio;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class HtmlGeneratorTest extends TestCase
{
	private $root;
	private $basePath;
	private $destinationPath;

	/** @before */
	public function setUpVfsStream()
	{
		$structure = [
			'contents' => [
				'images' => [],
				'pages' => [
					'bar.md' => '---
layout: page
title: Bar
published_at: yesterday
---
Barbaz'
				],
				'projects' => [
					'foo.md' => '---
layout: project
title: Foo
published_at: yesterday
---
Foobar'
				],
				'preferences' => [
					'defaults.json' => '[]'
				],
			],
			'layouts' => [],
			'public' => [
				'images' => []
			]
		];

		$this->root = vfsStream::setup('folio', null, $structure);
		$this->basePath = vfsStream::url('folio');
		$this->destinationPath = vfsStream::url('folio/public');
	}
	
	/** @test */
	public function it_can_be_instantiated()
	{
		$this->assertNotNull($this->getInstance());
	}

	/** @test */
	public function it_generates_index()
	{
	    $instance = $this->getInstance();

	    $this->assertNotContains('index.html', $this->getPublicHtmlFiles());

	    $instance->generate();

	    $this->assertContains('index.html', $this->getPublicHtmlFiles());
	    $this->assertEquals(
	    	'Homepage Content', 
	    	file_get_contents(vfsStream::url('folio/public/index.html'))
	    );
	}

	/** @test */
	public function it_creates_project_folder_if_does_not_exists()
	{
		$instance = $this->getInstance();

	    $this->assertNotContains('project', $this->getPublicHtmlFiles());

	    $instance->generate();

	    $this->assertContains('project', $this->getPublicHtmlFiles());
	}

	/** @test */
	public function it_creates_page_folder_if_does_not_exists()
	{
		$instance = $this->getInstance();

	    $this->assertNotContains('page', $this->getPublicHtmlFiles());

	    $instance->generate();

	    $this->assertContains('page', $this->getPublicHtmlFiles());
	}

	/** @test */
	public function it_generates_projects_html_files()
	{
		$this->createProjectFolder();

	    $instance = $this->getInstance();

	    $this->assertCount(0, $this->getPublicHtmlFiles('folio/public/project'));
	    
	    $instance->generate();

	    $this->assertCount(1, $this->getPublicHtmlFiles('folio/public/project'));
	    $this->assertContains('foo.html', $this->getPublicHtmlFiles('folio/public/project'));
	    $this->assertEquals(
	    	'Post Content', 
	    	file_get_contents(vfsStream::url('folio/public/project/foo.html'))
	    );
	}

	/** @test */
	public function it_generates_pages_html_files()
	{
		$this->createPageFolder();

	    $instance = $this->getInstance();

	    $this->assertCount(0, $this->getPublicHtmlFiles('folio/public/page'));
	    
	    $instance->generate();

	    $this->assertCount(1, $this->getPublicHtmlFiles('folio/public/page'));
	    $this->assertContains('bar.html', $this->getPublicHtmlFiles('folio/public/page'));
	    $this->assertEquals(
	    	'Post Content', 
	    	file_get_contents(vfsStream::url('folio/public/page/bar.html'))
	    );
	}

	/** @test */
	public function it_will_not_generate_project_html_file_if_render_is_disabled()
	{
		$this->createProjectFolder();

	    $portfolio = new Portfolio($this->basePath);
		
		$portfolio->projects()
			->addWithSlug('foo')
			->addWithArray([
				'slug' => 'do-not-render',
				'path' => '/project/do-not-render',
				'disableRender' => true
			]);
		
		$instance = $this->getInstance($portfolio);

		$this->assertCount(0, $this->getPublicHtmlFiles('folio/public/project'));
	    
	    $instance->generate();

	    $this->assertCount(1, $this->getPublicHtmlFiles('folio/public/project'));
	    $this->assertContains('foo.html', $this->getPublicHtmlFiles('folio/public/project'));
	}

	/** @test */
	public function it_will_not_generate_page_html_file_if_render_is_disabled()
	{
		$this->createPageFolder();

	    $portfolio = new Portfolio($this->basePath);
		
		$portfolio->pages()
			->addWithSlug('bar')
			->addWithArray([
				'slug' => 'contacts',
				'path' => 'mailto:info@example.com',
				'disableRender' => true
			]);
		
		$instance = $this->getInstance($portfolio);

		$this->assertCount(0, $this->getPublicHtmlFiles('folio/public/page'));
	    
	    $instance->generate();

	    $this->assertCount(1, $this->getPublicHtmlFiles('folio/public/page'));
	    $this->assertContains('bar.html', $this->getPublicHtmlFiles('folio/public/page'));
	}

	/** @test */
	public function it_will_not_generate_page_html_file_if_default_slug()
	{
		$this->createPageFolder();

	    $portfolio = new Portfolio($this->basePath);

		$portfolio->settings()->defaultSlug = 'bar';

		$portfolio->pages()->addWithSlug(
			$portfolio->settings()->defaultSlug
		);
		
		$instance = $this->getInstance($portfolio);

		$this->assertCount(0, $this->getPublicHtmlFiles('folio/public/page'));
	    
	    $instance->generate();

	    $this->assertCount(0, $this->getPublicHtmlFiles('folio/public/page'));
	}

	/** @test */
	public function it_will_not_generate_project_html_file_if_not_published()
	{
		$this->createProjectFolder();

	    $portfolio = new Portfolio($this->basePath);
		
		$portfolio->projects()
			->addWithSlug('foo')
			->addWithArray([
				'slug' => 'not-published',
				'path' => '/project/not-published',
				'published_at' => 'tomorrow'
			]);
		
		$instance = $this->getInstance($portfolio);

		$this->assertCount(0, $this->getPublicHtmlFiles('folio/public/project'));
	    
	    $instance->generate();

	    $this->assertCount(1, $this->getPublicHtmlFiles('folio/public/project'));
	    $this->assertContains('foo.html', $this->getPublicHtmlFiles('folio/public/project'));
	}

	/** @test */
	public function it_will_not_generate_page_html_file_if_not_published()
	{
		$this->createPageFolder();

	    $portfolio = new Portfolio($this->basePath);
		
		$portfolio->pages()
			->addWithSlug('bar')
			->addWithArray([
				'slug' => 'not-published',
				'path' => '/page/not-published',
				'published_at' => 'tomorrow'
			]);
		
		$instance = $this->getInstance($portfolio);

		$this->assertCount(0, $this->getPublicHtmlFiles('folio/public/page'));
	    
	    $instance->generate();

	    $this->assertCount(1, $this->getPublicHtmlFiles('folio/public/page'));
	    $this->assertContains('bar.html', $this->getPublicHtmlFiles('folio/public/page'));
	}

	/** @test */
	public function it_writes_manifest()
	{
		$manifest = app(\App\Generators\FolioManifest::class);

		$instance = $this->getInstance();

		$this->assertCount(0, $manifest);

		$instance->generate();

		$this->assertCount(5, $manifest);
		$this->assertTrue($manifest->hasFile(vfsStream::url('folio/public/index.html')));
		$this->assertTrue($manifest->hasFolder(vfsStream::url('folio/public/project')));
		$this->assertTrue($manifest->hasFolder(vfsStream::url('folio/public/page')));
		$this->assertTrue($manifest->hasFile(vfsStream::url('folio/public/project/foo.html')));
		$this->assertTrue($manifest->hasFile(vfsStream::url('folio/public/page/bar.html')));
	}

	private function createProjectFolder()
	{
		mkdir(vfsStream::url('folio/public/project'));
	}

	private function createPageFolder()
	{
		mkdir(vfsStream::url('folio/public/page'));
	}

	private function getInstance(Portfolio $portfolio = null)
	{
		if ( ! $portfolio) {
			$portfolio = new Portfolio($this->basePath);
			$portfolio->autoDiscovery();
		}

		$generator = new HtmlGenerator($portfolio, $this->destinationPath);

		$generator->swapResponseProviderForTest(
			$this->getThemeResponseProviderMock()
		);

		return $generator;
	}

	private function getThemeResponseProviderMock()
	{
		$themeResponseProviderMock = $this->createMock(
			\App\Themes\ThemeResponseProvider::class
		);

		$themeResponseProviderMock
			->method('homepage')
			->will($this->returnValue(response('Homepage Content')));
		
		$themeResponseProviderMock
			->method('content')
			->will($this->returnValue(response('Post Content')));

		return $themeResponseProviderMock;
	}
}