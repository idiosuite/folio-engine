<?php

namespace Tests\Unit\Generators\Json;

use App\Testing\InteractsWithFileSystem;
use App\Testing\InteractsWithImage;
use App\Generators\Json\JsonGenerator;
use App\Generators\FolioManifest;
use App\Portfolio;
use App\Repositories\Array\ArrayRepository;
use App\Repositories\Json\JsonRepository;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class JsonGeneratorTest extends TestCase
{
	use InteractsWithFileSystem;
	use InteractsWithImage;

	private $basePath;
	private $destinationPath;
	private $clientsDestinationPath;
	private $jsonDestinationPath;
	private $projectsDestinationPath;
	private $pagesDestinationPath;
	private $settingsDestinationPath;
	private $linksDestinationPath;

	public function setUp() : void
	{
		$this->basePath = vfsStream::url('folio');
		$this->destinationPath = vfsStream::url('folio/public');
		$this->jsonDestinationPath = vfsStream::url('folio/public/json');
		$this->clientsDestinationPath = $this->jsonDestinationPath . '/clients.json';
		$this->projectsDestinationPath = $this->jsonDestinationPath . '/projects.json';
		$this->pagesDestinationPath = $this->jsonDestinationPath . '/pages.json';
		$this->settingsDestinationPath = $this->jsonDestinationPath . '/settings.json';
		$this->linksDestinationPath = $this->jsonDestinationPath . '/links.json';
	}

	protected function getStructure()
	{
		return [
			'contents' => [
				'images' => [
				],
				'pages' => [
					'bar_public.md' => '---
layout: page
title: Bar Public
published_at: 2022-03-05
---
Barbaz',
					'bar_private.md' => '---
layout: page
title: Bar Private
---
Barbaz',
				],
				'projects' => [
					'foo_public.md' => '---
layout: project
title: Foo Public
published_at: 2022-02-25
---
Foobar',
					'foo_private.md' => '---
layout: project
title: Foo Private
---
Foobar',
				],
				'preferences' => [
					'defaults.json' => '[]'
				],
				'awards.json' => json_encode([
					['image' => '/image.jpg', 'foo' => 'bar'],
				]),
				'foo.json' => json_encode([
					['foo' => 'bar'],
				]),
				'links.json' => json_encode([
					[
						'slug' => 'email', 
						'text' => 'Email', 
						'url' => 'mailto:example@example.com'
					],
				]),
				'settings.json' => json_encode([
					'foo' => 'bar'
				])
			],
			'layouts' => [],
			'public' => []
		];
	}
	
	/** @test */
	public function it_can_be_instantiated()
	{
		$this->assertNotNull($this->getInstance());
	}

	/** @test */
	public function it_implements_generator_interface()
	{
		$this->assertInstanceOf(
			\App\Generators\Generator::class, 
			$this->getInstance()
		);
	}

	/** @test */
	public function it_creates_json_folder_if_it_does_not_exist()
	{
	    $this->assertFileDoesNotExist($this->jsonDestinationPath);

	    $this->getInstance()->generate();

	    $this->assertFileExists($this->jsonDestinationPath);
	}

	/** @test */
	public function it_generates_manifest()
	{
		$manifest = app(\App\Generators\FolioManifest::class);

	    $this->assertCount(0, $manifest);

	    $this->getInstance()->generate();

		$this->assertCount(6, $manifest);
		$this->assertTrue($manifest->hasFolder($this->jsonDestinationPath));
		$this->assertTrue($manifest->hasFile($this->clientsDestinationPath));
		$this->assertTrue($manifest->hasFile($this->linksDestinationPath));
		$this->assertTrue($manifest->hasFile($this->pagesDestinationPath));
		$this->assertTrue($manifest->hasFile($this->projectsDestinationPath));
		$this->assertTrue($manifest->hasFile($this->settingsDestinationPath));
	}

	/** @test */
	public function it_publishes_projects_json_file()
	{
	    $this->assertFileDoesNotExist($this->projectsDestinationPath);

	    $this->getInstance()->generate();

	    $this->assertFileExists($this->projectsDestinationPath);
	}

	/** @test */
	public function it_publishes_projects_data()
	{
	    $this->getInstance()->generate();

	    $result = $this->getDataFromFile($this->projectsDestinationPath);

	    $this->assertIsArray($result);
	    
	    $this->assertArrayHasKey('projects', $result);
	    $this->assertIsArray($result['projects']);
	    $this->assertCount(1, $result['projects']);
	    
	    $this->assertArrayHasKey('type', $result['projects'][0]);
	    $this->assertEquals('project', $result['projects'][0]['type']);
	    $this->assertArrayHasKey('title', $result['projects'][0]);
	    $this->assertEquals('Foo Public', $result['projects'][0]['title']);
	}

	/** @test */
	public function it_publishes_pages_json_file()
	{
	    $this->assertFileDoesNotExist($this->pagesDestinationPath);

	    $this->getInstance()->generate();

	    $this->assertFileExists($this->pagesDestinationPath);
	}

	/** @test */
	public function it_publishes_pages_data()
	{
	    $this->getInstance()->generate();

	    $result = $this->getDataFromFile($this->pagesDestinationPath);

	    $this->assertIsArray($result);
	    
	    $this->assertArrayHasKey('pages', $result);
	    $this->assertIsArray($result['pages']);
	    $this->assertCount(1, $result['pages']);
	    
	    $this->assertArrayHasKey('type', $result['pages'][0]);
	    $this->assertEquals('page', $result['pages'][0]['type']);
	    $this->assertArrayHasKey('title', $result['pages'][0]);
	    $this->assertEquals('Bar Public', $result['pages'][0]['title']);
	}

	/** @test */
	public function it_publishes_settings_json_file()
	{
	    $this->assertFileDoesNotExist($this->settingsDestinationPath);

	    $this->getInstance()->generate();

	    $this->assertFileExists($this->settingsDestinationPath);
	}

	/** @test */
	public function it_publishes_settings_data()
	{
	    $this->getInstance()->generate();

	    $result = $this->getDataFromFile($this->settingsDestinationPath);

	    $this->assertIsArray($result);
	    
	    $this->assertEquals([
	    	'settings' => [
	    		'foo' => 'bar'
	    	]
	    ], $result);
	}

	/** @test */
	public function it_published_runtime_settings_data()
	{
	    registry()->settings()->merge(['bar' => 'baz']);

		$this->getInstance()->generate();

	    $result = $this->getDataFromFile($this->settingsDestinationPath);

	    $this->assertIsArray($result);
	    
	    $this->assertEquals([
	    	'settings' => [
	    		'foo' => 'bar',
	    		'bar' => 'baz'
	    	]
	    ], $result);
	}

	/** @test */
	public function it_publishes_links_json_file()
	{
	    $this->assertFileDoesNotExist($this->linksDestinationPath);

	    $this->getInstance()->generate();

	    $this->assertFileExists($this->linksDestinationPath);
	}

	/** @test */
	public function it_publishes_links_data()
	{
	    $this->getInstance()->generate();

	    $result = $this->getDataFromFile($this->linksDestinationPath);

	    $this->assertIsArray($result);
	    
	    $this->assertEquals([
	    	'links' => [
	    		['slug' => 'email', 'text' => 'Email', 'url' => 'mailto:example@example.com'],
	    	]
	    ], $result);
	}

	/** @test */
	public function it_publishes_registry_json_file_registered_at_runtime()
	{
		registry()->register(
			'foo', 
			new JsonRepository("{$this->basePath}/contents/foo.json")
		);

	    $this->assertFileDoesNotExist($this->getDestinationFilePath('foo'));

	    $this->getInstance()->generate();

	    $this->assertFileExists($this->getDestinationFilePath('foo'));
	}

	/** @test */
	public function it_publishes_registry_data_registered_at_runtime()
	{
		registry()->register(
			'foo', 
			new JsonRepository("{$this->basePath}/contents/foo.json")
		);

	    $this->getInstance()->generate();

	    $result = $this->getDataFromFile($this->getDestinationFilePath('foo'));

	    $this->assertIsArray($result);
	    
	    $this->assertEquals([
	    	'foo' => [
	    		['foo' => 'bar']
	    	]
	    ], $result);
	}

	/** 
	 * @test 
	 * @dataProvider provideImageSupportedFormats
	 * */
	public function it_automagically_resolves_image_paths_in_settings($filename)
	{
		$this->createDummyImage(
			vfsStream::url("folio/contents/images/{$filename}"),
			$size = ['width' => 120, 'height' => 20]
		);

		registry()->settings()->merge([ 'my-image' => "/{$filename}" ]);

	    $this->getInstance()->generate();

	    $result = $this->getDataFromFile($this->getDestinationFilePath('settings'));

	    $this->assertEquals([
	    	'src' => "{$this->baseUrlImages}/{$filename}",
	    	'width' => $size['width'],
	    	'height' => $size['height']
	    ], $result['settings']['my-image']);
	}

	/** 
	 * @test 
	 * @dataProvider provideImageSupportedFormats
	 * */
	public function it_automagically_resolves_image_paths_in_runtime_file($filename)
	{
		$this->createDummyImage(
			vfsStream::url("folio/contents/images/{$filename}"),
			$size = ['width' => 120, 'height' => 20]
		);

		registry()->register(
			'awards', 
			new ArrayRepository([
				['my-image' => "/{$filename}"]
			])
		);

		$this->getInstance()->generate();

	    $result = $this->getDataFromFile($this->getDestinationFilePath('awards'));

	    $this->assertEquals([
	    	'src' => "{$this->baseUrlImages}/{$filename}",
	    	'width' => $size['width'],
	    	'height' => $size['height']
	    ], $result['awards'][0]['my-image']);
	}

	/** @test */
	public function it_automagically_resolves_image_paths_in_runtime_file_json()
	{
		$this->createDummyImage(
			vfsStream::url("folio/contents/images/image.jpg"),
			$size = ['width' => 120, 'height' => 20]
		);

		registry()->register(
			'awards', 
			new JsonRepository(vfsStream::url('folio/contents/awards.json'))
		);

		$this->getInstance()->generate();

	    $result = $this->getDataFromFile($this->getDestinationFilePath('awards'));

	    $this->assertEquals([
	    	'src' => $this->baseUrlImages . '/image.jpg',
	    	'width' => $size['width'],
	    	'height' => $size['height']
	    ], $result['awards'][0]['image']);
	}

	/** @test */
	public function it_automagically_resolves_image_paths_in_runtime_file_json_if_image_not_exists()
	{
		$url = vfsStream::url("folio/contents/images/image.jpg");

		registry()->register(
			'awards', 
			new JsonRepository(vfsStream::url('folio/contents/awards.json'))
		);

		$this->getInstance()->generate();

	    $result = $this->getDataFromFile($this->getDestinationFilePath('awards'));

	    $this->assertEquals([
	    	'src' => null,
	    	'width' => 0,
	    	'height' => 0
	    ], $result['awards'][0]['image']);
	}

	/** 
	 * @test 
	 * @dataProvider provideImageSupportedFormats
	 * */
	public function it_will_not_automagically_resolve_image_paths_if_already_object($filename)
	{
		$this->createDummyImage(
			$url = vfsStream::url("folio/contents/images/{$filename}"),
			$size = ['width' => 120, 'height' => 20]
		);

		registry()->register(
			'awards', 
			new ArrayRepository([
				[
					'image' => [
						'src' => "/{$filename}",
						'width' => '100',
						'height' => 100
					]
				]
			])
		);

		$this->getInstance()->generate();

	    $result = $this->getDataFromFile($this->getDestinationFilePath('awards'));

	    $this->assertEquals("/{$filename}", $result['awards'][0]['image']['src']);
	}

	public function provideImageSupportedFormats()
	{
		return [
			['image.jpg'],
			['image.png'],
			['image.svg']
		];
	}

	private function getDataFromFile($path)
	{
		return json_decode(
	    	file_get_contents($path), 
	    	$assoc = true
	    );
	}

	private function getDestinationFilePath($key)
	{
		return $this->jsonDestinationPath . '/' . $key . '.json';
	}

	private function getInstance($portfolio = null)
	{
		if ( ! $portfolio) {
			$portfolio = new Portfolio($this->basePath);
			$portfolio->autoDiscovery();
		}

		return new JsonGenerator($portfolio, $this->destinationPath);
	}
}