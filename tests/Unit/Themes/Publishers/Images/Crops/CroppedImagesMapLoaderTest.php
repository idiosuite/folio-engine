<?php

namespace Tests\Unit\Themes\Publishers\Images\Crops;

use Tests\TestCase;
use org\bovigo\vfs\vfsStream;
use App\Testing\ProjectBuilder;
use App\Testing\InteractsWithFileSystem;
use App\Themes\Publishers\Images\Crops\CroppedImagesMap;
use App\Themes\Publishers\Images\Crops\CroppedImagesMapLoader;

class CroppedImagesMapLoaderTest extends TestCase
{
    use InteractsWithFileSystem;
    
	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new CroppedImagesMapLoader);
	}
    
    /** @test */
	public function it_loads_map_from_file_system()
	{
	    $foo = $this->createProjectWithUsedImage('foo');
        $bar = $this->createProjectWithUsedImage('bar');;
        $baz = $this->createProjectWithUsedImage('baz');;
        
        $this->writeConfigFile('foo', [
            'foo.jpg' => ['skip' => true]
        ]);
        
        $this->writeConfigFile('baz', [
            'baz.jpg' => ['limit-crop' => 'square']
        ]);

        $expectedKeys = [
            'foo' => vfsStream::url('folio/contents/images/foo/foo.jpg'),
            'bar' => vfsStream::url('folio/contents/images/bar/bar.jpg'),
            'baz' => vfsStream::url('folio/contents/images/baz/baz.jpg'),
        ];
        
        $map = new CroppedImagesMap;

        $this->assertFalse( $map->hasExactly($expectedKeys['foo']) );
        $this->assertFalse( $map->hasExactly($expectedKeys['bar']) );
        $this->assertFalse( $map->hasExactly($expectedKeys['baz']) );

        (new CroppedImagesMapLoader)->load($map);
        
        $this->assertTrue( $map->hasExactly($expectedKeys['foo']) );
        $this->assertFalse( $map->hasExactly($expectedKeys['bar']) );
        $this->assertTrue( $map->hasExactly($expectedKeys['baz']) );
	}

    /** @test */
	public function it_allows_to_skip_all()
	{
	    $foo = $this->createProjectWithUsedImage('foo');
        
        $this->writeConfigFile('foo', [
            '/' => ['skip' => true]
        ]);

        $fooImage = vfsStream::url('folio/contents/images/foo/foo.jpg');
        
        $map = new CroppedImagesMap;

        $this->assertNotEquals([], $map->getFormats($fooImage) );
        
        (new CroppedImagesMapLoader)->load($map);

        $this->assertEquals([], $map->getFormats($fooImage) );
	}

    /** @test */
	public function it_allows_to_skip_all_except_whitelisted()
	{
	    $foo = $this->createProjectWithUsedImage('foo', [
            'foo',
            'bar'
        ]);
        
        $this->writeConfigFile('foo', [
            '/bar.jpg' => ['crop-limit' => 'none'],
            '/' => ['skip' => true],
        ]);

        $fooImage = vfsStream::url('folio/contents/images/foo/foo.jpg');
        $barImage = vfsStream::url('folio/contents/images/foo/bar.jpg');
        
        $map = new CroppedImagesMap;

        $this->assertNotEquals([], $map->getFormats($fooImage) );
        $this->assertNotEquals([], $map->getFormats($barImage) );
        
        (new CroppedImagesMapLoader)->load($map);

        $this->assertEquals([], $map->getFormats($fooImage) );
        $this->assertEquals(['square', 'portrait'], $map->getFormats($barImage) );
	}

    /** @test */
	public function it_guesses_extension()
	{
	    $foo = $this->createProjectWithUsedImage('foo', [
            'foo',
            'bar'
        ]);
        
        $this->writeConfigFile('foo', [
            'bar' => ['skip' => true],
        ]);

        $fooImage = vfsStream::url('folio/contents/images/foo/foo.jpg');
        $barImage = vfsStream::url('folio/contents/images/foo/bar.jpg');
        
        $map = new CroppedImagesMap;

        $this->assertNotEquals([], $map->getFormats($fooImage) );
        $this->assertNotEquals([], $map->getFormats($barImage) );
        
        (new CroppedImagesMapLoader)->load($map);

        $this->assertEquals(['square', 'portrait'], $map->getFormats($fooImage) );
        $this->assertEquals([], $map->getFormats($barImage) );
	}

    private function createProjectWithUsedImage($slug, array $images = [])
    {
        return ProjectBuilder::fromSlug($slug)
            ->withUsedImages($images ?: $slug)
            ->create();
    }

    private function writeConfigFile(string $slug, array $map)
    {
        file_put_contents(
            vfsStream::url("folio/contents/images/{$slug}/images.json"),
            json_encode($map)
        );
    }
}