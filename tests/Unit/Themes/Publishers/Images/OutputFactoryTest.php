<?php

namespace Tests\Unit\Themes\Publishers\Images;

use App\Themes\Publishers\Images\Output\OutputFactory;
use Tests\TestCase;

class OutputFactoryTest extends TestCase
{
	private $expectedOutput = [
    	'webp' => [
    		'method' => 'compressed', 
    		'exports' => ['jpg', 'webp']
    	],
	    'png' => [
	    	'method' => 'compressed', 
	    	'exports' => ['jpg', 'webp'], 
	    	'exports:alpha' => ['png', 'webp']
	    ],
	    'jpg' => [
	    	'method' => 'compressed', 
	    	'exports' => ['jpg', 'webp']
	    ],
	    'gif' => [
	    	'method' => 'uncompressed'
	    ],
	    'svg' => [
	    	'method' => 'uncompressed'
	    ],
	    'default' => [
	    	'method' => 'skip'
	    ]
	];

	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new OutputFactory);
	}

	/** @test */
	public function it_can_be_used_fluently()
	{
	    $outputFactory = (new OutputFactory)
	    	->export('webp')->compress(['jpg', 'webp'])
	    	->export('png')->compress(['jpg', 'webp'])->compressAlpha(['png', 'webp'])
	    	->export('jpg')->compress(['jpg', 'webp'])
	    	->export('gif')->raw()
	    	->export('svg')->raw()
	    	->export('default')->skip();

	    $this->assertEquals($this->expectedOutput, $outputFactory->toArray());
	}

	/** @test */
	public function it_returns_default_array()
	{
	    $this->assertEquals($this->expectedOutput, OutputFactory::make()->toArray());
	}

	/** @test */
	public function it_registers_format_to_compress()
	{
	    $outputFactory = (new OutputFactory)
	    	->export('webp')->compress(['jpg', 'webp']);

	    $this->assertEquals(
	    	['method' => 'compressed', 'exports' => ['jpg', 'webp']], 
	    	$outputFactory->get('webp')->toArray()
	    );
	}

	/** @test */
	public function it_registers_format_to_compress_with_alpha()
	{
	    $outputFactory = (new OutputFactory)
	    	->export('png')->compressAlpha(['png', 'webp']);

	    $this->assertEquals(
	    	['method' => 'compressed', 'exports:alpha' => ['png', 'webp']], 
	    	$outputFactory->get('png')->toArray()
	    );
	}

	/** @test */
	public function it_registers_format_to_compress_with_alpha_and_without()
	{
	    $outputFactory = (new OutputFactory)
	    	->export('png')
	    	->compress(['jpg', 'webp'])
	    	->compressAlpha(['png', 'webp']);

	    $this->assertEquals(
	    	[
	    		'method' => 'compressed', 
	    		'exports' => ['jpg', 'webp'], 
	    		'exports:alpha' => ['png', 'webp']
	    	], 
	    	$outputFactory->get('png')->toArray()
	    );
	}

	/** @test */
	public function it_registers_format_not_to_compress()
	{
	    $outputFactory = (new OutputFactory)->export('svg')->raw();

	    $this->assertEquals(
	    	['method' => 'uncompressed'], 
	    	$outputFactory->get('svg')->toArray()
	    );
	}

	/** @test */
	public function it_returns_qualified_row_for_known_format()
	{
	    $this->assertEquals(
			['method' => 'compressed', 'exports' => ['jpg', 'webp']], 
			OutputFactory::make()->get('webp')->toArray()
		);
	}

	/** @test */
	public function it_returns_default_for_unknown_format()
	{
	    $this->assertEquals(
			['method' => 'skip'], 
			OutputFactory::make()->get('json')->toArray()
		);
	}

	/** @test */
	public function it_returns_skip_by_default()
	{
	    $this->expectException(\Exception::class);
	    $this->expectExceptionMessage('Please provide a [default] export or specify an export for [json]');

		$this->assertEquals(
			['method' => 'skip'], 
			(new OutputFactory)->get('json')->toArray()
		);
	}
}