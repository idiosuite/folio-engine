<?php

namespace Tests\Unit\Themese\Publishers\Images\Processors;

use Tests\TestCase;
use org\bovigo\vfs\vfsStream;
use App\Testing\InteractsWithImage;
use App\Testing\InteractsWithFileSystem;
use App\Testing\InteractsWithMarkdownFiles;
use App\Themes\Publishers\Manifests\CropsManifest;
use App\Themes\Publishers\Manifests\ManifestFactory;
use App\Themes\Publishers\Images\Processors\Processor;
use App\Themes\Publishers\Images\Used\UsedImagesFinder;
use App\Themes\Publishers\Images\Crops\CroppedImagesMap;
use App\Themes\Publishers\Images\Processors\AllImagesProvider;
use App\Themes\Publishers\Images\Processors\ProcessorProvider;
use App\Themes\Publishers\Images\Processors\UsedImagesProvider;
use App\Themes\Publishers\Images\Processors\CreateImageFormatsProcessor;

class CreateImageFormatsProcessorTest extends TestCase
{
	use InteractsWithFileSystem;
	use InteractsWithImage;
    use InteractsWithMarkdownFiles;
	
	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new CreateImageFormatsProcessor);
	}

	/** @test */
	public function it_is_instance_of_processor()
	{
	    $this->assertInstanceOf(Processor::class, new CreateImageFormatsProcessor);
	}

    /** @test */
	public function it_is_instance_of_processor_provider()
	{
	    $this->assertInstanceOf(ProcessorProvider::class, new CreateImageFormatsProcessor);
	}

	/** @test */
	public function it_creates_crops_for_all_used_pictures()
	{
        $this->bootstrapPublishedProjectWithoutImages('bar');
        $this->bootstrapPublishedProject('baz');

        $crops = [
            $portraitFoo = vfsStream::url('folio/storage/images/foo-portrait.jpg'),
            $portraitBar = vfsStream::url('folio/storage/images/bar/bar-portrait.jpg'),
            $portraitBaz = vfsStream::url('folio/storage/images/baz/baz-portrait.jpg'),
        ];
        
        $this->assertFileDoesNotExist($portraitFoo);
        $this->assertFileDoesNotExist($portraitBar);
        $this->assertFileDoesNotExist($portraitBaz);
        
        $instance = new CreateImageFormatsProcessor;
		$result = $instance->process(new UsedImagesProvider);

        $this->assertInstanceOf(ProcessorProvider::class, $result);
        $this->assertSame($instance, $result);
        
        $this->assertFileDoesNotExist($portraitFoo);
        $this->assertFileDoesNotExist($portraitBar);
        $this->assertFileExists($portraitBaz);
	}

    /** @test */
	public function it_returns_images()
	{
        $this->bootstrapPublishedProject('bar');
        
        $expectedCrops = [
            $portraitBar = vfsStream::url('folio/storage/images/bar/bar-portrait.jpg'),
        ];

        $instance = new CreateImageFormatsProcessor;
		$result = $instance->process(new UsedImagesProvider);

        $this->assertIsArray($instance->getImages());
        $this->assertEquals(
            [
                $this->getSourceImage('bar') => [
                    $this->getSourceImage('bar'),
                    $portraitBar,
                ]
            ],
            $instance->getImages()
        );
	}

    /** @test */
	public function it_does_not_create_crops_for_picture_if_already_created()
	{
        $this->bootstrapPublishedProject('bar');
        $this->bootstrapPublishedProject('baz');

        $crops = [
            $portraitFoo = vfsStream::url('folio/storage/images/foo-portrait.jpg'),
            $portraitBar = vfsStream::url('folio/storage/images/bar/bar-portrait.jpg'),
            $portraitBaz = vfsStream::url('folio/storage/images/baz/baz-portrait.jpg'),
        ];
        
        $this->assertFileDoesNotExist($portraitFoo);
        $this->assertFileDoesNotExist($portraitBar);
        $this->assertFileDoesNotExist($portraitBaz);
        
        $instance = new CreateImageFormatsProcessor;
		$instance->process(new UsedImagesProvider);
        
        $this->assertFileDoesNotExist($portraitFoo);
        $this->assertFileExists($portraitBar);
        $this->assertFileExists($portraitBaz);

        $this->emptyFilesToVerifyChanges([
            $portraitBar,
            $portraitBaz
        ]);
        
        $instance->process(new UsedImagesProvider);

        $this->assertFilesAreNotChanged([
            $portraitBar,
            $portraitBaz
        ]);
        
        $this->assertCount(1, $this->getAllFilesForSlug('bar'));
        $this->assertCount(1, $this->getAllFilesForSlug('baz'));
	}

    /** @test */
	public function it_writes_crops_manifest()
	{
        app(UsedImagesFinder::class)
            ->whitelist()
            ->set([ 'foo.jpg' ]);

        $this->assertCount(0, $this->getLoadedManifest());
        
        $instance = new CreateImageFormatsProcessor;
		$instance->process(new UsedImagesProvider);
        
        $cropsManifest = $this->getLoadedManifest();
        $this->assertCount(1, $cropsManifest);
        $this->assertTrue(
            $cropsManifest->has(
                vfsStream::url('folio/contents/images/foo.jpg')
            )
        );
	}

    /** @test */
	public function it_creates_crops_for_all_pictures_in_a_specified_folder()
	{
        $this->bootstrapPublishedProject('bar');
        $this->bootstrapPublishedProject('baz');
        
        $this->assertCount(0, $this->getLoadedManifest());
        
        $instance = new CreateImageFormatsProcessor;
        $instance->process(
            new UsedImagesProvider(
                vfsStream::url('folio/contents/images/baz')
            )
        );
		
        $cropsManifest = $this->getLoadedManifest();
        $this->assertCount(1, $cropsManifest);
        $this->assertTrue($cropsManifest->has(
            vfsStream::url('folio/contents/images/baz/baz.jpg')
        ));

        $this->assertCount(0, $this->getAllFilesForSlug('bar'));
        $this->assertCount(1, $this->getAllFilesForSlug('baz'));
	}

    /** @test */
	public function it_creates_crops_for_all_pictures_using_a_map()
	{
        $this->bootstrapPublishedProject('bar');
        $this->bootstrapPublishedProject('baz');
        
        $this->assertCount(0, $this->getLoadedManifest());

        $map = app(CroppedImagesMap::class);
        $map->skip(
            vfsStream::url('folio/contents/images/bar/bar.jpg')
        );
        
        $instance = new CreateImageFormatsProcessor;
        $instance->process(
            new UsedImagesProvider()
        );
		
        $cropsManifest = $this->getLoadedManifest();
        
        $this->assertCount(1, $cropsManifest);
        $this->assertTrue($cropsManifest->has(
            vfsStream::url('folio/contents/images/baz/baz.jpg')
        ));

        $this->assertCount(0, $this->getAllFilesForSlug('bar'));
        $this->assertCount(1, $this->getAllFilesForSlug('baz'));
	}

    private function getLoadedManifest()
    {
        return ManifestFactory::croppedImages()->load();
    }

    private function emptyFilesToVerifyChanges(array $crops)
    {
        foreach ($crops as $crop) {
            $this->assertNotEquals('', file_get_contents($crop));
            file_put_contents($crop, '');
        }
    }

    private function assertFilesAreNotChanged(array $crops)
    {
        foreach ($crops as $crop) {
            $this->assertFileExists($crop);
            $this->assertEquals('', file_get_contents($crop));
        }
    }

    private function bootstrapPublishedProjectWithoutImages($slug)
    {
        return $this->bootstrapPublishedProject(
            $slug, 
            ['markdown' => '']
        );
    }

    private function bootstrapPublishedProject($slug, array $override = [])
    {
        $this->createImageFolderForSlug($slug);
        
        $this->createDummyImage(
            $this->getSourceImage($slug)
        );

        $projectData = [
            'slug' => $slug, 
            'markdown' => "![{$slug}]"
        ];

        $projectData = array_merge($projectData, $override);
        
        $this->createPublishedProjectFile($projectData);
        
        mkdir( vfsStream::url("folio/storage/images/{$slug}") );
    }

    private function getSourceImage($slug)
    {
        return vfsStream::url("folio/contents/images/{$slug}/{$slug}.jpg");
    }

    private function getAllFilesForSlug($slug)
    {
        return $this->getAllFiles(
            vfsStream::url("folio/storage/images/{$slug}")
        );
    }

    private function getAllFiles($path = null)
    {
        $path = $path ?: vfsStream::url("folio/storage/images");
        return array_diff(
            scandir($path),
            ['.', '..']
        );
    }
}