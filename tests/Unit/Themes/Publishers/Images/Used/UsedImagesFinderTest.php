<?php

namespace Tests\Unit\Themes\Publishers\Images\Used;

use App\Testing\InteractsWithFileSystem;
use App\Testing\InteractsWithImage;
use App\Testing\InteractsWithMarkdownFiles;
use App\Themes\Publishers\Images\Used\UsedImagesFinder;
use App\Themes\Publishers\Manifests\Manifest;
use App\Themes\Publishers\Images\Options\Options;
use App\Themes\Publishers\Images\Output\OutputFactory;
use App\Themes\Publishers\Images\Testing\TestImage;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class UsedImagesFinderTest extends TestCase
{
	use InteractsWithFileSystem;
    use InteractsWithImage;
    use InteractsWithMarkdownFiles;
	
	/** @test */
	public function it_can_be_instantiated()
	{
		$instance = new UsedImagesFinder;

	    $this->assertNotNull($instance);
	}

    /** @test */
	public function it_publishes_jpg_image_only_if_used_by_project()
	{
        $this->createImageFolderForSlug('foo');
        $this->createDummyImage(vfsStream::url('folio/contents/images/foo/foo.jpg'));
	    $this->createDummyImage(vfsStream::url('folio/contents/images/foo/bar.jpg'));
		$this->createDummyImage(vfsStream::url('folio/contents/images/foo/baz.png'));

        $this->createProjectFile(
            ['published_at' => 'yesterday'], 
            '![foo] ![baz]'
        );

	    $instance = new UsedImagesFinder;

	    $this->assertTrue($instance->isUsed(vfsStream::url('folio/contents/images/foo/foo.jpg')));
        $this->assertFalse($instance->isUsed(vfsStream::url('folio/contents/images/foo/bar.jpg')));
        $this->assertTrue($instance->isUsed(vfsStream::url('folio/contents/images/foo/baz.png')));
	}

    /** @test */
	public function it_publishes_jpg_image_only_if_used_by_project_only_if_visible()
	{
        $this->createImageFolderForSlug('foo');
        $this->createDummyImage(vfsStream::url('folio/contents/images/foo/foo.jpg'));
	    $this->createDummyImage(vfsStream::url('folio/contents/images/foo/bar.jpg'));
		$this->createDummyImage(vfsStream::url('folio/contents/images/foo/baz.png'));

        $this->createProjectFile(
            ['published_at' => 'tomorrow'], 
            '![foo] ![baz]'
        );

	    $instance = new UsedImagesFinder;

	    $this->assertFalse($instance->isUsed(vfsStream::url('folio/contents/images/foo/foo.jpg')));
        $this->assertFalse($instance->isUsed(vfsStream::url('folio/contents/images/foo/bar.jpg')));
        $this->assertFalse($instance->isUsed(vfsStream::url('folio/contents/images/foo/baz.png')));
	}

    /** @test */
	public function it_publishes_jpg_image_only_if_used_by_project_metadata()
	{
        $this->createImageFolderForSlug('foo');
        $this->createDummyImage($preview = vfsStream::url('folio/contents/images/foo/preview.jpg'));
        $this->createDummyImage($hero = vfsStream::url('folio/contents/images/foo/hero.jpg'));

        $this->createProjectFile(
            ['published_at' => 'yesterday'], 
            '![foo] ![baz]'
        );

	    $instance = new UsedImagesFinder;

        $this->assertTrue($instance->isUsed($preview));
        $this->assertTrue($instance->isUsed($hero));
	}

    /** @test */
	public function it_publishes_jpg_image_only_if_used_by_project_metadata_format()
	{
        $this->createImageFolderForSlug('foo');
        $this->createDummyImage(
            $preview = vfsStream::url('folio/contents/images/foo/preview.jpg')
        );
        $this->createProjectFile(
            ['published_at' => 'yesterday'], 
            '![foo] ![baz]'
        );

        $this->publishImages();

        $previewPortrait = vfsStream::url('folio/storage/images/foo/preview-portrait.jpg');

	    $instance = new UsedImagesFinder;

        $this->assertTrue($instance->isUsed($preview));
        $this->assertTrue($instance->isUsed($previewPortrait));
	}

    /** @test */
	public function it_publishes_jpg_image_only_if_used_by_project_metadata_only_if_visible()
	{
        $this->createImageFolderForSlug('foo');
        $this->createDummyImage($preview = vfsStream::url('folio/contents/images/foo/preview.jpg'));
        $this->createDummyImage($hero = vfsStream::url('folio/contents/images/foo/hero.jpg'));

        $this->createProjectFile(
            ['published_at' => 'tomorrow'], 
            '![foo] ![baz]'
        );

	    $instance = new UsedImagesFinder;

        $this->assertFalse($instance->isUsed($preview));
        $this->assertFalse($instance->isUsed($hero));
	}

	/** @test */
	public function it_publishes_jpg_image_only_if_used_by_page()
	{
        $this->createImageFolderForSlug('foo');
        $this->createDummyImage(vfsStream::url('folio/contents/images/foo/foo.jpg'));
	    $this->createDummyImage(vfsStream::url('folio/contents/images/foo/bar.jpg'));
		$this->createDummyImage(vfsStream::url('folio/contents/images/foo/baz.png'));

        $this->createPageFile(
            ['published_at' => 'yesterday'], 
            '![bar]'
        );
        
	    $instance = new UsedImagesFinder;

		$this->assertFalse($instance->isUsed(vfsStream::url('folio/contents/images/foo/foo.jpg')));
        $this->assertTrue($instance->isUsed(vfsStream::url('folio/contents/images/foo/bar.jpg')));
        $this->assertFalse($instance->isUsed(vfsStream::url('folio/contents/images/foo/baz.png')));
	}

    /** @test */
	public function it_publishes_jpg_image_only_if_used_by_page_metadata()
	{
        $this->createImageFolderForSlug('foo');
        $this->createDummyImage(vfsStream::url('folio/contents/images/foo/preview.jpg'));
        $this->createDummyImage(vfsStream::url('folio/contents/images/foo/hero.jpg'));

        $this->createPageFile(
            ['published_at' => 'yesterday'], 
            '![bar]'
        );

	    $instance = new UsedImagesFinder;

        $this->assertTrue($instance->isUsed(vfsStream::url('folio/contents/images/foo/preview.jpg')));
        $this->assertTrue($instance->isUsed(vfsStream::url('folio/contents/images/foo/hero.jpg')));
	}

    /** @test */
	public function it_handles_when_the_post_does_not_have_a_preview()
	{
        $this->createImageFolderForSlug('foo');
        $this->assertFileDoesNotExist(vfsStream::url('folio/contents/images/foo/preview.jpg'));

        $this->createProjectFile(['published_at' => 'yesterday']);

        $instance = new UsedImagesFinder;

        $this->assertFalse($instance->isUsed(vfsStream::url('folio/contents/images/foo/preview.jpg')));
	}

    /** @test */
	public function it_handles_when_the_post_does_not_have_a_hero()
	{
        $this->createImageFolderForSlug('foo');
        $this->assertFileDoesNotExist(vfsStream::url('folio/contents/images/foo/hero.jpg'));

        $this->createProjectFile(['published_at' => 'yesterday']);

        $instance = new UsedImagesFinder;

        $this->assertFalse($instance->isUsed(vfsStream::url('folio/contents/images/foo/hero.jpg')));
	}

    /** @test */
	public function it_handles_when_the_post_specifies_a_preview()
	{
        $this->createImageFolderForSlug('foo');
        $this->createDummyImage(vfsStream::url('folio/contents/images/foo/custom-preview.jpg'));

        $this->createProjectFile([
            'published_at' => 'yesterday', 
            'preview' => 'foo/custom-preview.jpg'
        ]);

        $instance = new UsedImagesFinder;

        $this->assertTrue($instance->isUsed(vfsStream::url('folio/contents/images/foo/custom-preview.jpg')));
	}

    /** @test */
	public function it_handles_when_the_post_specifies_a_hero()
	{
        $this->createImageFolderForSlug('foo');
        $this->createDummyImage($customHero = vfsStream::url('folio/contents/images/foo/custom-hero.jpg'));

        $this->createProjectFile([
            'published_at' => 'yesterday', 
            'hero' => 'foo/custom-hero.jpg'
        ]);

        $instance = new UsedImagesFinder;

        $this->assertTrue($instance->isUsed($customHero));
	}

    /** @test */
	public function it_allows_to_specify_a_whitelist_for_a_folder()
	{
        $this->createImageFolderForSlug('_icons');
        $this->createDummyImage($icon = vfsStream::url('folio/contents/images/_icons/my-icon.jpg'));
        
        $this->createImageFolderForSlug('foo');
        $this->createDummyImage($image = vfsStream::url('folio/contents/images/foo/foo.jpg'));
        
        $instance = new UsedImagesFinder();

        $this->assertFalse($instance->isUsed($icon));
        $this->assertFalse($instance->isUsed($image));
        
        $instance->whitelist()->set(['_icons']);
        
        $this->assertTrue($instance->isUsed($icon));
        $this->assertFalse($instance->isUsed($image));
	}

    /** @test */
	public function it_allows_to_specify_a_whitelist_for_a_folder_avoiding_duplicates()
	{
        $this->createImageFolderForSlug('_icons');
        $this->createDummyImage($icon = vfsStream::url('folio/contents/images/_icons/my-icon.jpg'));
        
        $this->createImageFolderForSlug('foo');
        $this->createImageFolderForSlug('foo/_icons');
        $this->createDummyImage($image = vfsStream::url('folio/contents/images/foo/_icons/foo.jpg'));
        
        $instance = new UsedImagesFinder();

        $this->assertFalse($instance->isUsed($icon));
        $this->assertFalse($instance->isUsed($image));
        
        $instance->whitelist()->set(['_icons']);
        
        $this->assertTrue($instance->isUsed($icon));
        $this->assertFalse($instance->isUsed($image));
	}

    /** @test */
	public function it_allows_to_specify_a_whitelist_for_a_file()
	{
        $this->createImageFolderForSlug('_icons');
        $this->createDummyImage($icon = vfsStream::url('folio/contents/images/_icons/my-icon.jpg'));
        $this->createDummyImage($anotherIcon = vfsStream::url('folio/contents/images/_icons/another-icon.jpg'));
        
        $this->createImageFolderForSlug('foo');
        $this->createDummyImage($image = vfsStream::url('folio/contents/images/foo/foo.jpg'));
        
        $instance = new UsedImagesFinder();

        $this->assertFalse($instance->isUsed($icon));
        $this->assertFalse($instance->isUsed($anotherIcon));
        $this->assertFalse($instance->isUsed($image));
        
        $instance->whitelist()->set(['_icons/my-icon.jpg']);
        
        $this->assertTrue($instance->isUsed($icon));
        $this->assertFalse($instance->isUsed($anotherIcon));
        $this->assertFalse($instance->isUsed($image));
	}

    /** @test */
	public function it_allows_to_specify_a_whitelist_for_a_folder_and_its_subfolders()
	{
        $this->createImageFolderForSlug('_icons');
        $this->createDummyImage($icon = vfsStream::url('folio/contents/images/_icons/my-icon.jpg'));
        $this->createImageFolderForSlug('_icons/subfolder');
        $this->createDummyImage($iconInSubfolder = vfsStream::url('folio/contents/images/_icons/subfolder/another-icon.jpg'));

        $this->createImageFolderForSlug('foo');
        $this->createDummyImage($image = vfsStream::url('folio/contents/images/foo/foo.jpg'));
        
        $instance = new UsedImagesFinder();

        $this->assertFalse($instance->isUsed($icon));
        $this->assertFalse($instance->isUsed($iconInSubfolder));
        $this->assertFalse($instance->isUsed($image));
        
        $instance->whitelist()->set(['_icons']);
        
        $this->assertTrue($instance->isUsed($icon));
        $this->assertTrue($instance->isUsed($iconInSubfolder));
        $this->assertFalse($instance->isUsed($image));
	}
}