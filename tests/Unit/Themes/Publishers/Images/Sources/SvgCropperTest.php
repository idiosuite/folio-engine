<?php

namespace Tests\Unit\Themes\Publishers\Images\Sources;

use Tests\TestCase;
use org\bovigo\vfs\vfsStream;
use App\Testing\InteractsWithImage;
use App\Testing\InteractsWithFileSystem;
use App\Themes\Publishers\Images\Crops\CropsImage;
use App\Themes\Publishers\Images\Sources\SvgCropper;

class SvgCropperTest extends TestCase
{
	use InteractsWithFileSystem;
    use InteractsWithImage;
    
    /** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new SvgCropper);
	}

    /** @test */
	public function it_implements_crops_image_interface()
	{
	    $this->assertInstanceOf(CropsImage::class, new SvgCropper);
	}

    /** @test */
	public function it_writes_cropped_image()
	{
        $sourceSvg = vfsStream::url('folio/contents/images/foo.svg');
        $destinationSvg = vfsStream::url('folio/public/images/foo.svg');

        $this->createDummySvg($sourceSvg);
        
        $this->assertFileDoesNotExist($destinationSvg);
        
	    (new SvgCropper)->writeCroppedImage(
            $sourceSvg, 
            $destinationSvg, 
            $this->getCropRectangle()
        );

        $this->assertFileExists($destinationSvg);
	}

    /** @test */
	public function it_replaces_viewbox_attribute()
	{
        $sourceSvg = vfsStream::url('folio/contents/images/foo.svg');
        $destinationSvg = vfsStream::url('folio/public/images/foo.svg');

        $this->createDummySvg($sourceSvg);
        
        $this->assertStringContainsString(
            'viewBox="0 0 1920 1080"', 
            file_get_contents($sourceSvg)
        );
        
	    (new SvgCropper)->writeCroppedImage(
            $sourceSvg, 
            $destinationSvg, 
            $this->getCropRectangle()
        );

        $this->assertStringContainsString(
            'viewBox="528 0 864 1080"', 
            file_get_contents($destinationSvg)
        );
	}

    /** @test */
	public function it_replaces_width_attribute()
	{
        $sourceSvg = vfsStream::url('folio/contents/images/foo.svg');
        $destinationSvg = vfsStream::url('folio/public/images/foo.svg');

        $this->createDummySvg($sourceSvg);
        
        $this->assertStringContainsString(
            'width="1920" height="1080"', 
            file_get_contents($sourceSvg)
        );
        
	    (new SvgCropper)->writeCroppedImage(
            $sourceSvg, 
            $destinationSvg, 
            $this->getCropRectangle()
        );

        $this->assertStringContainsString(
            'width="864" height="1080"', 
            file_get_contents($destinationSvg)
        );
	}

    /** @test */
	public function it_replaces_height_attribute()
	{
        $sourceSvg = vfsStream::url('folio/contents/images/foo.svg');
        $destinationSvg = vfsStream::url('folio/public/images/foo.svg');

        $this->createDummySvg($sourceSvg);
        
        $this->assertStringContainsString(
            'width="1920" height="1080"', 
            file_get_contents($sourceSvg)
        );
        
	    (new SvgCropper)->writeCroppedImage(
            $sourceSvg, 
            $destinationSvg, 
            $this->getCropRectangle(['height' => 864])
        );

        $this->assertStringContainsString(
            'width="864" height="864"', 
            file_get_contents($destinationSvg)
        );
	}

    /** @test */
	public function it_replaces_size_attributes_when_inverted()
	{
        $sourceSvg = vfsStream::url('folio/contents/images/foo.svg');
        $destinationSvg = vfsStream::url('folio/public/images/foo.svg');

        $this->createDummySvg($sourceSvg);
        $this->invertSizeAttributes($sourceSvg);

        $this->assertStringContainsString(
            'height="1080" width="1920"', 
            file_get_contents($sourceSvg)
        );
        
	    (new SvgCropper)->writeCroppedImage(
            $sourceSvg, 
            $destinationSvg, 
            $this->getCropRectangle()
        );

        $this->assertStringContainsString(
            'height="1080" width="864"', 
            file_get_contents($destinationSvg)
        );
	}

    /** @test */
	public function it_affects_only_svg_tag_attributes()
	{
        $sourceSvg = vfsStream::url('folio/contents/images/foo.svg');
        $destinationSvg = vfsStream::url('folio/public/images/foo.svg');

        $this->createDummySvg($sourceSvg);
        
        $this->assertStringContainsString(
            '<rect class="cls-1" width="1920" height="1080"/>', 
            file_get_contents($sourceSvg)
        );
        
	    (new SvgCropper)->writeCroppedImage(
            $sourceSvg, 
            $destinationSvg, 
            $this->getCropRectangle()
        );

        $this->assertStringContainsString(
            '<rect class="cls-1" width="1920" height="1080"/>', 
            file_get_contents($destinationSvg)
        );
	}

    /** @test */
	public function it_does_not_fail_if_width_attribute_is_not_present()
	{
        $sourceSvg = vfsStream::url('folio/contents/images/foo.svg');
        $destinationSvg = vfsStream::url('folio/public/images/foo.svg');

        $this->createDummySvg($sourceSvg);

        file_put_contents(
            $sourceSvg,
            str_replace(
                'width="1920" height="1080" viewBox="0 0 1920 1080"',
                'height="1080" viewBox="0 0 1920 1080"',
                file_get_contents($sourceSvg)
            )
        );
        
        (new SvgCropper)->writeCroppedImage(
            $sourceSvg, 
            $destinationSvg, 
            $this->getCropRectangle()
        );

        $this->assertStringContainsString(
            'width="864"',
            explode('>', file_get_contents($destinationSvg))[1]
        );
	}

    /** @test */
	public function it_does_not_fail_if_height_attribute_is_not_present()
	{
        $sourceSvg = vfsStream::url('folio/contents/images/foo.svg');
        $destinationSvg = vfsStream::url('folio/public/images/foo.svg');

        $this->createDummySvg($sourceSvg);

        file_put_contents(
            $sourceSvg,
            str_replace(
                'width="1920" height="1080" viewBox="0 0 1920 1080"',
                'width="1920" viewBox="0 0 1920 1080"',
                file_get_contents($sourceSvg)
            )
        );
        
        (new SvgCropper)->writeCroppedImage(
            $sourceSvg, 
            $destinationSvg, 
            $this->getCropRectangle()
        );

        $this->assertStringContainsString(
            'height="1080"',
            explode('>', file_get_contents($destinationSvg))[1]
        );
	}

    /** @test */
	public function it_fails_if_viewbox_attribute_is_not_present()
	{
        $sourceSvg = vfsStream::url('folio/contents/images/foo.svg');
        $destinationSvg = vfsStream::url('folio/public/images/foo.svg');

        $this->createDummySvg($sourceSvg);

        file_put_contents(
            $sourceSvg,
            str_replace(
                'width="1920" height="1080" viewBox="0 0 1920 1080"',
                'width="1920" height="1080"',
                file_get_contents($sourceSvg)
            )
        );

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('<svg> tag requires attribute viewBox');
        
        (new SvgCropper)->writeCroppedImage(
            $sourceSvg, 
            $destinationSvg, 
            $this->getCropRectangle()
        );
	}

    /** @test */
	public function it_finds_svg_tag_multiple_lines()
	{
        $svg = '<?xml version="1.0" encoding="utf-8"?>
<!-- Generator: Adobe Illustrator 21.1.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" 
        viewBox="0 0 1920 1080" style="enable-background:new 0 0 1920 1080;" xml:space="preserve">
</svg>';

        $this->createImagesDestinationFolder();

        $sourceSvg = vfsStream::url('folio/contents/images/foo.svg');
        $destinationSvg = vfsStream::url('folio/public/images/foo.svg');

        file_put_contents($sourceSvg, $svg);

        (new SvgCropper)->writeCroppedImage(
            $sourceSvg, 
            $destinationSvg, 
            $this->getCropRectangle()
        );

        $result = file_get_contents($destinationSvg);

        $this->assertStringContainsString('width="864"', $result);
        $this->assertStringContainsString('height="1080"', $result);
        $this->assertStringContainsString('viewBox="528 0 864 1080"', $result);
	}

    /** @test */
	public function it_finds_svg_tag_one_line()
	{
        $svg = '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1920 1080" style="enable-background:new 0 0 1920 1080;" xml:space="preserve"></svg>';

        $this->createImagesDestinationFolder();
        
        $sourceSvg = vfsStream::url('folio/contents/images/foo.svg');
        $destinationSvg = vfsStream::url('folio/public/images/foo.svg');

        file_put_contents($sourceSvg, $svg);

        (new SvgCropper)->writeCroppedImage(
            $sourceSvg, 
            $destinationSvg, 
            $this->getCropRectangle()
        );

        $result = file_get_contents($destinationSvg);

        $this->assertStringContainsString('width="864"', $result);
        $this->assertStringContainsString('height="1080"', $result);
        $this->assertStringContainsString('viewBox="528 0 864 1080"', $result);
	}

    private function getCropRectangle(array $override = [])
    {
        return array_merge([
			'width' => 864, 
			'height' => 1080,
            'x' => (1920 - 864) / 2, 
			'y' => 0, 
		], $override);
    }

    private function createDummySvg($sourceSvg)
    {
        $this->createImagesDestinationFolder();

        file_put_contents(
            $sourceSvg, 
            '<svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" width="1920" height="1080" viewBox="0 0 1920 1080">
                <rect class="cls-1" width="1920" height="1080"/>
            </svg>'
        );
    }

    private function invertSizeAttributes($sourceSvg)
    {
        file_put_contents(
            $sourceSvg,
            str_replace(
                'width="1920" height="1080"', 
                'height="1080" width="1920"', 
                file_get_contents($sourceSvg)
            )
        );
    }

    private function createImagesDestinationFolder()
    {
        mkdir($this->baseDestinationPathImages);
    }
}