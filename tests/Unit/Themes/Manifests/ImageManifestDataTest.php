<?php

namespace Tests\Unit\Themes\Manifests;

use App\Themes\Publishers\Manifests\ImageManifestData;
use App\Testing\InteractsWithFileSystem;
use App\Testing\InteractsWithImage;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class ImageManifestDataTest extends TestCase
{
    use InteractsWithFileSystem;
    use InteractsWithImage;

	/** @test */
    public function it_can_be_instantiated()
    {
        $this->assertNotNull(new ImageManifestData('path/to/image.jpg'));
    }

    /** @test */
    public function it_can_be_instantiated_from_file_path()
    {
        $this->assertInstanceOf(
            ImageManifestData::class, 
            ImageManifestData::fromFilePath('path/to/image.jpg')
        );
    }

    /** @test */
    public function it_can_be_instantiated_from_array()
    {
        $this->assertInstanceOf(
            ImageManifestData::class, 
            ImageManifestData::fromArray('path/to/image.jpg', [])
        );
    }

    /** @test */
    public function it_returns_manifest_data_for_not_existing_image()
    {
        $result = ImageManifestData::fromFilePath('path/to/image.jpg')->get();

        $this->assertIsArray($result);

        $this->assertArrayHasKey('checksum', $result);
        $this->assertEquals('', $result['checksum']);
        
        $this->assertArrayHasKey('bytes', $result);
        $this->assertEquals(0, $result['bytes']);
        
        $this->assertArrayHasKey('width', $result);
        $this->assertEquals(0, $result['width']);
        
        $this->assertArrayHasKey('height', $result);
        $this->assertEquals(0, $result['height']);
        
        $this->assertArrayHasKey('path', $result);
        $this->assertEquals('path/to/image.jpg', $result['path']);
        
        $this->assertArrayHasKey('format', $result);
        $this->assertEquals('jpg', $result['format']);
    }

    /** @test */
    public function it_returns_format_based_on_file_extension()
    {
        $jpg = ImageManifestData::fromFilePath('path/to/image.jpg')->get();
        $png = ImageManifestData::fromFilePath('path/to/image.png')->get();
        $gif = ImageManifestData::fromFilePath('path/to/image.gif')->get();
        $webp = ImageManifestData::fromFilePath('path/to/image.webp')->get();
        $json = ImageManifestData::fromFilePath('path/to/image.json')->get();

        $this->assertEquals('jpg', $jpg['format']);
        $this->assertEquals('png', $png['format']);
        $this->assertEquals('gif', $gif['format']);
        $this->assertEquals('webp', $webp['format']);
        $this->assertEquals('json', $json['format']);
    }

    /** @test */
    public function it_returns_file_size_for_not_existing_file()
    {
        $notExisting = ImageManifestData::fromFilePath('path/to/image.jpg')->get();
        
        $this->assertEquals(0, $notExisting['bytes']);
    }

    /** @test */
    public function it_returns_file_size_for_existing_image_file()
    {
        $this->createDummyImage($existingImagePath = vfsStream::url('folio/public/foo.jpg'));

        $existing = ImageManifestData::fromFilePath($existingImagePath)->get();
        
        $this->assertNotEquals(0, $existing['bytes']);
        $this->assertEquals(
            filesize($existingImagePath), 
            $existing['bytes']
        );
    }

    /** @test */
    public function it_returns_file_size_for_existing_file()
    {
        file_put_contents($existingFilePath = vfsStream::url('folio/public/foo.txt'), 'foobar');

        $existing = ImageManifestData::fromFilePath($existingFilePath)->get();
        
        $this->assertNotEquals(0, $existing['bytes']);
        $this->assertEquals(
            filesize($existingFilePath), 
            $existing['bytes']
        );
    }

    /** @test */
    public function it_returns_image_size_for_not_existing_image()
    {
        $notExisting = ImageManifestData::fromFilePath('path/to/image.jpg')->get();

        $this->assertEquals(0, $notExisting['width']);
        $this->assertEquals(0, $notExisting['height']);
    }

    /** @test */
    public function it_returns_image_size_for_image_file()
    {
        $this->createDummyImage($existingImagePath = vfsStream::url('folio/public/foo.jpg'));
        $existing = ImageManifestData::fromFilePath($existingImagePath)->get();

        $this->assertEquals(50, $existing['width']);
        $this->assertEquals(50, $existing['height']);
    }

    /** @test */
    public function it_returns_image_size_for_svg_image()
    {
        $this->createDummyImage($svgImagePath = vfsStream::url('folio/public/foo.svg'));
        $existing = ImageManifestData::fromFilePath($svgImagePath)->get();

        $this->assertEquals(50, $existing['width']);
        $this->assertEquals(50, $existing['height']);
    }

    /** @test */
    public function it_returns_checksum_for_existing_file()
    {
        $this->createDummyImage($imagePath = vfsStream::url('folio/public/foo.jpg'));
        $existing = ImageManifestData::fromFilePath($imagePath)->get();

        $this->assertEquals('8c514319ff22dbe396596327f540133e', $existing['checksum']);
    }

    /** @test */
    public function it_returns_checksum_for_not_existing_file()
    {
        $imagePath = vfsStream::url('folio/public/foo.jpg');
        $notExisting = ImageManifestData::fromFilePath($imagePath)->get();

        $this->assertEquals('', $notExisting['checksum']);
    }

    /** @test */
    public function it_returns_checksum_from_array_if_set()
    {
        $data = ['checksum' => 'foobar'];

        $imagePath = vfsStream::url('folio/public/foo.jpg');
        
        $instance = ImageManifestData::fromArray($imagePath, $data);

        $this->assertEquals('foobar', $instance->getChecksum());
    }

    /** @test */
    public function it_returns_manifest_data_provided()
    {
        $expected = ['checksum' => 'foobar'];

        $result = ImageManifestData::fromArray('path/to/image.jpg', $expected)->get();

        $this->assertIsArray($result);

        $this->assertArrayHasKey('checksum', $result);
        $this->assertEquals('foobar', $result['checksum']);
        
        $this->assertArrayHasKey('bytes', $result);
        $this->assertEquals(0, $result['bytes']);
        
        $this->assertArrayHasKey('width', $result);
        $this->assertEquals(0, $result['width']);
        
        $this->assertArrayHasKey('height', $result);
        $this->assertEquals(0, $result['height']);
        
        $this->assertArrayHasKey('path', $result);
        $this->assertEquals('path/to/image.jpg', $result['path']);
        
        $this->assertArrayHasKey('format', $result);
        $this->assertEquals('jpg', $result['format']);
    }
}