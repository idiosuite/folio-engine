<?php

namespace App\Themes;

use App\App;
use App\Context;
use App\Domain\Post;
use App\Portfolio;
use App\Themes\Theme;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class ThemeTest extends TestCase
{
	private $root;
	private $basePath;
	
	/** @before */
	public function setUpVfsStream()
	{
		$structure = [
			'contents' => [
				'pages' => [],
				'projects' => [],
				'preferences' => [
					'defaults.json' => '[]'
				],
				'settings.json' => json_encode([
					'theme' => 'test',
					'foo' => 'Foo',
					'bar' => 'Bar',
					'baz' => 'Baz',
				])
			],
			'theme' => [
				'resources' => [
					'views' => [
						'home.blade.php' => 'Home',
						'404.blade.php' => 'Not found',
						'project.blade.php' => 'Project',
						'page.blade.php' => 'Page',
						'about.blade.php' => 'Qualified page',
						'special.blade.php' => 'Qualified project',
						'settings.blade.php' => '{{ $settings->theme }} {{ $settings->bar }}'
					]
				]
			],
		];

		$this->root = vfsStream::setup('folio', null, $structure);
		$this->basePath = vfsStream::url('folio');
	}

	/** @before */
	public function setUpThemes()
	{
		App::bootstrapThemes([
			'test' => \Folio\Themes\Test\ThemeProvider::class,
		]);
	}

	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(
	    	new Theme(
	    		new Portfolio($this->basePath),
	    		new Context
	    	)
	    );
	}

	/** @test */
	public function it_returns_home_markup()
	{
	    $instance = new Theme(
    		new Portfolio($this->basePath),
    		new Context
    	);

    	$this->assertEquals('Home', $instance->getHomeResponse());
	}

	/** @test */
	public function it_returns_not_found_markup()
	{
	    $instance = new Theme(
    		new Portfolio($this->basePath),
    		new Context
    	);

    	$this->assertEquals('Not found', $instance->getNotFoundResponse());
	}

	/** @test */
	public function it_returns_project_markup()
	{
	    $instance = new Theme(
    		new Portfolio($this->basePath),
    		new Context
    	);

    	$post = new Post(['layout' => 'project']);

    	$this->assertEquals(
    		'Project', 
    		$instance->getPostResponse($post)
    	);
	}

	/** @test */
	public function it_returns_page_markup()
	{
	    $instance = new Theme(
    		new Portfolio($this->basePath),
    		new Context
    	);

    	$post = new Post(['layout' => 'page']);

    	$this->assertEquals(
    		'Page', 
    		$instance->getPostResponse($post)
    	);
	}

	/** @test */
	public function it_returns_qualified_project_markup()
	{
	    $instance = new Theme(
    		new Portfolio($this->basePath),
    		new Context
    	);

    	$post = new Post(['layout' => 'special']);

    	$this->assertEquals(
    		'Qualified project', 
    		$instance->getPostResponse($post)
    	);
	}

	/** @test */
	public function it_returns_qualified_page_markup()
	{
	    $instance = new Theme(
    		new Portfolio($this->basePath),
    		new Context
    	);

    	$post = new Post(['layout' => 'about']);

    	$this->assertEquals(
    		'Qualified page', 
    		$instance->getPostResponse($post)
    	);
	}

	/** @test */
	public function it_provides_settings_object()
	{
	    $instance = new Theme(
    		new Portfolio($this->basePath),
    		new Context
    	);

    	$post = new Post(['layout' => 'settings']);

    	$this->assertEquals(
    		'test Bar', 
    		$instance->getPostResponse($post)
    	);
	}
}