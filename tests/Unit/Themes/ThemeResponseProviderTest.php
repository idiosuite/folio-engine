<?php

namespace Tests\Unit\Themes;

use App\Domain\Post;
use App\Portfolio;
use App\Themes\ThemeResponseProvider;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class ThemeResponseProviderTest extends TestCase
{
	private $root;
	private $basePath;

	/** @before */
	public function setUpVfsStream()
	{
		$structure = [
			'contents' => [
				'pages' => [
					'bar.md' => '---
layout: page
title: Bar
---
Barbaz'
				],
				'projects' => [
					'foo.md' => '---
layout: project
title: Foo
---
Foobar'
				],
				'preferences' => [
					'defaults.json' => '[]'
				],
				'settings.json' => json_encode(['theme' => 'test']),
			],
			'theme' => [
				'resources' => [
					'views' => [
						'home.blade.php' => 'Homepage',
						'project.blade.php' => 'Project',
						'page.blade.php' => 'Page',
						'404.blade.php' => 'Not Found',
					]
				]
			],
			'public' => []
		];

		$this->root = vfsStream::setup('folio', null, $structure);
		$this->basePath = vfsStream::url('folio');
	}
	
	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new ThemeResponseProvider(new Portfolio($this->basePath)));
	}

	/** @test */
	public function it_renders_homepage_layout()
	{
	    $portfolioGenerator = new ThemeResponseProvider(
	    	new Portfolio($this->basePath)
	    );

	    $this->assertEquals('Homepage', $portfolioGenerator->homepage());
	}

	/** @test */
	public function it_renders_project_layout()
	{
	    $portfolioGenerator = new ThemeResponseProvider(
	    	new Portfolio($this->basePath)
	    );

	    $this->assertEquals('Project', $portfolioGenerator->project('foo'));
	}

	/** @test */
	public function it_renders_page_layout()
	{
	    $portfolioGenerator = new ThemeResponseProvider(
	    	new Portfolio($this->basePath)
	    );

	    $this->assertEquals('Page', $portfolioGenerator->page('bar'));
	}

	/** @test */
	public function it_renders_post_layout_if_exists()
	{
	    $portfolioGenerator = new ThemeResponseProvider(
	    	new Portfolio($this->basePath)
	    );

	    $post = new Post([
	    	'layout' => 'project', 
	    	'type' => 'project', 
	    	'slug' => 'foo',
	    	'exists' => true
	    ]);

	    $this->assertEquals('Project', $portfolioGenerator->content($post));
	}

	/** @test */
	public function it_does_not_renders_post_layout_if_specified_in_preferences()
	{
	    $portfolioGenerator = new ThemeResponseProvider(
	    	new Portfolio($this->basePath)
	    );

	    $post = new Post([
	    	'disableRender' => 'true'
	    ]);

	    $this->assertEquals('', $portfolioGenerator->content($post));
	}

	/** @test */
	public function it_renders_not_found_layout()
	{
	    $portfolioGenerator = new ThemeResponseProvider(
	    	new Portfolio($this->basePath)
	    );

	    $this->assertEquals('Not Found', $portfolioGenerator->notFound());
	}

	/** @test */
	public function it_renders_not_found_layout_if_project_does_not_exist()
	{
	    $portfolioGenerator = new ThemeResponseProvider(
	    	new Portfolio($this->basePath)
	    );

	    $post = new Post([
	    	'layout' => 'project', 
	    	'type' => 'project', 
	    	'slug' => 'does-not-exist'
	    ]);

	    $this->assertEquals('Not Found', $portfolioGenerator->project('does-not-exist'));
	    $this->assertEquals('Not Found', $portfolioGenerator->content($post));
	}

	/** @test */
	public function it_renders_not_found_layout_if_page_does_not_exist()
	{
	    $portfolioGenerator = new ThemeResponseProvider(
	    	new Portfolio($this->basePath)
	    );

	    $post = new Post([
	    	'layout' => 'page', 
	    	'type' => 'page', 
	    	'slug' => 'does-not-exist'
	    ]);

	    $this->assertEquals('Not Found', $portfolioGenerator->page('does-not-exist'));
	    $this->assertEquals('Not Found', $portfolioGenerator->content($post));
	}
}