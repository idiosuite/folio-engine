<?php

namespace Tests\Unit\Resources;

use App\Domain\ImageFactory;
use App\Domain\Images\Image;
use App\Testing\InteractsWithImage;
use App\Testing\InteractsWithFileSystem;
use App\Resources\ImagePathResource;
use App\Themes\Publishers\Images\Testing\InteractsWithManifest;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class ImagePathResourceTest extends TestCase
{
	use InteractsWithFileSystem;
	use InteractsWithImage;
	use InteractsWithManifest;

	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(ImagePathResource::hydrate([]));
	}

	/** @test */
	public function it_returns_instance_of_self()
	{
	    $this->assertInstanceOf(ImagePathResource::class, ImagePathResource::hydrate([]));
	}

	/** @test */
	public function it_can_be_converted_to_array()
	{
		$result = ImagePathResource::hydrate([])->toArray();

	    $this->assertIsArray($result);
	    $this->assertEquals([], $result);
	}

	/** @test */
	public function it_does_not_convert_non_image_value()
	{
	    $result = ImagePathResource::hydrate(['foo' => 'bar'])->toArray();

	    $this->assertIsArray($result);
	    $this->assertEquals(['foo' => 'bar'], $result);
	}

	/** @test */
	public function it_converts_image_path_to_array()
	{
	    $result = ImagePathResource::hydrate(['foo' => 'foo.jpg'])->toArray();

	    $this->assertIsArray($result);
	    $this->assertEquals([
	    	'foo' => [
	    		'src' => $this->baseUrlImages . '/foo.jpg', 
	    		'width' => 120, 
	    		'height' => 20
	    	]
	    ], $result);
	}

	/** @test */
	public function it_converts_image_path_to_array_recursively()
	{
	    $result = ImagePathResource::hydrate(['foo' => ['bar' => 'foo.jpg']])->toArray();

	    $this->assertIsArray($result);
	    $this->assertEquals([
	    	'foo' => [
	    		'bar' => [
		    		'src' => $this->baseUrlImages . '/foo.jpg', 
		    		'width' => 120, 
		    		'height' => 20
		    	]
		    ]
	    ], $result);
	}

	/** @test */
	public function it_does_not_convert_image_array_to_array()
	{
	    $result = ImagePathResource::hydrate([
	    	'foo' => [
	    		'src' => 'foo.jpg', 
	    		'width' => 60, 
	    		'height' => 30
	    	]
	    ])->toArray();

	    $this->assertIsArray($result);
	    $this->assertEquals([
	    	'foo' => [
	    		'src' => 'foo.jpg', 
	    		'width' => 60, 
	    		'height' => 30
	    	]
	    ], $result);
	}

	/** @test */
	public function it_converts_image_path_to_object()
	{
	    $result = ImagePathResource::hydrate(['foo' => 'foo.jpg'])->get();

	    $this->assertIsArray($result);
	    $this->assertArrayHasKey('foo', $result);
	    $this->assertInstanceOf(Image::class, $result['foo']);
	    $this->assertEquals(
	    	$this->baseUrlImages . '/foo.jpg', 
	    	$result['foo']->getUrl()
	    );
	}

	/** @test */
	public function it_converts_image_array_to_object()
	{
	    $result = ImagePathResource::hydrate([
	    	'foo' => [
	    		'src' => 'foo.jpg',
	    		'width' => 30,
	    		'height' => 60
	    	]
	    ])->get();

	    $this->assertIsArray($result);
	    $this->assertArrayHasKey('foo', $result);
	    $this->assertInstanceOf(Image::class, $result['foo']);
	    $this->assertEquals(
	    	$this->baseUrlImages . '/foo.jpg', 
	    	$result['foo']->getUrl()
	    );
	}

	/** @test */
	public function it_converts_image_array_with_url_to_object()
	{
		$manifest = $this->getPublishedManifestInstance();

		$result = ImagePathResource::hydrate([
	    	'foo' => [
	    		'src' => vfsStream::url('folio/contents/images/foo.jpg'),
	    		'width' => 30,
	    		'height' => 60
	    	]
	    ])->get();

	    $this->assertIsArray($result);
	    $this->assertArrayHasKey('foo', $result);
	    $this->assertInstanceOf(Image::class, $result['foo']);
	    $this->assertEquals($this->baseUrlImages . '/foo-compressed.jpg', $result['foo']->getUrl());
	}

	/** @test */
	public function it_converts_image_array_with_object_to_array()
	{
		$manifest = $this->getPublishedManifestInstance();

		$result = ImagePathResource::hydrate([
	    	'foo' => $image = ImageFactory::make('foo.jpg')
	    ])->toArray();

	    $this->assertIsArray($result);
	    $this->assertArrayHasKey('foo', $result);
	    $this->assertIsArray($result['foo']);
	    $this->assertEquals(
	    	$image->getUrl(), 
	    	$result['foo']['src']
	    );
	}

	/** @test */
	public function it_converts_image_array_with_object_to_object()
	{
		$manifest = $this->getPublishedManifestInstance();

		$result = ImagePathResource::hydrate([
	    	'foo' => $image = ImageFactory::make('foo.jpg')
	    ])->get();

	    $this->assertIsArray($result);
	    $this->assertArrayHasKey('foo', $result);
	    $this->assertInstanceOf(Image::class, $result['foo']);
	    $this->assertEquals($image->getUrl(), $result['foo']->getUrl());
	}
}