<?php

namespace Tests\Unit\Resources;

use Tests\TestCase;
use App\Domain\Preview;
use App\Testing\PreviewTest;
use org\bovigo\vfs\vfsStream;
use App\Testing\InteractsWithImage;
use App\Repositories\PostCollection;
use App\Repositories\PostsRepository;
use App\Repositories\VideoCollection;
use App\Resources\CollectionResource;
use App\Repositories\PreviewCollection;
use App\Testing\InteractsWithFileSystem;

class CollectionResourceTest extends TestCase
{
	use InteractsWithFileSystem;
	use InteractsWithImage;

	/** @test */
	public function it_can_be_instantiated_for_posts()
	{
	    $this->assertNotNull(
            new CollectionResource(
                new PostCollection(
                    $this->createMock(PostsRepository::class)
                )
            )
        );
	}

    /** @test */
	public function it_can_be_instantiated_for_previews()
	{
	    $this->assertNotNull(
            new CollectionResource(
                new PreviewCollection()
            )
        );
	}

    /** @test */
	public function it_can_be_instantiated_for_videos()
	{
	    $this->assertNotNull(
            new CollectionResource(
                new VideoCollection()
            )
        );
	}

    /** @test */
	public function it_can_be_converted_to_json_for_folio_rich_gallery()
	{
        $this->createImageFolderForSlug('foo');
        $this->createDummyImage(vfsStream::url('folio/contents/images/foo/foo.jpg'));

        $previews = new CollectionResource(
            new PreviewCollection([
                PreviewTest::previewImage('foo/foo.jpg'),
                PreviewTest::vimeoVideo()
            ])
        );

        $resource = $previews->toArray();

        $this->assertCount(2, $resource);
	}
}