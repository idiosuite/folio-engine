<?php

namespace Tests\Unit\Domain;

use App\Domain\DomainObject;
use Carbon\Carbon;
use Tests\TestCase;

class DomainObjectTest extends TestCase
{
	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new Foo);
	}

	/** @test */
	public function it_can_be_instantiated_with_attributes()
	{
	    $domainObject = new Foo(['foo' => 'bar']);

	    $this->assertEquals('bar', $domainObject->foo);
	}

	/** @test */
	public function it_can_load_attributes_after_instantiation()
	{
	    $domainObject = new Foo;
	    $this->assertNull($domainObject->foo);

	    $domainObject->load(['foo' => 'bar', 'baz' => 'foobar']);
	    $this->assertEquals('bar', $domainObject->foo);
	    $this->assertEquals('foobar', $domainObject->baz);

	    $domainObject->load(['foo' => 'foobar']);
	    $this->assertEquals('foobar', $domainObject->foo);
	    $this->assertEquals('', $domainObject->baz);

	    $this->assertSame($domainObject, $domainObject->load([]));
	}

	/** @test */
	public function it_asserts_it_has_an_attribute()
	{
	    $this->assertFalse((new Foo)->has('foo'));
	    
	    $this->assertTrue((new Foo(['foo' => 'Foobar']))->has('foo'));
	    
	    $this->assertFalse((new Foo(['foo' => false]))->has('foo'));
	    $this->assertFalse((new Foo(['foo' => '']))->has('foo'));
	    $this->assertFalse((new Foo(['foo' => null]))->has('foo'));
	}

	/** @test */
	public function it_can_be_rendered_as_array()
	{
	    $this->assertEquals(
	    	['foo' => 'bar'], 
	    	(new Foo(['foo' => 'bar']))->toArray()
	    );
	}

	/** @test */
	public function it_provides_access_to_attributes_as_property()
	{
		$this->assertNull(
	    	(new Foo)->foo
	    );

	    $this->assertEquals(
	    	'bar', 
	    	(new Foo(['foo' => 'bar']))->foo
	    );
	}

	/** @test */
	public function it_sets_an_attribute_and_returns_self()
	{
		$foo = new Foo;
		
		$this->assertNull($foo->foo);

		$this->assertSame($foo, $foo->set('foo', 'bar'));

	    $this->assertEquals('bar', $foo->foo);
	}

	/** @test */
	public function it_provides_a_way_to_massage_attributes()
	{
	    $foo = new Foo;

	    $this->assertEquals('', $foo->special);
	    $this->assertSame($foo, $foo->set('special', 'foo'));
	    $this->assertEquals('Foo', $foo->special);
	}
}

class Foo extends DomainObject
{
	public function getSpecialAttribute($value)
	{
		return ucfirst((string)$value);
	}
}