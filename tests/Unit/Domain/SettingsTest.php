<?php

namespace Tests\Unit\Domain;

use App\Testing\InteractsWithImage;
use App\Testing\InteractsWithFileSystem;
use App\Domain\Settings;
use Carbon\Carbon;
use Tests\TestCase;

class SettingsTest extends TestCase
{
	use InteractsWithFileSystem;
	use InteractsWithImage;
	
	/** @test */
	public function it_implements_provides_api_resource_interface()
	{
	    $instance = new Settings;

	    $this->assertInstanceOf(
	    	\App\Resources\ProvidesApiResource::class, 
	    	$instance
	    );

	    $this->assertInstanceOf(
	    	\App\Resources\SettingsResource::class, 
	    	$instance->apiResource()
	    );
	}

	/** @test */
	public function it_implodes_keywords_settings()
	{
	    $settings = new Settings(['keywords' => ['foo', 'bar', 'baz']]);
	    $this->assertEquals('foo, bar, baz', $settings->keywords);

	    $settings = new Settings;
	    $this->assertNull($settings->keywords);

	    $settings->load(['keywords' => ['foo', 'bar', 'baz']]);
	    $this->assertEquals('foo, bar, baz', $settings->keywords);

	    $this->assertSame($settings, $settings->load([]));
	}

	/** @test */
	public function it_hydrates_image_paths()
	{
	    $settings = new Settings([
	    	'image' => 'foo/bar/baz.jpg'
	    ]);

	    $this->assertInstanceOf(
	    	\App\Domain\Images\Image::class, 
	    	$settings->image
	    );
	}
}