<?php

namespace Tests\Unit\Domain;

use App\Domain\Video;
use App\Testing\VideoTest as VideoTestHelper;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;
use App\Testing\InteractsWithFileSystem;
use App\Testing\InteractsWithImage;

class VideoTest extends TestCase
{
    use InteractsWithFileSystem;
    
    /** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new Video);
	}

    /** @test */
	public function it_extends_domain_object()
	{
	    $this->assertInstanceOf(\App\Domain\DomainObject::class, new Video);
	}

    /** @test */
	public function it_implements_provides_api_resource_interface()
	{
	    $instance = new Video;

	    $this->assertInstanceOf(
	    	\App\Resources\ProvidesApiResource::class, 
	    	$instance
	    );

	    $this->assertInstanceOf(
	    	\App\Resources\VideoResource::class, 
	    	$instance->apiResource()
	    );
	}
}