<?php

namespace Tests\Unit\Domain;

use App\Domain\ImageFactory;
use App\Domain\Images\Image;
use App\Domain\Images\LocalImage;
use App\Domain\Images\NullImage;
use App\Domain\Images\RemoteImage;
use App\Domain\Images\ResponsiveImage;
use App\Testing\InteractsWithImage;
use App\Testing\InteractsWithFileSystem;
use App\Themes\Publishers\Images\Testing\InteractsWithManifest;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class ImageFactoryTest extends TestCase
{
	use InteractsWithFileSystem;
	use InteractsWithImage;
	use InteractsWithManifest;

	/** @test */
	public function it_instantiates_local_image_with_no_path()
	{
	    $image = ImageFactory::make('');

	    $this->assertInstanceOf(Image::class, $image);
	    $this->assertInstanceOf(LocalImage::class, $image);
	}

	/** @test */
	public function it_instantiates_local_image_with_path()
	{
	    $image = ImageFactory::make('foo.jpg');

	    $this->assertInstanceOf(Image::class, $image);
	    $this->assertInstanceOf(LocalImage::class, $image);
	}

	/** @test */
	public function it_instantiates_remote_image_with_https_url()
	{
	    $image = ImageFactory::make($url = 'https://loremflickr.com/320/240');

	    $this->assertInstanceOf(Image::class, $image);
	    $this->assertInstanceOf(RemoteImage::class, $image);
	}

	/** @test */
	public function it_instantiates_remote_image_with_http_url()
	{
	    $image = ImageFactory::make($url = 'http://loremflickr.com/320/240');

	    $this->assertInstanceOf(Image::class, $image);
	    $this->assertInstanceOf(RemoteImage::class, $image);
	}

	/** @test */
	public function it_instantiates_remote_image_with_https_url_not_existing()
	{
	    $image = ImageFactory::make($url = 'https://foo.bar/baz.jpg');

	    $this->assertInstanceOf(Image::class, $image);
	    $this->assertInstanceOf(RemoteImage::class, $image);
	}

	/** @test */
	public function it_instantiates_null_image()
	{
	    $image = ImageFactory::makeNull();

	    $this->assertInstanceOf(Image::class, $image);
	    $this->assertInstanceOf(NullImage::class, $image);
	}

	/** @test */
	public function it_instantiates_responsive_image()
	{
		$this->getPublishedManifestInstance();

	    $image = ImageFactory::makeResponsive('foo.jpg');

	    $this->assertInstanceOf(Image::class, $image);
	    $this->assertInstanceOf(ResponsiveImage::class, $image);

	    $this->assertStringContainsString('foo-compressed.webp', $image->getHtml());
	    $this->assertStringContainsString('foo-compressed.jpg', $image->getHtml());
	}
}