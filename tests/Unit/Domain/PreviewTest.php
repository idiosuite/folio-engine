<?php

namespace Tests\Unit\Domain;

use Tests\TestCase;
use App\Domain\Preview;
use org\bovigo\vfs\vfsStream;
use App\Testing\InteractsWithImage;
use App\Testing\InteractsWithFileSystem;
use Illuminate\Contracts\Support\Arrayable;
use App\Testing\PreviewTest as PreviewTestHelper;
use App\Themes\Publishers\Images\Used\UsedImagesProvider;

class PreviewTest extends TestCase
{
    use InteractsWithFileSystem;
    use InteractsWithImage;

    /** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new Preview);
	}

    /** @test */
	public function it_extends_domain_object()
	{
	    $this->assertInstanceOf(\App\Domain\DomainObject::class, new Preview);
	}

    /** @test */
	public function it_asserts_media()
	{
        $preview = new Preview(
            PreviewTestHelper::previewImage('foo/foo.jpg')
        );
        $this->assertTrue($preview->isMedia('preview'));
        $this->assertFalse($preview->isMedia('hero'));

        $hero = new Preview(
            PreviewTestHelper::heroImage('foo/foo.jpg')
        );
        $this->assertTrue($hero->isMedia('hero'));
        $this->assertFalse($hero->isMedia('preview'));
	}

    /** @test */
	public function it_asserts_type()
	{
        $preview = new Preview(
            PreviewTestHelper::previewImage('foo/foo.jpg')
        );
        $this->assertTrue($preview->isType('image'));
        $this->assertFalse($preview->isType('video'));

        $vimeoVideo = new Preview(
            PreviewTestHelper::vimeoVideo('https://www.vimeo.com/12345')
        );
        $this->assertTrue($vimeoVideo->isType('video'));
        $this->assertFalse($vimeoVideo->isType('image'));
	}

    /** @test */
	public function it_returns_resource_for_image()
	{
        $preview = new Preview(
            PreviewTestHelper::previewImage('foo/foo.jpg')
        );

        $this->assertInstanceOf(
            UsedImagesProvider::class, 
            $preview->getResource()
        );

        $this->assertInstanceOf(
            Arrayable::class, 
            $preview->getResource()
        );
	}

    /** @test */
	public function it_returns_resource_for_video()
	{
        $preview = new Preview(
            PreviewTestHelper::vimeoVideo()
        );
        
        $this->assertInstanceOf(
            Arrayable::class, 
            $preview->getResource()
        );
	}

    /** @test */
	public function it_implements_provides_api_resource_interface()
	{
	    $instance = new Preview;

	    $this->assertInstanceOf(
	    	\App\Resources\ProvidesApiResource::class, 
	    	$instance
	    );

	    $this->assertInstanceOf(
	    	\App\Resources\PreviewResource::class, 
	    	$instance->apiResource()
	    );
	}
}