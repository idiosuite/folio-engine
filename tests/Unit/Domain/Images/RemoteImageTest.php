<?php

namespace Tests\Unit\Domain\Images;

use App\Domain\Images\RemoteImage;
use App\Domain\Images\Image;
use Carbon\Carbon;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class RemoteImageTest extends TestCase
{
	/** @test */
	public function it_can_be_instantiated()
	{
	    $this->assertNotNull(new RemoteImage($url = ''));
	}

	/** @test */
	public function it_implements_image_interface()
	{
	    $this->assertInstanceOf(
	    	Image::class, 
	    	new RemoteImage($url = 'https://loremflickr.com/320/240')
	    );
	}

	/** @test */
	public function it_can_be_instantiated_with_url()
	{
	    $image = new RemoteImage($url = 'https://loremflickr.com/320/240');

	    $this->assertEquals($url, $image->getUrl());
	    $this->assertEquals($url, $image->getPath());
	    $this->assertTrue($image->exists());
	    $this->assertEquals(
	    	'<img src="https://loremflickr.com/320/240" width="320" height="240" />', 
	    	$image->getHtml()
	    );
	    $this->assertNull($image->getBasePath());
	    $this->assertNull($image->getBaseUrl());
	    $this->assertEquals($url, $image->getRelativePath());
	    $this->assertEquals(320, $image->getWidth());
	    $this->assertEquals(240, $image->getHeight());
	    $this->assertEquals($url, (string)$image);
	}

	/** @test */
	public function it_can_be_instantiated_with_url_not_exists()
	{
	    $image = new RemoteImage($url = 'https://foo.bar/baz.jpg');

	    $this->assertEquals($url, $image->getUrl());
	    $this->assertEquals($url, $image->getPath());
	    $this->assertFalse($image->exists());
	    $this->assertEquals('', $image->getHtml());
	    $this->assertNull($image->getBasePath());
	    $this->assertNull($image->getBaseUrl());
	    $this->assertEquals($url, $image->getRelativePath());
	    $this->assertNull($image->getWidth());
	    $this->assertNull($image->getHeight());
	    $this->assertEquals($url, (string)$image);
	}

	/** @test */
	public function it_can_be_converted_to_array()
	{
	    $image = new RemoteImage($url = 'https://loremflickr.com/320/240');

	    $this->assertInstanceOf(
	    	\Illuminate\Contracts\Support\Arrayable::class, 
	    	$image
	    );

	    $result = $image->toArray();

	    $this->assertIsArray($result);
	    
	    $this->assertArrayHasKey('src', $result);
	    $this->assertEquals('https://loremflickr.com/320/240', $result['src']);
	    
	    $this->assertArrayHasKey('width', $result);
	    $this->assertEquals('320', $result['width']);
	    
	    $this->assertArrayHasKey('height', $result);
	    $this->assertEquals('240', $result['height']);
	}
}