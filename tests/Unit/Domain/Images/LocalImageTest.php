<?php

namespace Tests\Unit\Domain\Images;

use App\Domain\Images\LocalImage;
use App\Testing\InteractsWithImage;
use App\Testing\InteractsWithFileSystem;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class LocalImageTest extends TestCase
{
	use InteractsWithFileSystem;
	use InteractsWithImage;

	/** @test */
	public function it_can_be_instantiated()
	{
	    $image = new LocalImage($imagePath = '');

	    $this->assertNotNull($image);
	}

	/** @test */
	public function it_returns_base_path()
	{
	    $image = new LocalImage('');

	    $this->assertEquals('vfs://folio/contents/images', $image->getBasePath());
	}

	/** @test */
	public function it_returns_base_url()
	{
	    $image = new LocalImage('');

	    $this->assertEquals($this->baseUrlImages, $image->getBaseUrl());
	}

	/** @test */
	public function it_returns_image_existence()
	{
	    $image = new LocalImage($this->getImageFileName());
	    $this->assertTrue($image->exists());

	    $image = new LocalImage(vfsStream::url('bar.jpg'));
	    $this->assertFalse($image->exists());

	    $image = new LocalImage('');
	    $this->assertFalse($image->exists());
	}

	/** @test */
	public function it_returns_image_path()
	{
		$image = new LocalImage($this->getImageFileName());

	    $this->assertEquals($this->getImagePath(), $image->getPath());
	}

	/** @test */
	public function it_returns_image_width()
	{
	    $image = new LocalImage($this->getImageFileName());

	    $this->assertEquals('120', $image->getWidth());
	}

	/** @test */
	public function it_returns_image_width_not_existing()
	{
	    $image = new LocalImage('');

	    $this->assertNull($image->getWidth());
	}

	/** @test */
	public function it_returns_image_height()
	{
	    $image = new LocalImage($this->getImageFileName());

	    $this->assertEquals('20', $image->getHeight());
	}

	/** @test */
	public function it_returns_image_height_not_existing()
	{
	    $image = new LocalImage('');

	    $this->assertNull($image->getHeight());
	}

	/** @test */
	public function it_returns_html()
	{
		$image = new LocalImage($this->getImageFileName());

	    $this->assertEquals(
	    	sprintf(
	    		'<img src="%s" width="%s" height="%s" />', 
	    		$this->baseUrlImages . '/foo.jpg', 
	    		120, 
	    		20
	    	), 
	    	$image->getHtml()
	    );
	}

	/** @test */
	public function it_will_return_url()
	{
		$this->createDummyImage(
			$original = vfsStream::url('folio/contents/images/foo.png')
		);

		$image = new LocalImage('foo.png');

		$this->assertEquals(
			env('IMAGES_DESTINATION_URL') . '/foo.png', 
			$image->getUrl()
		);
	}

	/** @test */
	public function it_returns_html_with_alt()
	{
		$image = new LocalImage($this->getImageFileName());

	    $this->assertEquals(
	    	sprintf(
	    		'<img src="%s" width="%s" height="%s" alt="Foobar" />', 
	    		$this->baseUrlImages . '/foo.jpg', 
	    		120, 
	    		20
	    	), 
	    	$image->getHtml(['alt' => 'Foobar'])
	    );
	}

	/** @test */
	public function it_returns_no_html_if_image_does_not_exist()
	{
		$image = new LocalImage('foobar.png');

	    $this->assertEquals('', $image->getHtml());
	}

	/** @test */
	public function it_returns_image_path_if_casted_to_string()
	{
	    $image = new LocalImage($this->getImageFileName());

	    $this->assertEquals(
	    	$this->getImageFileName(), 
	    	(string)$image
	    );
	}

	/** @test */
	public function it_removes_double_slashes()
	{
	    $image = new LocalImage('/' . $this->getImageFileName());

	    $this->assertEquals(
	    	$this->baseUrlImages . '/foo.jpg', 
	    	$image->getUrl()
	    );

	    $this->assertEquals(
	    	'vfs://folio/contents/images/foo.jpg', 
	    	$image->getPath()
	    );
	}

	/** @test */
	public function it_can_be_converted_to_array()
	{
	    $image = new LocalImage('/' . $this->getImageFileName());

	    $this->assertInstanceOf(
	    	\Illuminate\Contracts\Support\Arrayable::class, 
	    	$image
	    );

	    $result = $image->toArray();

	    $this->assertIsArray($result);
	    
	    $this->assertArrayHasKey('src', $result);
	    $this->assertEquals($this->baseUrlImages . '/foo.jpg', $result['src']);
	    
	    $this->assertArrayHasKey('width', $result);
	    $this->assertEquals('120', $result['width']);
	    
	    $this->assertArrayHasKey('height', $result);
	    $this->assertEquals('20', $result['height']);
	}
}