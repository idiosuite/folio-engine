<?php

namespace Tests\Unit\Domain\Images;

use App\Domain\Images\AbsoluteImage;
use App\Testing\InteractsWithImage;
use App\Testing\InteractsWithFileSystem;
use Tests\TestCase;
use org\bovigo\vfs\vfsStream;

class AbsoluteImageTest extends TestCase
{
	use InteractsWithFileSystem;
	use InteractsWithImage;

	/** @test */
	public function it_returns_array_if_image_exists()
	{
		$this->createDummyImage(vfsStream::url('folio/public/foo.jpg'));

		$image = \App\Domain\ImageFactory::make('~/foo.jpg');

	    $this->assertEquals(
	    	['src' => '/foo.jpg', 'width' => 50, 'height' => 50], 
	    	$image->toArray()
	    );
	}

	/** @test */
	public function it_returns_null_instead_of_array_if_image_does_not_exist()
	{
		$image = \App\Domain\ImageFactory::make('~/foo.jpg');

	    $this->assertNull(
	    	$image->toArray()
	    );
	}

	/** @test */
	public function it_returns_path()
	{
	    $image = \App\Domain\ImageFactory::make('~/foo.jpg');

	    $this->assertEquals(vfsStream::url('folio/public/foo.jpg'), $image->getPath());
	}

	/** @test */
	public function it_returns_url()
	{
	 	$image = \App\Domain\ImageFactory::make('~/foo.jpg');
		
	    $this->assertEquals('/foo.jpg', $image->getUrl());   
	}

	/** @test */
	public function it_returns_base_path()
	{
	 	$image = \App\Domain\ImageFactory::make('~/foo.jpg');

	    $this->assertEquals(vfsStream::url('folio/public'), $image->getBasePath());
	}

	/** @test */
	public function it_returns_base_url()
	{
	    $image = \App\Domain\ImageFactory::make('~/foo.jpg');

	    $this->assertEquals('', $image->getBaseUrl());
	}

	/** @test */
	public function it_returns_html_if_image_exists()
	{
		$this->createDummyImage(vfsStream::url('folio/public/foo.jpg'));

	    $image = \App\Domain\ImageFactory::make('~/foo.jpg');

	    $this->assertEquals(
	    	'<img src="/foo.jpg" width="50" height="50" />', 
	    	$image->getHtml()
	    );
	}

	/** @test */
	public function it_returns_no_html_if_image_does_not_exist()
	{
		$image = \App\Domain\ImageFactory::make('~/foo.jpg');

	    $this->assertNull(
	    	$image->getHtml()
	    );
	}

	/** @test */
	public function it_assert_existence()
	{
	    $this->createDummyImage(vfsStream::url('folio/public/foo.jpg'));

		$existing = \App\Domain\ImageFactory::make('~/foo.jpg');
	    $this->assertTrue($existing->exists());

	    $notExisting = \App\Domain\ImageFactory::make('~/bar.jpg');
	    $this->assertFalse($notExisting->exists());
	}
}