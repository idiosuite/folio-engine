<?php

namespace Tests\Feature;

use App\App;
use App\Portfolio;
use Illuminate\Testing\TestResponse;
use Symfony\Component\HttpFoundation\Request;
use Tests\FeatureTestCase;
use org\bovigo\vfs\vfsStream;

class ProjectTest extends FeatureTestCase
{
	/** @test */
	public function it_provides_endpoint_to_project()
	{
	    $response = $this->visit('/project/test-project');

	    $response->assertOk();
	}

	/** @test */
	public function it_displays_project()
	{
	    $response = $this->visit('/project/test-project');

	    $response->assertSee('My Test project');
		$response->assertSee('Proin convallis eget odio in faucibus');
		$response->assertSee('https://player.vimeo.com/video/148891637');
	}

	/** @test */
	public function it_interpolates_images()
	{
	    $response = $this->visit('/project/test-project');

	    $response->assertSee(
	    	'<img src="vfs://folio/contents/images/test-project/image-1.jpg" width="5000" height="3333" />', 
	    	$escape = false
	    );
	}

	/** @test */
	public function it_interpolates_vimeo()
	{
	    $response = $this->visit('/project/test-project');

	    $response->assertSee('https://player.vimeo.com/video/148891637');
	}

	/** @test */
	public function it_interpolates_slideshow()
	{
	    $response = $this->visit('/project/test-project');

	    $response->assertSee(
	    	'<span class="Slideshow">',
	    	$escape = false
	    );
	}

	/** @test */
	public function it_interpolates_credits()
	{
	    $response = $this->visit('/project/test-project');

	    $response->assertSee(
	    	'<div class="Credits">
Photo courtesy of: Unsplash
</div>', 
			$escape = false
		);
	}

	/** @test */
	public function it_displays_project_credits()
	{
	    $response = $this->visit('/project/test-project');

	    $response->assertSee('Credits');
	    $response->assertSee('Company Name - City - Copyright © 2021');
	    $response->assertSee('CEO: John Smith');
	    $response->assertSee('Marketing Director: Jane Smith');
	    $response->assertSee('Art Director: John Doe');
	    $response->assertSee('Designer: Jane Doe');
	}

	/** @test */
	public function it_displays_project_date()
	{
	    $response = $this->visit('/project/test-project');

	    $response->assertSee('November 2021');
	}

	/** @test */
	public function it_displays_menu()
	{
	    $response = $this->visit('/project/test-project');

	    $this->assertSeeMenu($response);
	}

	/** @test */
	public function it_displays_gallery()
	{
	    $response = $this->visit('/project/test-project');

	    $this->assertSeeGallery($response);
	}

	/** @test */
	public function it_displays_links()
	{
	    $response = $this->visit('/project/test-project');

	    $this->assertSeeLinks($response);
	}

	/** @test */
	public function it_displays_settings()
	{
	    $response = $this->visit('/project/test-project');

	    $this->assertSeeSettings($response);
	}

	/** @test */
	public function it_displays_utilities()
	{
	    $response = $this->visit('/project/test-project');

	    $this->assertSeeUtilities($response, 'page');
	}

	/** @test */
	public function it_displays_assets()
	{
		$response = $this->visit('/project/test-project');

	    $this->assertSeeAssets($response);
	}
}