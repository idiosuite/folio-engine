<?php

namespace Tests\Feature;

use App\App;
use App\Portfolio;
use Illuminate\Testing\TestResponse;
use Symfony\Component\HttpFoundation\Request;
use Tests\FeatureTestCase;
use org\bovigo\vfs\vfsStream;

class HomeTest extends FeatureTestCase
{
	/** @test */
	public function it_provides_endpoint()
	{
	    $response = $this->visit('/');

	    $response->assertOk();
	}

	/** @test */
	public function it_does_not_display_default_slug_if_none_is_set()
	{
		$this->overrideSettings(['defaultSlug' => '']);

	    $response = $this->visit('/');

	    $response->assertOk();
	    $this->assertDontSeeDefaultPage($response);
	}

	/** @test */
	public function it_displays_default_slug_in_homepage()
	{
		$this->overrideSettings(['defaultSlug' => 'welcome']);

	    $response = $this->visit('/');

	    $response->assertOk();
	    $this->assertSeeDefaultPage($response);
	}

	/** @test */
	public function it_displays_published_default_slug_in_homepage_but_not_in_menu()
	{
		$this->overrideSettings(['defaultSlug' => 'test-page']);

	    $response = $this->visit('/');

		$response->assertOk();
		$response->assertSee('My Test Page');
		$response->assertDontSee('<li class="Menu__item Menu__item--test-page">');
		$this->assertDontSeeMenuItem('My Test Page');
	}

	/** @test */
	public function it_displays_menu()
	{
	    $response = $this->visit('/');

	    $this->assertSeeMenu($response);
	}

	/** @test */
	public function it_displays_gallery()
	{
	    $response = $this->visit('/');

	    $this->assertSeeGallery($response);
	}

	/** @test */
	public function it_displays_links()
	{
	    $response = $this->visit('/');

	    $this->assertSeeLinks($response);
	}

	/** @test */
	public function it_displays_settings()
	{
	    $response = $this->visit('/');

	    $this->assertSeeSettings($response);
	}

	/** @test */
	public function it_displays_utilities()
	{
	    $response = $this->visit('/');

	    $this->assertSeeUtilities($response, 'home');
	}

	/** @test */
	public function it_displays_assets()
	{
	    $response = $this->visit('/');

	    $this->assertSeeAssets($response);
	}
}