<?php

namespace Tests\Feature;

use App\App;
use App\Portfolio;
use Illuminate\Testing\TestResponse;
use Symfony\Component\HttpFoundation\Request;
use Tests\FeatureTestCase;
use org\bovigo\vfs\vfsStream;

class PageTest extends FeatureTestCase
{
	/** @test */
	public function it_provides_endpoint()
	{
	    $response = $this->visit('/page/test-page');

	    $response->assertOk();
	}

	/** @test */
	public function it_provides_nested_endpoint()
	{
	    $response = $this->visit('/page/nested/test-page-nested');

	    $response->assertOk();
	}

	/** @test */
	public function it_provides_nested_endpoint_recursive()
	{
	    $response = $this->visit('/page/nested/sub/test-page-sub');

	    $response->assertOk();
	}

	/** @test */
	public function it_prevents_endpoint_dots_for_security_reasons()
	{
	    $response = $this->visit('/page/nested/sub/../test-page-nested');

	    $response->assertOk();

	    $response->assertSee('Not found');
	}

	/** @test */
	public function it_displays_page()
	{
	    $response = $this->visit('/page/test-page');

	    $response->assertSee('My Test Page');
		$response->assertSee('<strong>Hello</strong>', $escape = false);
		$response->assertSee('Mauris dui massa, rhoncus interdum fermentum sed, maximus in orci');
	}

	/** @test */
	public function it_displays_nested_page()
	{
	    $response = $this->visit('/page/nested/test-page-nested');

	    $response->assertSee('My Test Nested Page');
		$response->assertSee('<strong>Hello</strong>', $escape = false);
		$response->assertSee('Mauris dui massa, rhoncus interdum fermentum sed, maximus in orci');
	}

	/** @test */
	public function it_displays_nested_page_recursive()
	{
	    $response = $this->visit('/page/nested/sub/test-page-sub');

	    $response->assertSee('My Test Sub Page');
		$response->assertSee('<strong>Hello</strong>', $escape = false);
		$response->assertSee('Mauris dui massa, rhoncus interdum fermentum sed, maximus in orci');
	}

	/** @test */
	public function it_displays_page_with_custom_layout()
	{
	    $response = $this->visit('/page/test-page-custom');

	    $response->assertSee('This is a custom page');
	    $response->assertSee('My Test Page With Custom Layout');
	    $response->assertSee('<strong>About</strong>', $escape = false);
	    $response->assertSee('Praesent at scelerisque eros');
	}

	/** @test */
	public function it_does_not_display_gallery()
	{
	    $response = $this->visit('/page/test-page');

	    $this->assertDontSeeGallery($response);
	}

	/** @test */
	public function it_displays_menu()
	{
	    $response = $this->visit('/page/test-page');

	    $this->assertSeeMenu($response);
	}

	/** @test */
	public function it_displays_links()
	{
	    $response = $this->visit('/page/test-page');

	    $this->assertSeeLinks($response);
	}

	/** @test */
	public function it_displays_settings()
	{
	    $response = $this->visit('/page/test-page');

	    $this->assertSeeSettings($response);
	}

	/** @test */
	public function it_displays_utilities()
	{
	    $response = $this->visit('/page/test-page');

	    $this->assertSeeUtilities($response, 'page');
	}

	/** @test */
	public function it_displays_assets()
	{
	    $response = $this->visit('/page/test-page');

	    $this->assertSeeAssets($response);
	}
}